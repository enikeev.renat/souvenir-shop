import '~/assets/scss/style.scss'

window.app = window.app || {};
/**
 * modules
 */
import scrollbar from '~/view/modules/scrollbar/script'
import '~/view/modules/form/script.js'

import 'tippy.js/dist/tippy.css';
import 'tippy.js/themes/light.css';
import tippy from 'tippy.js';
window.app.tippy = tippy;

import '~/view/modules/popup/style.scss'
import Popup from '~/view/modules/popup/script'

window.app.popup = new Popup({
	bodyClass: 'popup-opened',
	fixedOnIos: true,
	closePopupOnEsc: true,
	closePopupOnOverlay: false,
});


/**
 * main layout
 */
import '~/view/layout/menu/script.js'
import '~/view/layout/search/script.js'
import '~/view/layout/header/script.js'
import '~/view/layout/footer/script.js'

/**
 * sections
 */

import '~/view/pages/home/script.js'
import '~/view/pages/catalog/script.js'
import '~/view/pages/catalog/script.js'
import '~/view/pages/article/script.js'
import '~/view/pages/category/script.js'
import '~/view/pages/cart/script.js'
import '~/view/pages/lk/script.js'
import '~/view/pages/contact/script.js'
import '~/view/pages/review/script.js'

import fakeServer from './fake-api';

if (IS_LOCAL_ENV) {
	fakeServer()
}


document.addEventListener("DOMContentLoaded", function() {
	scrollbar.init(document.querySelectorAll('[data-scrollbar]'));
});

if (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream){
	document.body.classname += ' is-ios';
}

if ("ontouchstart" in document.documentElement){
	document.body.className += ' is-touchable';
}

$(()=>{

	$('body').on('mouseover', '[data-tooltip]', function () {
		const $this = $(this);
		const tooltip = $this.attr('data-tooltip');
		const isContent = /^[.#][\w\-]+$/.test(tooltip);
		const $content = $this.find(tooltip);
		const content = isContent ? $content.html() : tooltip;

		if (this._tippy){
			this._tippy.setContent(content)
		} else {
			window.app.tippy(this, {
				content,
				delay: [0, 100],
				theme: 'light',
				placement: isContent ? 'bottom' : 'top',
				arrow: true,
				allowHTML: true,
				interactive: isContent,
			})
		}

	})

})