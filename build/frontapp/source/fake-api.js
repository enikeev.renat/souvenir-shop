import { createServer } from "miragejs"

export default function () {
	createServer({
		routes() {
			this.post("/api/catalog/get_all", () => {

				let html = '';
				let done = Math.floor(Math.random() * (5 - 1)) + 1 === 3;

				for (let i = 0; i < 18; i++){
					const inCart = Math.floor(Math.random() * (7 - 1)) + 1 === 3;
					const notAvailable = Math.floor(Math.random() * (7 - 1)) + 1 === 3;
					const id = Math.floor(Math.random() * (10000 - 1)) + 1;
					html += `<div class="product-item ${inCart ? 'product-item_incart' : ''} ${notAvailable ? 'product-item_absent' : ''}">
						<div class="product-item__inner">
							<div class="product-item__top">
								<div class="product-item__image">
									<a href="/catalog-item.html">
										<img src="https://picsum.photos/id/${Math.floor(Math.random() * (399 - 301)) + 301}/600/600">
									</a>
								</div>
							</div>
							<div class="product-item__info">
								<div class="product-item__category"><a href="/category-item.html">Категория</a></div>
								<div class="product-item__title"><a href="/catalog-item.html">Название сувенира</a></div>
								<div class="product-item__price">₽ ${Math.floor(Math.random() * Math.floor(10)) * 100}</div>
								<div class="product-item__action">
									<div class="btn fill" data-cart-added="${inCart}" data-cart-id="${id}">${inCart ? 'Убрать из корзины' : 'Добавить в корзину'}</div>
								</div>
							</div>
						</div>
					</div>`
				}

				return {
					html,		// String - разметка, которая будет добавлена на страницу
					done,  	  	// Boolean - больше карточек нет загружены все.
					total: 80 	// общее количество карточек
				}
			})

			this.post("/article/json/list", () => {

				let html = '';
				let done = Math.floor(Math.random() * (5 - 1)) + 1 === 3;

				for (let i = 0; i < 18; i++){
					html += `<div class="article-item">
						<div class="article-item__inner">
						  <div class="article-item__top">
							<div class="article-item__image"><a href="article-item.html"><img src="https://picsum.photos/id/${Math.floor(Math.random() * (399 - 301)) + 301}/600/600"></a></div>
						  </div>
						  <div class="article-item__info">
							<div class="article-item__title"><a href="article-item.html">Заголовок статьи</a></div>
							<div class="article-item__description"><a href="article-item.html">Краткое описание, в одно не очень длинное предложение. Или в два коротких, но чтобы не сильно много читать.</a></div>
						  </div>
						</div>
					  </div>
					`
				}

				return {
					html,		// String - разметка, которая будет добавлена на страницу
					done,  	  	// Boolean - больше карточек нет загружены все.
					total: 80 	// общее количество карточек
				}
			})

			// добавление товара в корзину
			this.post("/api/cart/add", () => {
				return {
					inCart: 5,	// Количество продуктов в корзине
				}
			})

			// удаление товара из корзины
			this.post("/api/cart/remove", () => {
				return {
					inCart: 4,	// Количество продуктов в корзине
				}
			})

			// удаление товара из корзины
			this.post("/api/cart/remove", () => {
				return {
					inCart: 4,	// Количество продуктов в корзине
				}
			})

			this.post("/api/cart/calc", () => {
				return {
					info: {
						goodsCount: 4, // Количество наименований товара
						itemsCount: 12, // Общее количество единиц товара
						priceWithoutDiscount: 65160, // Стоимость без учета скидки в руб.
						priceWithDiscount: 58644, // Стоимость с учетом скидки в руб.
						discount: 6516, // размер скидки в рублях
					}
				}
			})

			this.post("/api/login", () => {
				return {
					errors: null,
				}
			})

			this.post("/api/register", () => {
				return {
					errors: null,
					message: '<h3>Спасибо за регистрацию!</h3><p>Вам на почту отправлено письмо.<br>Для подтверждения email, перейдите по ссылке в письме</p>'
				}
			})

			this.post("/api/restore", () => {
				return {
					errors: null,
					message: '<h3>Вам на почту отправлено письмо.</h3><p>Для сброса пороля, перейдите по ссылке в письме</p>'
				}
			})

			this.post("/api/feedback", () => {

				return {
					errors: null,
					message: '<h3>Спасибо!</h3><p>Ваше сообщение отправлено</p>'
				}
			})

			this.post("/api/search", () => {


				let html = '';

				html += `<h2>Сувениры</h2>`;

				for (let i = 0; i < 5; i++){
					html += `<a class="search-result__item" href="#">
								<div class="search-result__title">Название, заголовок</div>
								<div class="search-result__description">Описание описание описание описание описание описание описание описание</div>
							</a>`
				}

				html += `<h2>ПреМудрости</h2>`;

				for (let i = 0; i < 9; i++){
					html += `<a class="search-result__item" href="#">
								<div class="search-result__title">Название, заголовок</div>
								<div class="search-result__description">Описание описание описание описание описание описание описание описание</div>
							</a>`
				}

				return {
					errors: null,
					html
				}
			})

		},
	})
}


