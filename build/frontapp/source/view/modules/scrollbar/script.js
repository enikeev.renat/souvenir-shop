import './style.scss'
import OverlayScrollbars from 'overlayscrollbars';

let options = {
	className: 'os-custom-theme',
	sizeAutoCapable : true,
	paddingAbsolute : true,
	scrollbars : {
		clickScrolling : true,
		autoHide : "leave"
	},
	overflowBehavior:{
		x: 'hidden'
	}
};

export default {
	init(el, opts){
		if (!el.scrollbar) {
			el.scrollbar = OverlayScrollbars(el, Object.assign(options, opts || {}));
		}
	}
}