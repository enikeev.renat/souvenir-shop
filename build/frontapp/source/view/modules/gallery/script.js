import 'lightgallery'

export default function (wrap, item) {

	$(wrap).each((i, e)=>{
		$(e).eq(i).lightGallery({
			counter: false,
			download: false,
			loop: true,
			hideControlOnEnd: true,
			selector: item,
			youtubePlayerParams: {
				modestbranding: 1,
				showinfo: 0,
				rel: 0,
				controls: 0
			},
			vimeoPlayerParams: {
				byline : 0,
				portrait : 0,
				color : 'A90707'
			},

			thumbnail:true,
			// dynamic:true,
		})
	});

}