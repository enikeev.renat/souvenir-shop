import './style.scss'

import $ from 'jquery'

$(()=>{

	const $b = $('body');

	$b
		.on('click', '.header__handle', () => {
			$b.toggleClass('menu-opened')
		})
		.on('click', '.header__overlay, .header__menu-handle', () => {
			$b.removeClass('menu-opened')
		})
	;

});

