import './style.scss'

import $ from 'jquery'

$(()=>{

	let menuTimer = null;

	$.fn.clickOutSide = function(callback = ()=>{}){
		$(document).on('click',(e) => {
			if ($(e.target).closest(this).length <= 0) {
				callback(this);
				return this;
			}
		});
		return this;
	};

	const $b = $('body');

	$b
		.on('click', '.menu-mobile [data-menu-goto]', function(e){
			const $t = $(this);
			const $wrap = $t.closest('.menu-mobile');
			const id = $t.attr('data-menu-goto');
			const back = $t.hasClass('menu-mobile__btn_back');
			const target = $wrap.find(`[data-menu-list = ${id}]`);

			if (id) e.preventDefault();
			if (back) $t.closest('.menu-mobile__list').removeClass('is-active');
			target.addClass('is-active');
		})

		.on('mouseover', '.menu-desktop__main-item', function(e){
			const $t = $(this);
			const $wrap = $t.closest('.menu-desktop');
			const id = $t.attr('data-menu-main');
			if ($t.hasClass('is-active')) return false;

			if (id) {
				$t.siblings('.is-active').removeClass('is-active');
				$t.addClass('is-active');
				$wrap.find('.menu-desktop__list').removeClass('is-active');
				$wrap.find(`[data-menu-list="${id}"]`).addClass('is-active');
			}

			$wrap.find('.menu-desktop__item.is-active').removeClass('is-active');
			$wrap.find('.menu-desktop__body-visual-image').fadeOut();
		})

		.on('mouseover', '.menu-desktop__item[data-menu-child]', function(e){
			const $t = $(this);
			const $wrap = $t.closest('.menu-desktop');
			const id = $t.attr('data-menu-child');

			if ($t.hasClass('is-active')) return false;

			if (id) {
				menuTimer = setTimeout(()=>{
					$wrap.find('.menu-desktop__body-visual-image').fadeOut();
					$t.addClass('is-active');
					$t.siblings('.is-active').removeClass('is-active');
					$wrap.find(`[data-menu-list="${id}"]`).addClass('is-active');
				}, 300)
			}

		})

		.on('mouseover', '.menu-desktop__item[data-menu-image]', function(e){
			const $t = $(this);
			if ($t.hasClass('is-active')) return false;

			menuTimer = setTimeout(()=>{
				const $wrap = $t.closest('.menu-desktop');
				const src = $t.attr('data-menu-image');
				const $box = $('.menu-desktop__body-visual');
				const $oldImage = $box.find('.menu-desktop__body-visual-image');
				const $newImage = $('<div/>', {
					class: 'menu-desktop__body-visual-image',
					style: `background-image:url(${src}); display: none`
				});
				const level1 = $t.closest('.menu-desktop__list_level-1').length;
				const level2 = $t.closest('.menu-desktop__list_level-2').length;
				let loaded = false;
				let hided = false;

				if ( level1 ){
					$wrap.find('.menu-desktop__item.is-active').removeClass('is-active');
					$wrap.find('.menu-desktop__list_level-2.is-active').removeClass('is-active');
				}
				if ( level2 ){
					$wrap.find('.menu-desktop__list_level-2').find('.menu-desktop__item.is-active').removeClass('is-active');
				}
				$box.append($newImage);
				$t.addClass('is-active');

				const img = new Image();
				img.src = src;
				img.onload = () => {
					loaded = true;
					if (hided && $newImage.length) $newImage.fadeIn();
				};

				if ($oldImage.length){
					$oldImage.fadeOut(300, ()=>{
						hided = true;
						$oldImage.remove();
						if (loaded && $newImage.length) $newImage.fadeIn();
					})
				} else {
					hided = true;
					$oldImage.remove();
					if (loaded && $newImage.length) $newImage.fadeIn();
				}
			}, 300);

		})

		.on('mouseleave', '.menu-desktop__item[data-menu-child]', ()=>{
			clearTimeout(menuTimer);
		})

		.on('mouseleave', '.menu-desktop__item[data-menu-image]', ()=>{
			clearTimeout(menuTimer);
		})
	;







});

