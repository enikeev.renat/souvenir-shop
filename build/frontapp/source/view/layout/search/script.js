import './style.scss'

$(()=>{

	$('body').on('click', '.js-open-search', ()=>{
		$('body').addClass('search-opened').removeClass('menu-opened');
	});

	$('.search').each(function () {
		const $t = $(this);
		const $input = $t.find('.search-input__field');
		const $form = $input.closest('form');
		const $result = $t.find('.search-result');
		const $clear = $t.find('.search-input__clear');
		let timer = null;

		$form.on('submit', e => e.preventDefault());

		$clear.click(()=>{
			$('body').removeClass('search-opened');
			$input.val('');
			$result.html('');
		})

		$input.on('keydown input change', ()=>{
			clearTimeout(timer);
			timer = setTimeout(()=>{
				const value = $input.val().trim();

				if (value.length >= 3){
					$t.addClass('is-loading');
					$.ajax({
						type: $form.attr('method'),
						url: $form.attr('action'),
						data: $form.serialize(),
						success: res => {
							$result.html('');
							if (res.html) $result.html(res.html);
						},
						error: (e)=>{

						},
						complete: ()=>{
							$t.removeClass('is-loading');
						}
					});
				} else {
					$result.html('<p>Введите минимум 3 символа для поиска</p>');
				}

			}, 500)
		})

	})

});

