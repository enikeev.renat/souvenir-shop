import './style.scss'
import '../../../assets/img/map-marker.svg'

$(()=>{

	const mapInit = () => {
		$('.contact-map').each((i, e) => {
			const center = [59.8858829, 30.4741537];
			const map = new ymaps.Map(e, {
				center,
				zoom: 15,
				controls: []
			});

			const placementMarker = new ymaps.Placemark(center,
					{},
					{
						iconLayout: 'default#image',
						iconImageHref: IS_LOCAL_ENV ? '../img/map-marker.svg' : '/assets/img/map-marker.svg',
						iconImageSize: [60, 83],
						iconImageOffset: [-30, -83]
					});
			map.geoObjects.add(placementMarker);

			map.behaviors.disable('scrollZoom');
			// map.behaviors.disable('multiTouch');

			map.controls.add('zoomControl', {
				size: 'small',
				float: 'none',
				position: {
					bottom: '50px',
					right: '30px'
				}
			});

			try {
				if ( document.createEvent("TouchEvent") ){
					map.behaviors.disable('drag');
				}
			} catch (e) {
			}

		});
	}

	try {
		mapInit()
	} catch (e) {
		window.ymaps.ready(mapInit);
	}
});