import './style.scss'
import {declension} from '~/view/modules/helper/script'

$(()=>{

	let counterTimer = null;

	$('body')
	.on('keydown', 'input[type="number"]', (e) => {
		const invalidChars = ["-", "+", "e"];
		if (invalidChars.includes(e.key)) {
			e.preventDefault();
		}
	})
	.on('click', '.cart-item__counter-handle', function() {
		const $t = $(this);
		const up = $t.hasClass('cart-item__counter-handle_up');
		const down = $t.hasClass('cart-item__counter-handle_down');
		const $input = $t.parent().find('.cart-item__counter-input');
		const value = +$input.val();

		if (up) $input.val(value + 1);
		if (down && value > 1) $input.val(value - 1);

		$input.trigger('input');

	})
	.on('input change keyup', '.cart-item__counter-input', function() {
		const $t = $(this);
		const $item = $t.closest('.cart-item');
		const $form = $t.closest('.form_cart');
		const id = $item.find('.cart-item__counter-input').attr('name');
		const val = +$t.val();
		const price = +$item.attr('data-price');

		const cost = price * val;

		$item.find('.cart-item__price').html(`₽ ${cost}`);

		clearTimeout(counterTimer);
		counterTimer = setTimeout(()=>{
			$form.submit();
		}, 500)

	})
	.on('click', '[data-cart-id]', async function () {
		const $t = $(this);
		const id = $t.attr('data-cart-id');
		const added = JSON.parse($t.attr('data-cart-added'));

		if ($t.hasClass('is-loading')) return false;
		$t.addClass('is-loading');

		try {

			const res = await $.ajax({
				type: 'POST',
				url: added ? '/api/cart/remove' : '/api/cart/add',
				data: {
					id,
					count: added ? 1 : undefined
				},
			});

			console.info(res)
			const count = parseInt(res.inCart);
			const $item = $(`[data-cart-id="${id}"]`);
			drawCartCount(count);

			if (added) {
				$t.removeClass('incart');
				$item.closest('.product-item').removeClass('product-item_incart');
			} else {
				$t.addClass('incart');
				$item.closest('.product-item').addClass('product-item_incart');
			}
			$t.attr('data-cart-added', !added);
			$t.html(added ? 'Добавить в корзину' : 'Убрать из корзины')
		} finally {
			$t.removeClass('is-loading');
		}
	})
	.on('click', '.cart-item__remove', async function () {
		const $t = $(this);
		const $item = $t.closest('.cart-item');
		const id = $item.find('.cart-item__counter-input').attr('name');

		if ($item.hasClass('is-loading')) return false;
		$item.addClass('is-loading');
		try {
			const res = await $.ajax({
				type: 'POST',
				url: '/api/cart/remove',
				data: {
					id
				}
			});
			$item.slideUp(()=>{
				$item.remove();
			});
			const count = parseInt(res.inCart);
			drawCartCount(count);
			$t.closest('form').submit();
		} finally {
			$item.removeClass('is-loading');
		}

	})
	.on('submit', '.form_cart', async function (e) {
		e.preventDefault();

		const $form = $(this);

		if ($form.hasClass('is-loading')) return false;
		$form.addClass('is-loading');

		try {
			const {info} = await $.ajax({
				type: $form.attr('method'),
				url: $form.attr('action'),
				data: $form.serialize(),
			});

			$form.find('.cart-bill__info-item_goods .cart-bill__info-value').html(`${info ? info.goodsCount : 0} шт.`);
			$form.find('.cart-bill__info-item_items .cart-bill__info-value').html(`${info ? info.itemsCount : 0} шт.`);
			$form.find('.cart-bill__info-item_price .cart-bill__info-value').html(`₽ ${info ? info.priceWithoutDiscount : 0}`);
			$form.find('.cart-bill__info-item_discount .cart-bill__info-value').html(`₽ ${info ? info.discount : 0}`);
			$form.find('.cart-bill__info-item_result .cart-bill__info-value').html(`₽ ${info ? info.priceWithDiscount : 0}`);

			drawCartCount(info ? info.goodsCount : 0)
		} finally {
			$form.removeClass('is-loading');
		}

	})
	;

	function drawCartCount(count) {
		const $cart = $('.menu__item_cart');
		const $cartWrapper = $('.cart-wrapper');
		$cart.find('.menu__icon').find('span').html(count > 0 ? count : '');
		$cart.find('.menu__tooltip').html(count > 0 ? `В корзине ${count} ${declension(count, 'товар', 'товара', 'товаров')}` : 'В корзине пусто');

		if (count && count > 0) {
			$cart.addClass('menu__item_incart');
		} else {
			$cart.removeClass('menu__item_incart');
			$cartWrapper.html('<div class="article-content"><p>Корзина пуста</p></div>');
		}
	}
});
