import './style.scss'
import gallery from '~/view/modules/gallery/script'

$(()=>{

	const $b = $('body');


	$b
		.on('click', '.filter__handle', () => {
			$b.toggleClass('filter-opened')
		})
		.on('click', '.filter__overlay, .filter__menu-handle, .close-filter', () => {
			$b.removeClass('filter-opened')
		})
	;

	gallery('.product-card__preview', 'a');

	let updateTimer = null;
	$('.form_filter')
		.on('change', function (){
			clearTimeout(updateTimer);
			updateTimer = setTimeout(()=>{
				$(this).submit();
			}, 500)
		})
	;
});

