import './style.scss'
import gallery from '~/view/modules/gallery/script'
import Swiper from "swiper";

$(()=>{

	$('.article-gallery').each(function(){
		let $t = $(this),
			$slider = $t.find('.article-gallery__slider'),
			$dots = $t.find('.slider__dots')
		;

		$slider
			.children()
			.addClass('swiper-wrapper')
			.children()
			.addClass('swiper-slide')
		;

		new Swiper($slider[0], {
			slidesPerView: 'auto',
			autoHeight: false,
			spaceBetween: 24,
			loop: false,
			autoplay: false,
			pagination: {
				el: $dots,
				clickable: true,
			},
			navigation: {
				nextEl: '.slider__arrow_next',
				prevEl: '.slider__arrow_prev',
			},
			lazy: {
				loadPrevNext: true,
			},
		});
	});

	gallery('.article-gallery', 'a');
});