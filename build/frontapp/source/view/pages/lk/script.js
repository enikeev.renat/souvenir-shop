import './style.scss'

$(()=>{


	$('body')
		.on('click', '.js-order-toggle', function () {
			const $t = $(this);
			const $wrap = $t.closest('.order-item');
			const $details = $wrap.find('.order-item__detail');
			const isVisible = $details.is(':visible');

			if ($details.hasClass('is-loading')) return false;
			$details.addClass('is-loading');
			$t.text(isVisible ? 'показать' : 'скрыть')
			$details.slideToggle(()=> {
				$details.removeClass('is-loading');
			});
		})
	;

});