import './style.scss'
import Swiper from 'swiper'

$(()=>{

	$('.welcome-category').each(function(){
		let $t = $(this),
			$slider = $t.find('.welcome-category__slider'),
			$dots = $t.find('.slider__dots')
		;

		$slider
			.children()
			.addClass('swiper-wrapper')
			.children()
			.addClass('swiper-slide')
		;

		new Swiper($slider[0], {
			slidesPerView: 'auto',
			autoHeight: false,
			loop: false,
			autoplay: false,
			pagination: {
				el: $dots,
				clickable: true,
			},
			navigation: {
				nextEl: '.slider__arrow_next',
				prevEl: '.slider__arrow_prev',
			},
			lazy: {
				loadPrevNext: true,
			},
		});
	});
});