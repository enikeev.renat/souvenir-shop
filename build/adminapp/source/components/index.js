import Vue from 'vue';

import ElementUI from 'element-ui'
import locale from 'element-ui/lib/locale/lang/ru-RU'

import ElChartDoughnut from './el-chart-doughnut.vue'
import ElEditor from './el-editor'
import ElGallery from './el-gallery'
import ElCascaderSingle from './el-cascader-single'

const components = {
	ElEditor,
	ElGallery,
	ElCascaderSingle,
	ElChartDoughnut
};

Vue.use(ElementUI, {locale});

Object.keys(components).forEach(name => {
	Vue.component(name, components[name]);
});

