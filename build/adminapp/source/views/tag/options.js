export default {

	section: 'tag',

	names: {
		createStatus: 'Новый тег',
		updateStatus: 'Редактирование тега',
		addBtn: 'Добавить тег',
		saveBtn: 'Сохранить тег',
		deleteBtn: 'Удалить тег',
		deleteQuestion: 'Вы уверены, что хотите удалить этот тег?',
	},

	message: {
		created: 'Тег создан',
		updated: 'Тег обновлен',
		deleted: 'Тег удален',
		clean: 'В разделе еще не добавлено ни одного тега'
	},

	route: {
		table: '/tag',
		add: '/tag/add',
		pagination: true
	}
}