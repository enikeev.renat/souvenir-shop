import {state, actions, mutations, getters} from '~/middleware/store/util'
import options from './options'
import client from '~/middleware/api'

const defaultItem = (responseItem) => {
	return {
		id: responseItem.id || undefined,
		name: responseItem.name || '',
		description: responseItem.description || '',
	}
};

export default {
	namespaced: true,

	state: {
		...state(defaultItem)
	},

	mutations: {
		...mutations(defaultItem)
	},

	actions: {
		...actions(options.section),

		async autoCreate({dispatch}, arr){
			const requests = arr.map(item => {
				return client.tag.addItem({
					name: item,
					description: ''
				})
			});
			const res = await Promise.all(requests);
			dispatch('collection/tagCollection', true, { root: true });
			return res;
		}
	},

	getters: {
		...getters()
	}
}