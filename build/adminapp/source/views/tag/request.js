import axios from '~/middleware/api/instance'

export default {

	getAll(data = {}){
		return axios.post('/api/tag/get-all', {...data}).then(r => r.data)
	},

	getItem(data = {}){
		return axios.post('/api/tag/get', {...data}).then(r => r.data)
	},

	addItem(data = {}){
		return axios.post('/api/tag/new', {...data}).then(r => r.data)
	},

	updateItem(data = {}){
		return axios.post('/api/tag/update', {...data}).then(r => r.data)
	},

	deleteItem(data = {}){
		return axios.post('/api/tag/delete', {...data}).then(r => r.data)
	},

}