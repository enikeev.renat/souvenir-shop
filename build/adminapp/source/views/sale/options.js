export default {

	section: 'sale',

	names: {
		createStatus: 'Новая скидка',
		updateStatus: 'Редактирование скидки',
		addBtn: 'Добавить скидку',
		saveBtn: 'Сохранить скидку',
		deleteBtn: 'Удалить скидку',
		deleteQuestion: 'Вы уверены, что хотите удалить эту скидку?',
	},

	message: {
		created: 'Скидка создана',
		updated: 'Скидка обновлена',
		deleted: 'Скидка удалена',
		clean: 'В разделе еще не добавлено ни одной скидки'
	},

	route: {
		table: '/sale',
		add: '/sale/add',
		pagination: false
	}
}