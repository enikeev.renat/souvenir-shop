import axios from '~/middleware/api/instance'

export default {

	getAll(data = {}){
		return axios.post('/api/sale/get-all', {...data}).then(r => r.data)
	},

	getItem(data = {}){
		return axios.post('/api/sale/get', {...data}).then(r => r.data)
	},

	addItem(data = {}){
		return axios.post('/api/sale/new', {...data}).then(r => r.data)
	},

	updateItem(data = {}){
		return axios.post('/api/sale/update', {...data}).then(r => r.data)
	},

	deleteItem(data = {}){
		return axios.post('/api/sale/delete', {...data}).then(r => r.data)
	},

}