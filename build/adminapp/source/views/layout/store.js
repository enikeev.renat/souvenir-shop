import client from '~/middleware/api'

export default {
	namespaced: true,

	state: {
		authorized: undefined,

		loginForm: {
			login: '',
			password: '',
		},

		userInfo: null,

		statistic: {
			articles: {
				all: 0,
				published: 0,
			},
			feedback: {
				all: 0,
				completed: 0,
			},
			orders: {
				all: 0,
				new: 0,
			},
			products: {
				all: 0,
				published: 0,
			},
			reviews: {
				all: 0,
				published: 0,
			},
			users: {
				all: 0,
				admins: 0,
			},
		},

		navigation: [
			{
				name: 'Пользователи',
				nav: [
					{
						name: 'Пользователи',
						route: '/user',
						icon: 'account-tie',
						access: ['1'],
					},
				]
			},
			{
				name: 'Контент',
				nav: [
					{
						name: 'Упражнения',
						route: '/exercises',
						icon: 'newspaper-variant',
						access: ['2'],
					},
				]
			},
		],

	},

	mutations: {
		SET_AUTH_STATUS(state, value){
			state.authorized = value;
		},

		SET_LOGIN_PROP(state, data){
			state.loginForm[data.name] = data.value;
		},

		SET_USER_INFO(state, data){
			state.userInfo = data;
		},

		SET_STATISTIC(state, {
			articles,
			feedback,
			orders,
			products,
			reviews,
			users,
		}){
			state.statistic.articles = articles;
			state.statistic.feedback = feedback;
			state.statistic.orders = orders;
			state.statistic.products = products;
			state.statistic.reviews = reviews;
			state.statistic.users = users;
		}
	},

	actions: {
		async checkAuth({commit}){
			try {
				const v = await client.base.checkAuth();
				commit('SET_AUTH_STATUS', true)
			} catch (e) {
				commit('SET_AUTH_STATUS', false)
			}
		},

		async logIn({commit, state}){

			try {
				await client.base.logIn(state.loginForm);
				commit('SET_AUTH_STATUS', true)
			} catch (e) {
				commit('SET_AUTH_STATUS', false)
			}
		},

		async logOut({commit}){
			await client.base.logOut();
			commit('SET_AUTH_STATUS', false)
		},

		async userInfo({commit}){
			const {item} = await client.base.userInfo();
			commit('SET_USER_INFO', item);
		},

		async getRoles(){
			await client.base.getRoles();
		},

		async getStatistic({commit}){
			const value = await client.base.getStatistic();
			commit('SET_STATISTIC', value)
		},

		async clearCache(){
			return await client.base.clearCache();
		},

	},

	getters: {
		navigationMenu: state => {
			return state.navigation;
		},

		statistic: state => {
			return state.statistic
		},

		isSuper: state => {
			return state.userInfo && state.userInfo.roles && state.userInfo.roles.length && state.userInfo.roles.includes('ROLE_GOD')
		},


	}
}