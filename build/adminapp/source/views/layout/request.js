import axios from '~/middleware/api/instance'

export default {
	checkAuth() {
		return axios.get('/api/auth/check').then(r => r.data)
	},
	logIn(data) {
		return axios.post('/api/auth/login', {...data}).then(r => r.data)
	},
	logOut() {
		return axios.post('/api/auth/logout').then(r => r.data)
	},
	userInfo() {
		return axios.get('/api/user/info').then(r => r.data)
	},
	uploadImage(data, progress = ()=>{}) {
		return axios.post('/api/service/upload-image', {...data}, {onUploadProgress: progress}).then(r => r.data)
	},
	uploadFile(data, progress = ()=>{}) {
		console.error(`method uploadFile doesn't exist`)
		//return axios.post('', {action: 'upload_image', ...data}, {onUploadProgress: progress}).then(r => r.data)
	},
	clearCache() {
		return axios.get('/api/service/clear-cache').then(r => r.data)
	},
	getStatistic() {
		return axios.get('/api/service/statistic').then(r => r.data)
	},
	getRoles() {
		return axios.get('/api/user/get-roles').then(r => r.data)
	},

}