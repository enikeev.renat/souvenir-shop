export default {

	section: 'product',

	names: {
		createStatus: 'Новый продукт',
		updateStatus: 'Редактирование продукта',
		addBtn: 'Добавить продукт',
		saveBtn: 'Сохранить продукт',
		deleteBtn: 'Удалить продукт',
		deleteQuestion: 'Вы уверены, что хотите удалить этот продукт?',
	},

	message: {
		created: 'Продукт создан',
		updated: 'Продукт обновлен',
		deleted: 'Продукт удален',
		clean: 'В разделе еще не добавлено ни одного продукта'
	},

	route: {
		table: '/product',
		add: '/product/add',
		pagination: true
	}
}