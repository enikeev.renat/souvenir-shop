import {state, actions, mutations, getters} from '~/middleware/store/util'
import options from './options'

const defaultItem = (responseItem) => {

	const product_gallery = responseItem.productGallery ?
		responseItem.productGallery.map(item => {
			return {
				filename: item.visual.filename,
				thumb: item.visual.urls.small,
			}
		}) : [];

	const materials = responseItem.materials ?
		responseItem.materials.map(item => {
			return item.id
		}) : [];

	return {
		id: responseItem.id || undefined,
		name: responseItem.name || '',
		vendor_code: responseItem.vendorCode || '',
		description: responseItem.description || '',
		price: responseItem.price || 0,
		available: responseItem.available || 0,
		published: responseItem.published === undefined || responseItem.published,
		product_gallery,
		category_id: responseItem.category ? responseItem.category.id : undefined,
		materials,
		tags: responseItem.tags ? responseItem.tags.map(item => item.id) : [],
	}
};

export default {
	namespaced: true,

	state: {
		...state(defaultItem)
	},

	mutations: {
		...mutations(defaultItem)
	},

	actions: {
		...actions(options.section)
	},

	getters: {
		...getters()
	}
}