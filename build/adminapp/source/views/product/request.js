import axios from '~/middleware/api/instance'

export default {

	getAll(data = {}){
		return axios.post('/api/product/get-all', {...data}).then(r => r.data)
	},

	getItem(data = {}){
		return axios.post('/api/product/get', {...data}).then(r => r.data)
	},

	addItem(data = {}){
		return axios.post('/api/product/new', {...data}).then(r => r.data)
	},

	updateItem(data = {}){
		return axios.post('/api/product/update', {...data}).then(r => r.data)
	},

	deleteItem(data = {}){
		return axios.post('/api/product/delete', {...data}).then(r => r.data)
	},

}