export default {

	section: 'order',

	names: {
		updateStatus: 'Обработать заказ',
		saveBtn: 'Сохранить заказ',
		deleteBtn: 'Удалить заказ',
		deleteQuestion: 'Вы уверены, что хотите удалить этот заказ?',
	},

	message: {
		updated: 'Заказ сохранен',
		deleted: 'Заказ удален',
		clean: 'Нет заказов с сайта'
	},

	route: {
		table: '/order',
		pagination: true
	}
}