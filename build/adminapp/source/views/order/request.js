import axios from '~/middleware/api/instance'

export default {

	getAll(data = {}){
		return axios.post('/api/order/get-all', {...data}).then(r => r.data)
	},

	getItem(data = {}){
		return axios.post('/api/order/get', {...data}).then(r => r.data)
	},

	addItem(data = {}){
		return axios.post('/api/order/new', {...data}).then(r => r.data)
	},

	updateItem(data = {}){
		return axios.post('/api/order/update', {...data}).then(r => r.data)
	},

	deleteItem(data = {}){
		return axios.post('/api/order/delete', {...data}).then(r => r.data)
	},

	getStatuses(){
		return axios.post('/api/order/get-statuses').then(r => r.data)
	},

}