import {state, actions, mutations, getters} from '~/middleware/store/util'
import options from './options'
import client from '~/middleware/api'

const defaultItem = (responseItem) => {
	return {
		id: responseItem.id || undefined,
		description: responseItem.description || '',
		status: responseItem.status ? responseItem.status.id : 0,
	}
};

export default {
	namespaced: true,

	state: {
		...state(defaultItem),

		statuses: null,
	},

	mutations: {
		...mutations(defaultItem),

		SET_STATUSES(state, items){
			state.statuses = items
		}
	},

	actions: {
		...actions(options.section),

		async getStatuses({commit, state}){
			if (state.statuses) return;
			const {items} = await client.order.getStatuses();
			commit('SET_STATUSES', items);
		}
	},

	getters: {
		...getters(),

		statuses: state => state.statuses
	}
}