import {state, actions, mutations, getters} from '~/middleware/store/util'
import options from './options'

const defaultItem = (responseItem) => {

	const review_gallery = responseItem.reviewGallery ?
		responseItem.reviewGallery.map(item => {
			return {
				filename: item.visual.filename,
				thumb: item.visual.urls.small,
			}
		}) : [];

	return {
		id: responseItem.id || undefined,
		author: responseItem.author || '',
		text: responseItem.text || '',
		added: responseItem.added || '',
		published: responseItem.published === undefined || responseItem.published,
		review_gallery,
	}
};

export default {
	namespaced: true,

	state: {
		...state(defaultItem)
	},

	mutations: {
		...mutations(defaultItem)
	},

	actions: {
		...actions(options.section)
	},

	getters: {
		...getters()
	}
}