export default {

	section: 'review',

	names: {
		createStatus: 'Новый отзыв',
		updateStatus: 'Редактирование отзыва',
		addBtn: 'Добавить отзыв',
		saveBtn: 'Сохранить отзыв',
		deleteBtn: 'Удалить отзыв',
		deleteQuestion: 'Вы уверены, что хотите удалить этот отзыв?',
	},

	message: {
		created: 'Отзыв создан',
		updated: 'Отзыв обновлен',
		deleted: 'Отзыв удален',
		clean: 'В разделе еще не добавлено отзывов'
	},

	route: {
		table: '/review',
		add: '/review/add'
	}
}