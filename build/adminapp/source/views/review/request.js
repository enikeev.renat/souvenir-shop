import axios from '~/middleware/api/instance'

export default {

	getAll(data = {}){
		return axios.post('/api/review/get-all', {...data}).then(r => r.data)
	},

	getItem(data = {}){
		return axios.post('/api/review/get', {...data}).then(r => r.data)
	},

	addItem(data = {}){
		return axios.post('/api/review/new', {...data}).then(r => r.data)
	},

	updateItem(data = {}){
		return axios.post('/api/review/update', {...data}).then(r => r.data)
	},

	deleteItem(data = {}){
		return axios.post('/api/review/delete', {...data}).then(r => r.data)
	},

}