export default {

	section: 'feedback',

	names: {
		updateStatus: 'Обработать обращение',
		saveBtn: 'Сохранить обращение',
		deleteBtn: 'Удалить обращение',
		deleteQuestion: 'Вы уверены, что хотите удалить это обращение?',
	},

	message: {
		updated: 'Обращение сохранено',
		deleted: 'Обращение удалено',
		clean: 'Нет обращений с сайта'
	},

	route: {
		table: '/feedback',
		pagination: true
	}
}