import {state, actions, mutations, getters} from '~/middleware/store/util'
import options from './options'

const defaultItem = (responseItem) => {
	return {
		id: responseItem.id || undefined,
		description: responseItem.description || '',
		completed: responseItem.completed || false,
	}
};

export default {
	namespaced: true,

	state: {
		...state(defaultItem)
	},

	mutations: {
		...mutations(defaultItem)
	},

	actions: {
		...actions(options.section)
	},

	getters: {
		...getters()
	}
}