import axios from '~/middleware/api/instance'

export default {

	getAll(data = {}){
		return axios.post('/api/feedback/get-all', {...data}).then(r => r.data)
	},

	getItem(data = {}){
		return axios.post('/api/feedback/get', {...data}).then(r => r.data)
	},

	addItem(data = {}){
		return axios.post('/api/feedback/new', {...data}).then(r => r.data)
	},

	updateItem(data = {}){
		return axios.post('/api/feedback/update', {...data}).then(r => r.data)
	},

	deleteItem(data = {}){
		return axios.post('/api/feedback/delete', {...data}).then(r => r.data)
	},

}