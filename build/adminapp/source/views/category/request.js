import axios from '~/middleware/api/instance'

export default {

	getAll(){
		return axios.get('/api/category/get-all').then(r => r.data)
	},

	getTree(){
		return axios.get('/api/category/get-tree').then(r => r.data)
	},

	getItem(data = {}){
		return axios.post('/api/category/get', {...data}).then(r => r.data)
	},

	addItem(data = {}){
		return axios.post('/api/category/new', {...data}).then(r => r.data)
	},

	updateItem(data = {}){
		return axios.post('/api/category/update', {...data}).then(r => r.data)
	},

	deleteItem(data = {}){
		return axios.post('/api/category/delete', {...data}).then(r => r.data)
	},

}