import {state, actions, mutations, getters} from '~/middleware/store/util'
import options from './options'

const defaultItem = (responseItem) => {

	const visual = responseItem.visual ? [{
		filename: responseItem.visual.filename,
		thumb: responseItem.visual.urls.small,
	}] : [];

	return {
		id: responseItem.id || undefined,
		name: responseItem.name || '',
		description: responseItem.description || '',
		parent_id: responseItem.parent ? responseItem.parent.id : undefined,
		visual,
	}
};

export default {
	namespaced: true,

	state: {
		...state(defaultItem)
	},

	mutations: {
		...mutations(defaultItem)
	},

	actions: {
		...actions(options.section)
	},

	getters: {
		...getters()
	}
}