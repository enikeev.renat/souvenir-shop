export default {

	section: 'category',

	names: {
		createStatus: 'Новая категория',
		updateStatus: 'Редактирование категории',
		addBtn: 'Добавить категорию',
		saveBtn: 'Сохранить категорию',
		deleteBtn: 'Удалить категорию',
		deleteQuestion: 'Вы уверены, что хотите удалить эту категорию?',
	},

	message: {
		created: 'Категория создана',
		updated: 'Категория обновлена',
		deleted: 'Категория удалена',
		clean: 'В разделе еще не добавлено ни одной категории'
	},

	route: {
		table: '/category',
		add: '/category/add',
		pagination: false
	}
}