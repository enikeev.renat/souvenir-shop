export default {

	section: 'material',

	names: {
		createStatus: 'Новый материал',
		updateStatus: 'Редактирование материала',
		addBtn: 'Добавить материал',
		saveBtn: 'Сохранить материал',
		deleteBtn: 'Удалить материал',
		deleteQuestion: 'Вы уверены, что хотите удалить этот материал?',
	},

	message: {
		created: 'Материал создан',
		updated: 'Материал обновлен',
		deleted: 'Материал удален',
		clean: 'В разделе еще не добавлено материалов'
	},

	route: {
		table: '/material',
		add: '/material/add'
	}
}