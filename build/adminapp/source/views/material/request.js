import axios from '~/middleware/api/instance'

export default {

	getAll(data = {}){
		return axios.post('/api/material/get-all', {...data}).then(r => r.data)
	},

	getItem(data = {}){
		return axios.post('/api/material/get', {...data}).then(r => r.data)
	},

	addItem(data = {}){
		return axios.post('/api/material/new', {...data}).then(r => r.data)
	},

	updateItem(data = {}){
		return axios.post('/api/material/update', {...data}).then(r => r.data)
	},

	deleteItem(data = {}){
		return axios.post('/api/material/delete', {...data}).then(r => r.data)
	},

}