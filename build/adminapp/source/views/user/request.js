import axios from '~/middleware/api/instance'

export default {

	getAll(data = {}){
		return axios.post('/api/user/get-all', {...data}).then(r => r.data)
	},

	getItem(data = {}){
		return axios.post('/api/user/get', {...data}).then(r => r.data)
	},

	addItem(data = {}){
		return axios.post('/api/user/new', {...data}).then(r => r.data)
	},

	updateItem(data = {}){
		return axios.post('/api/user/update', {...data}).then(r => r.data)
	},

	deleteItem(data = {}){
		return axios.post('/api/user/delete', {...data}).then(r => r.data)
	},

}