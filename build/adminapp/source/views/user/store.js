import {state, actions, mutations, getters} from '~/middleware/store/util'
import options from './options'

const defaultItem = (responseItem) => {
	return {
		id: responseItem.id || undefined,
		email: responseItem.email || '',
		mobile: responseItem.mobile || '',
		address: responseItem.address || '',
		fio: responseItem.fio || '',
		roles: responseItem.roles || [],
	}
};

export default {
	namespaced: true,

	state: {
		...state(defaultItem)
	},

	mutations: {
		...mutations(defaultItem)
	},

	actions: {
		...actions(options.section)
	},

	getters: {
		...getters()
	}
}