export default {

	section: 'user',

	names: {
		createStatus: 'Новый пользователь',
		updateStatus: 'Редактирование пользователя',
		addBtn: 'Добавить пользователя',
		saveBtn: 'Сохранить пользователя',
		deleteBtn: 'Удалить учетную запись',
		deleteQuestion: 'Вы уверены, что хотите удалить доступы этого пользователя?',
	},

	message: {
		created: 'Пользователь создан',
		updated: 'Пользователь обновлен',
		deleted: 'Доступы пользователя удалены',
		clean: ''
	},

	route: {
		table: '/user',
		add: '/user/add',
		pagination: true
	}
}