export default {

	section: 'article',

	names: {
		createStatus: 'Новая статья',
		updateStatus: 'Редактирование статью',
		addBtn: 'Добавить статью',
		saveBtn: 'Сохранить статью',
		deleteBtn: 'Удалить статью',
		deleteQuestion: 'Вы уверены, что хотите удалить эту статью?',
	},

	message: {
		created: 'Статья создана',
		updated: 'Статья обновлена',
		deleted: 'Статья удалена',
		clean: 'В разделе еще не добавлено ни одной статьи'
	},

	route: {
		table: '/article',
		add: '/article/add',
		pagination: true
	}
}