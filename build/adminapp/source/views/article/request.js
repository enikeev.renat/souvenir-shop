import axios from '~/middleware/api/instance'

export default {

	getAll(data = {}){
		return axios.post('/api/article/get-all', {...data}).then(r => r.data)
	},

	getItem(data = {}){
		return axios.post('/api/article/get', {...data}).then(r => r.data)
	},

	addItem(data = {}){
		return axios.post('/api/article/new', {...data}).then(r => r.data)
	},

	updateItem(data = {}){
		return axios.post('/api/article/update', {...data}).then(r => r.data)
	},

	deleteItem(data = {}){
		return axios.post('/api/article/delete', {...data}).then(r => r.data)
	},

}