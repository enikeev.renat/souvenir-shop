import {state, actions, mutations, getters} from '~/middleware/store/util'
import options from './options'

const defaultItem = (responseItem) => {

	const visual = responseItem.visual ? [{
		filename: responseItem.visual.filename,
		thumb: responseItem.visual.urls.small,
	}] : [];

	const article_gallery = responseItem.articleGallery ?
		responseItem.articleGallery.map(item => {
			return {
				filename: item.visual.filename,
				thumb: item.visual.urls.small,
			}
		}) :
		[]

	return {
		id: responseItem.id || undefined,
		title: responseItem.title || '',
		description: responseItem.description || '',
		content: responseItem.content || '',
		visual,
		article_gallery: article_gallery,
		published: responseItem.published !== undefined ? responseItem.published : true,
		tags: responseItem.tags ? responseItem.tags.map(item => item.id) : [],
	}
};

export default {
	namespaced: true,

	state: {
		...state(defaultItem)
	},

	mutations: {
		...mutations(defaultItem)
	},

	actions: {
		...actions(options.section)
	},

	getters: {
		...getters()
	}
}