import '~/assets/scss/styles.scss'

try{console.log('%cdeveloped by %c soft.maestros.ru ','font:bold 12px/18px monospace;color:green','font:12px/18px monospace;color:white;background-color:green;border-radius:3px;');}catch(e){}

import Vue from 'vue';
import store from '~/middleware/store'
import router from '~/middleware/router'
import Index from './views/layout';

//Vue.config.productionTip = false;
Vue.config.devtools = true;


import "~/middleware/filters";
import '~/components';

import '~/middleware/lib';

import '~/middleware/api/fake-api';

if ( document.getElementById('app') ){
	new Vue({
		router,
		store,
		render: h => h(Index)
	}).$mount('#app');
}
