
import Vue from 'vue'
import Vuex from 'vuex'

import base from '~/views/layout/store'
import collection from './collection'
import product from '~/views/product/store'
import article from '~/views/article/store'
import category from '~/views/category/store'
import user from '~/views/user/store'
import material from '~/views/material/store'
import sale from '~/views/sale/store'
import tag from '~/views/tag/store'
import feedback from '~/views/feedback/store'
import order from '~/views/order/store'
import review from '~/views/review/store'

Vue.use(Vuex);

export default new Vuex.Store({
	modules: {
		base,
		collection,
		product,
		article,
		category,
		user,
		material,
		sale,
		tag,
		feedback,
		order,
		review,
	}
})


