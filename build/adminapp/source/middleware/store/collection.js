import client from '~/middleware/api'
import arrayToTree from 'array-to-tree'

export default {
	namespaced: true,

	state: {
		tag: [],
		material: [],
		category: [],
		role: [],
	},

	mutations: {
		SET_COLLECTION(state, {name, items}){
			state[name] = items;
		},
	},

	actions: {

		async tagCollection({state, commit}, forceUpdate = true){
			if (!forceUpdate && state.tag.length) return;
			const value = await client.tag.getAll();
			commit('SET_COLLECTION', {
				name: 'tag',
				items: value.items
			})
		},

		async materialCollection({state, commit}, forceUpdate = true){
			if (!forceUpdate && state.material.length) return;
			const value = await client.material.getAll();
			commit('SET_COLLECTION', {
				name: 'material',
				items: value.items
			})
		},

		async categoryCollection({state, commit}, forceUpdate = true){
			if (!forceUpdate && state.category.length) return;
			const value = await client.category.getAll();
			commit('SET_COLLECTION', {
				name: 'category',
				items: value.items
			})
		},

		async roleCollection({state, commit}, forceUpdate = true){
			if (!forceUpdate && state.role.length) return;
			const value = await client.base.getRoles();
			commit('SET_COLLECTION', {
				name: 'role',
				items: value.items
			})
		},

	},

	getters: {
		tagItems: state => {
			return state.tag || [];
		},

		materialItems: state => {
			return state.material || [];
		},

		categoryItems: state => {
			return state.category || [];
		},

		categoryTree: state => {
			return arrayToTree(state.category.map(item => {
				return {
					id: item.id,
					label: item.name,
					description: item.description,
					visual: item.visual,
					parent: item.parent ? item.parent.id : null,
				}
			}), {
				parentProperty: 'parent',
				customID: 'id'
			});
		},

		categoryCascade: state => {
			return arrayToTree(state.category.map(item => {
				return {
					value: item.id,
					label: item.name,
					parent: item.parent ? item.parent.id : null,
				}
			}), {
				parentProperty: 'parent',
				customID: 'value'
			});
		},

		categoryParent: state => disableId => {
			return arrayToTree(state.category.map(item => {
				return {
					value: item.id,
					disabled: disableId ? item.id.toString() === disableId.toString() : false,
					label: item.name,
					parent: item.parent ? item.parent.id : null,
				}
			}), {
				parentProperty: 'parent',
				customID: 'value'
			});
		},

		roleItems: state => {
			return state.role || [];
		},

	}
}