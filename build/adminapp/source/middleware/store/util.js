import { getField, updateField } from 'vuex-map-fields';
import client from '~/middleware/api'

function updateObject(obj1, obj2) {
	const base = {};
	Object.keys(obj1).forEach(name => {
		base[name] = obj2[name] !== undefined ? obj2[name] : obj1[name];
	});
	return base;
}

export const state = (defaultItem) => {
	return {
		itemAll: {},

		item: defaultItem({}),

		cast: defaultItem({}),

		filter: {
			limit: 10,
			total: undefined,
			page: 1,
			sortName: undefined,
			sortDirection: undefined,
			query: undefined,
		},

		list: [],

		loading: false,
	}
};

export const mutations = (defaultItem) => {
	return {
		updateField,

		SET_LIST(state, value){
			const items = value ? value.items || [] : []
			const total = value ? value.total || 0 : 0
			state.list = [...items];
			state.filter.total = total;
		},

		SET_ITEM(state, item = {}){
			state.itemAll = item;
			state.item = updateObject(defaultItem({}), defaultItem(item));
			state.cast = updateObject(defaultItem({}), defaultItem(item));
		},

		SET_FILTER(state, obj){
			state.filter[obj.name] = obj.value;
		},

		SET_LOADING(state, value){
			state.loading = value;
		},
	}
};

export const actions = (section) => {
	return {
		async getAll({commit, state}, page){
			commit('SET_LOADING', true);
			try {
				let query = undefined;
				if ( page ) {
					query = {
						sortName: state.filter.sortName,
						sortDirection: state.filter.sortDirection,
						query: state.filter.query,
						limit: state.filter.limit,
						offset: (page - 1) * state.filter.limit
					}
					state.filter.page = parseInt(page);
				} else {
					commit('SET_LIST');
				}
				const value = await client[section].getAll(query);
				commit('SET_LIST', value);
			} catch (e) {
				commit('SET_LIST');
			} finally {
				commit('SET_LOADING', false);
			}
		},

		async getItem({commit}, id){
			commit('SET_ITEM');
			if (id){
				commit('SET_LOADING', true);
				try {
					const value = await client[section].getItem({id});
					commit('SET_ITEM', value.item);
				} finally {
					commit('SET_LOADING', false);
				}
			}
		},

		async addItem({state, commit, dispatch}){
			commit('SET_LOADING', true);
			try {
				await client[section].addItem(state.item);
				dispatch('getAll', state.filter.page);
			} finally {
				commit('SET_LOADING', false)
			}
		},

		async updateItem({state, commit, dispatch}){
			commit('SET_LOADING', true);
			try {
				await client[section].updateItem(state.item);
				dispatch('getAll', state.filter.page);
			} finally {
				commit('SET_LOADING', false);
			}
		},

		async deleteItem({commit, dispatch, state}, id){
			commit('SET_LOADING', true);
			try {
				await client[section].deleteItem({id});
				dispatch('getAll', state.filter.page);
			} finally {
				commit('SET_LOADING', false);
			}
		},
	}
};


export const getters = () => {
	return {
		getField,

		changed: state => {
			return JSON.stringify(state.item) !== JSON.stringify(state.cast)
		},

		items: state => {
			return state.list;
		},

		loading: state => state.loading,
	}
};