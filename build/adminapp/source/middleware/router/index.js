import Vue from 'vue'
import Router from 'vue-router'
import routes from './routes'

Vue.use(Router);

const router = new Router({
	base: IS_LOCAL_ENV ? '/' : '/admin/',
	mode: IS_LOCAL_ENV ? 'hash' : 'history',
	routes,
	linkActiveClass: 'is-active',
	linkExactActiveClass: 'is-active',
	transitionOnLoad: true,
	root: '/'
});

const originalPush = Router.prototype.push;
Router.prototype.push = function push(location) {
	return originalPush.call(this, location).catch(err => err);
};

router.afterEach((to, from) => {
	document.title = to.meta.title;
	window.scrollTo(0, 0);
});

export default router