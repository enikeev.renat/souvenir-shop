

export default [
	{
		path: '/',
		redirect: '/home'
	},

	{
		path: '/home',
		component: () => import('../../views/home/index.vue').then(m => m.default),
		name: 'home',
		meta: {
			title: 'Главная'
		}
	},

	{
		path: '/user/:id?',
		component: () => import('../../views/user/index.vue').then(m => m.default),
		name: 'user',
		meta: {
			title: 'Пользователи'
		}
	},

	{
		path: '/article/:id?',
		component: () => import('../../views/article/index.vue').then(m => m.default),
		name: 'article',
		meta: {
			title: 'Статьи'
		}
	},

	{
		path: '/category/:id?',
		component: () => import('../../views/category/index.vue').then(m => m.default),
		name: 'category',
		meta: {
			title: 'Категории'
		}
	},

	{
		path: '/material/:id?',
		component: () => import('../../views/material/index.vue').then(m => m.default),
		name: 'material',
		meta: {
			title: 'Материалы'
		}
	},

	{
		path: '/tag/:id?',
		component: () => import('../../views/tag/index.vue').then(m => m.default),
		name: 'tag',
		meta: {
			title: 'Теги'
		}
	},

	{
		path: '/sale/:id?',
		component: () => import('../../views/sale/index.vue').then(m => m.default),
		name: 'sale',
		meta: {
			title: 'Скидки'
		}
	},

	{
		path: '/product/:id?',
		component: () => import('../../views/product/index.vue').then(m => m.default),
		name: 'product',
		meta: {
			title: 'Продукты'
		}
	},

	{
		path: '/feedback/:id?',
		component: () => import('../../views/feedback/index.vue').then(m => m.default),
		name: 'feedback',
		meta: {
			title: 'Обратная связь'
		}
	},

	{
		path: '/order/:id?',
		component: () => import('../../views/order/index.vue').then(m => m.default),
		name: 'order',
		meta: {
			title: 'Заказы'
		}
	},

	{
		path: '/review/:id?',
		component: () => import('../../views/review/index.vue').then(m => m.default),
		name: 'review',
		meta: {
			title: 'Отзывы'
		}
	},

	{
		path: '*/*',
		component: () => import('../../views/error.vue').then(m => m.default),
		name: 'error',
		meta: {
			title: 'Страница не существует'
		}
	}
];

