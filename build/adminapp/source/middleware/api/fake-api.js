import instance from './instance'
import MockAdapter from 'axios-mock-adapter'

const fakeServer = () => {

	const mock = new MockAdapter(instance);

	mock.onGet('/api/auth/check').reply((config) => {
		return [200, {
			errors: null,
		}]
	});

	mock.onGet('/api/user/info').reply((config) => {
		return [200, {
			errors: null,
			item: {
				id: 1,
				email: "admin@admin.ru",
				username: "admin@admin.ru",
				roles: ["ROLE_GOD","ROLE_USER"],
				password: "$argon2id$v=19$m=65536,t=4,p=1$aiiEmsWjFKqRJVyctO7LuA$Sp8pJENgEKhSxiLSbPbpvnQPQzuNAQZVbc64d2gPmoM",
				salt: null
			}
		}]
	});

	mock.onPost('/api/auth/logout').reply((config) => {
		return [200, {
			errors: null,
		}]
	});

	mock.onPost('/api/auth/login').reply((config) => {
		return [200, {
			errors: null,
		}]
	});

	mock.onPost('/api/service/upload-image').reply((config) => {
		return [200, {
			errors: null,
			item: {
				filename: `${Math.floor(Math.random() * 10000) }.jpeg`,
				urls: {
					original: "https://sun9-48.userapi.com/uFW4J9IcqLKCBv4ouo6UTl2gwgo4W6YYaBcF1A/BIzs_VNhbHQ.jpg",
					small: "https://sun9-48.userapi.com/uFW4J9IcqLKCBv4ouo6UTl2gwgo4W6YYaBcF1A/BIzs_VNhbHQ.jpg"
				}
			}
		}]
	});

	/**
	 * tag
	 */


	mock.onPost('/api/tag/get-all').reply((config) => {
		const items = [];
		for (let i = 1; i <= 10; i++){
			items.push({
				id: i,
				name: `тег_${i}`,
				description: "Описание тега"
			})
		}

		return [200, {
			errors: null,
			items,
			total: 100
		}]
	});

	mock.onPost('/api/tag/get').reply((config) => {
		return [200, {
			errors:null,
			item:{
				id:1,
				name:"новинка",
				description:"Новые поступления",
				products:[]
			}
		}]
	});

	mock.onPost('/api/tag/new').reply((config) => {
		return [200, {errors:null,id:Math.floor(Math.random() * 10000)}]
	});

	mock.onPost('/api/tag/update').reply((config) => {
		return [200, {errors:null}]
	});

	mock.onPost('/api/tag/delete').reply((config) => {
		return [200, {errors:null}]
	});

	/**
	 * materials
	 */


	mock.onPost('/api/material/get-all').reply((config) => {
		const items = [];
		for (let i = 1; i <= 10; i++){
			items.push({
				id: i,
				name: `материал_${i}`,
				description: "Описание материала",
				visualFilename: "5f3fc565007c5.jpeg",
				visual: {
					filename: "5f3fc565007c5.jpeg",
					urls: {
						original:"https:\/\/suvenir.maestros.ru\/assets\/uploads\/images\/5f\/3f\/5f3fc565007c5.jpeg",
						small:"https:\/\/suvenir.maestros.ru\/media\/cache\/small\/assets\/uploads\/images\/5f\/3f\/5f3fc565007c5.jpeg"
					}
				}
			})
		}

		return [200, {
			errors: null,
			items,
			total: 100
		}]
	});

	mock.onPost('/api/material/get').reply((config) => {
		return [200, {
			"errors":null,
			"item":{
				"id":7,
				"name":"rotate",
				"description":"sdfdf",
				"visualFilename":"5f3fb1377f8bb.jpeg",
				"visual":{
					"filename":"5f3fb1377f8bb.jpeg",
					"urls":{
						"original":"https:\/\/suvenir.maestros.ru\/assets\/uploads\/images\/5f\/3f\/5f3fb1377f8bb.jpeg",
						"small":"https:\/\/suvenir.maestros.ru\/media\/cache\/small\/assets\/uploads\/images\/5f\/3f\/5f3fb1377f8bb.jpeg"
					}
				}
			}
		}]
	});

	/**
	 * article
	 */

	mock.onPost('/api/article/get').reply((config) => {
		return [200, {
			errors:null,
			item:{
				id:7,
				slug:"test-1",
				title:"test 1",
				description:"test",
				content:"<p>test<\/p>",
				visual:{
					filename:"5f3fee4885f10.jpeg",
					urls:{
						original:"https:\/\/suvenir.maestros.ru\/assets\/uploads\/images\/5f\/3f\/5f3fee4885f10.jpeg",
						small:"https:\/\/suvenir.maestros.ru\/media\/cache\/small\/assets\/uploads\/images\/5f\/3f\/5f3fee4885f10.jpeg"
					}},
				published:true,
				articleGallery:[
					{
						visual:{
							filename:"5f3fee542a7f9.png",
							urls:{
								original:"https:\/\/suvenir.maestros.ru\/assets\/uploads\/images\/5f\/3f\/5f3fee542a7f9.png",
								small:"https:\/\/suvenir.maestros.ru\/media\/cache\/small\/assets\/uploads\/images\/5f\/3f\/5f3fee542a7f9.png"
							}
						}
					},
					{
						visual:{
							filename:"5f3fee54808fe.png",
							urls:{
								original:"https:\/\/suvenir.maestros.ru\/assets\/uploads\/images\/5f\/3f\/5f3fee54808fe.png",
								small:"https:\/\/suvenir.maestros.ru\/media\/cache\/small\/assets\/uploads\/images\/5f\/3f\/5f3fee54808fe.png"
							}
						}
					}],
				tags:[]
			}
		}]
	});



	mock.onPost('/api/article/new').reply((config) => {
		return [200, {errors:null}]
	});

	mock.onPost('/api/article/update').reply((config) => {
		return [200, {errors:null}]
	});


	/**
	 * category
	 */


	mock.onGet('/api/category/get-all').reply((config) => {
		return [200, {
			"errors": null,
			"items": [{
				"id": 9,
				"slug": "permogorskaia",
				"name": "ПермогОрская",
				"description": "ПермогОрская роспись",
				"visual": null,
				"parent": {
					"id": 7,
					"slug": "rospis-severa-rossii",
					"name": "Роспись севера России",
					"description": "Роспись севера России",
					"visual": null,
					"parent": {
						"id": 2,
						"slug": "rospis",
						"name": "Роспись",
						"description": "Разная роспись",
						"visual": null,
						"parent": null
					}
				}
			},
				{
					"id": 8,
					"slug": "mezenskaia",
					"name": "МезЕнская",
					"description": "МезЕнская роспись",
					"visual": null,
					"parent": {
						"id": 7,
						"slug": "rospis-severa-rossii",
						"name": "Роспись севера России",
						"description": "Роспись севера России",
						"visual": null,
						"parent": {
							"id": 2,
							"slug": "rospis",
							"name": "Роспись",
							"description": "Разная роспись",
							"visual": null,
							"parent": null
						}
					}
				},
				{
					"id": 7,
					"slug": "rospis-severa-rossii",
					"name": "Роспись севера России",
					"description": "Роспись севера России",
					"visual": null,
					"parent": {
						"id": 2,
						"slug": "rospis",
						"name": "Роспись",
						"description": "Разная роспись",
						"visual": null,
						"parent": null
					}
				},
				{
					"id": 6,
					"slug": "avtorskaia",
					"name": "Авторская",
					"description": "Авторская роспись",
					"visual": null,
					"parent": {
						"id": 2,
						"slug": "rospis",
						"name": "Роспись",
						"description": "Разная роспись",
						"visual": null,
						"parent": null
					}
				},
				{
					"id": 5,
					"slug": "loskutnoe-shite",
					"name": "Лоскутное шитье",
					"description": "Лоскутное шитье",
					"visual": null,
					"parent": null
				},
				{
					"id": 4,
					"slug": "beresta",
					"name": "Береста", "description": "Изделия из бересты", "visual": null, "parent": null
				}, {
					"id": 3,
					"slug": "rezba-po-derevu",
					"name": "Резьба по дереву",
					"description": "Резьба по дереву",
					"visual": {
						"filename": "5f4174fcaa989.jpeg",
						"urls": {
							"original": "https:\/\/suvenir.maestros.ru\/assets\/uploads\/images\/5f\/41\/5f4174fcaa989.jpeg",
							"small": "https:\/\/suvenir.maestros.ru\/media\/cache\/small\/assets\/uploads\/images\/5f\/41\/5f4174fcaa989.jpeg"
						}
					},
					"parent": null
				}, {
					"id": 2,
					"slug": "rospis",
					"name": "Роспись",
					"description": "Разная роспись",
					"visual": null,
					"parent": null
				}, {
					"id": 1,
					"slug": "kukly",
					"name": "Куклы",
					"description": "Разные куклы",
					"visual": null,
					"parent": null
				}]
		}]
	});

	mock.onPost('/api/category/get').reply((config) => {
		return [200, {
			"errors":null,
			"item":{
				"id":3,
				"slug":"rezba-po-derevu",
				"name":"Резьба по дереву",
				"description":"Резьба по дереву",
				"visual":{"filename":"5f4174fcaa989.jpeg","urls":{"original":"https:\/\/suvenir.maestros.ru\/assets\/uploads\/images\/5f\/41\/5f4174fcaa989.jpeg","small":"https:\/\/suvenir.maestros.ru\/media\/cache\/small\/assets\/uploads\/images\/5f\/41\/5f4174fcaa989.jpeg"}},
				"parent": {
					id: 2
				}
			}
		}]
	});

	mock.onPost('/api/category/new').reply((config) => {
		return [200, {errors:null}]
	});

	mock.onPost('/api/category/update').reply((config) => {
		return [200, {errors:null}]
	});

	/**
	 * user
	 */

	mock.onGet('/api/user/get-roles').reply((config) => {
		return [200, {"errors":null,"items":[{"key":"ROLE_ADMIN","value":"Администратор","disabled":false},{"key":"ROLE_GOD","value":"Супер администратор","disabled":true},{"key":"ROLE_USER","value":"Пользователь","disabled":true}]}]
	});

	mock.onPost('/api/user/get-all').reply((config) => {
		return [200, {"errors":null,"items":[{"id":5,"email":"renn@mail.ru","username":"renn@mail.ru","roles":["ROLE_USER"],"password":"$argon2id$v=19$m=65536,t=4,p=1$alNHWXdiUlVISzhrdEVWeA$fMZeL+wlXjKUxzpRzC4Vv\/CERv9cA7ftD4RXZzixIGE","salt":null,"fio":"Ренат Еникеев ","mobile":"","legal":null,"requisites":"","subscribe":false,"address":"","added":"2020-10-31T18:54:54+03:00"},{"id":2,"email":"dan_spb_ru@mail.ru","username":"dan_spb_ru@mail.ru","roles":["ROLE_GOD","ROLE_USER"],"password":"$argon2id$v=19$m=65536,t=4,p=1$LnZISFdYWjVUNk5kUi5hVQ$9BT\/R2fGvQHRGpXSG\/uR2bdRmR+MvMGDAj7bNMiF+HQ","salt":null,"fio":null,"mobile":null,"legal":null,"requisites":null,"subscribe":true,"address":null,"added":"2020-10-27T15:09:31+03:00"},{"id":1,"email":"admin@admin.ru","username":"admin@admin.ru","roles":["ROLE_GOD","ROLE_USER"],"password":"$argon2id$v=19$m=65536,t=4,p=1$zVaqIwuEDjoh0CYdjPM0Cg$M352nkzRDh6+IGqAHU6wmWXjFPt8BlKfiZeCQofDYz8","salt":null,"fio":"Главный На Сайте","mobile":"+7 999 999-99-99","legal":null,"requisites":"","subscribe":false,"address":"","added":"2020-10-27T15:09:31+03:00"}],"total":3}]
	});

	mock.onPost('/api/user/get').reply((config) => {
		return [200, {"errors":null,"item":{"id":1,"email":"admin@admin.ru","username":"admin@admin.ru","roles":["ROLE_GOD","ROLE_USER"],"password":"$argon2id$v=19$m=65536,t=4,p=1$zVaqIwuEDjoh0CYdjPM0Cg$M352nkzRDh6+IGqAHU6wmWXjFPt8BlKfiZeCQofDYz8","salt":null,"fio":"Главный На Сайте","mobile":"+7 999 999-99-99","legal":null,"requisites":"","subscribe":false,"address":"","added":"2020-10-27T15:09:31+03:00"}}]
	});

	/**
	 * sale
	 */

	mock.onPost('/api/sale/get').reply((config) => {
		return [200, {"errors":null,"item":{"id":2,"min":15000,"value":3}}]
	});

	/**
	 * product
	 */

	mock.onPost('/api/product/get').reply((config) => {
		return [200, {"errors":null,"item":{"id":151,"slug":"podveska-30","name":"Подвеска","vendorCode":"036-002","description":"Кукла-подвеска в русском, народном костюме.\r\nВысота - 42 см.;\r\nВес - 250 гр.;","price":1200,"available":10,"published":false,"productGallery":[{"visual":{"filename":"5f722f7a5e24a.jpeg","urls":{"original":"http:\/\/suvenir.maestros.ru\/assets\/uploads\/images\/5f\/72\/5f722f7a5e24a.jpeg","small":"http:\/\/suvenir.maestros.ru\/media\/cache\/small\/assets\/uploads\/images\/5f\/72\/5f722f7a5e24a.jpeg","medium":"http:\/\/suvenir.maestros.ru\/media\/cache\/medium\/assets\/uploads\/images\/5f\/72\/5f722f7a5e24a.jpeg"}}}],"category":{"id":12,"slug":"kukla-v-narodnom-kostiume","name":"Кукла в народном костюме","description":"","visual":null,"parent":{"id":1,"slug":"kukly","name":"Куклы","description":"Разные куклы","visual":null,"parent":null},"children":[]},"materials":[{"id":12,"name":"Крепсатин","description":"Крепсатин","visual":null},{"id":17,"name":"Фарфор","description":"фарфор","visual":null},{"id":18,"name":"Текстиль","description":"текстиль","visual":null},{"id":19,"name":"Бижутерия","description":"бижутерия","visual":null}],"tags":[{"id":11,"name":"Подвеска","description":""}]}}]
	});

	mock.onPost('/api/product/get-all').reply((config) => {
		return [200, {"errors":null,"items":[{"id":207,"slug":"russkaia-krasa-23","name":"Русская краса","vendorCode":"006-016","description":"<p>Кукла в русском, народном костюме. <\/p><p>Высота - 17 сантиметров; <\/p><p>Вес - 50 грамм;<\/p>","price":450,"available":10,"published":true,"productGallery":[{"visual":{"filename":"5f91cca13a722.jpeg","urls":{"original":"http:\/\/suvenir.maestros.ru\/assets\/uploads\/images\/5f\/91\/5f91cca13a722.jpeg","small":"http:\/\/suvenir.maestros.ru\/media\/cache\/small\/assets\/uploads\/images\/5f\/91\/5f91cca13a722.jpeg","medium":"http:\/\/suvenir.maestros.ru\/media\/cache\/medium\/assets\/uploads\/images\/5f\/91\/5f91cca13a722.jpeg"}}}],"category":{"id":12,"slug":"kukla-v-narodnom-kostiume","name":"Кукла в народном костюме","description":"","visual":null,"parent":{"id":1,"slug":"kukly","name":"Куклы","description":"Разные куклы","visual":null,"parent":null},"children":[]},"materials":[{"id":17,"name":"Фарфор","description":"фарфор","visual":null},{"id":18,"name":"Текстиль","description":"текстиль","visual":null},{"id":19,"name":"Бижутерия","description":"бижутерия","visual":null}],"tags":[]},{"id":206,"slug":"russkaia-krasa-22","name":"Русская краса","vendorCode":"006-015","description":"<p>Кукла в русском, народном костюме. <\/p><p>Высота - 19 сантиметров; <\/p><p>Вес - 50 грамм;<\/p>","price":450,"available":10,"published":true,"productGallery":[{"visual":{"filename":"5f91cadec3be7.jpeg","urls":{"original":"http:\/\/suvenir.maestros.ru\/assets\/uploads\/images\/5f\/91\/5f91cadec3be7.jpeg","small":"http:\/\/suvenir.maestros.ru\/media\/cache\/small\/assets\/uploads\/images\/5f\/91\/5f91cadec3be7.jpeg","medium":"http:\/\/suvenir.maestros.ru\/media\/cache\/medium\/assets\/uploads\/images\/5f\/91\/5f91cadec3be7.jpeg"}}}],"category":{"id":12,"slug":"kukla-v-narodnom-kostiume","name":"Кукла в народном костюме","description":"","visual":null,"parent":{"id":1,"slug":"kukly","name":"Куклы","description":"Разные куклы","visual":null,"parent":null},"children":[]},"materials":[{"id":14,"name":"Сатин","description":"Сатин","visual":null},{"id":17,"name":"Фарфор","description":"фарфор","visual":null},{"id":18,"name":"Текстиль","description":"текстиль","visual":null},{"id":19,"name":"Бижутерия","description":"бижутерия","visual":null}],"tags":[]},{"id":205,"slug":"russkaia-krasa-21","name":"Русская краса","vendorCode":"006-014","description":"<p>Кукла в русском, народном костюме. <\/p><p>Высота - 19 сантиметров; <\/p><p>Вес - 50 грамм;<\/p>","price":450,"available":10,"published":true,"productGallery":[{"visual":{"filename":"5f91ca71ea93f.jpeg","urls":{"original":"http:\/\/suvenir.maestros.ru\/assets\/uploads\/images\/5f\/91\/5f91ca71ea93f.jpeg","small":"http:\/\/suvenir.maestros.ru\/media\/cache\/small\/assets\/uploads\/images\/5f\/91\/5f91ca71ea93f.jpeg","medium":"http:\/\/suvenir.maestros.ru\/media\/cache\/medium\/assets\/uploads\/images\/5f\/91\/5f91ca71ea93f.jpeg"}}}],"category":{"id":12,"slug":"kukla-v-narodnom-kostiume","name":"Кукла в народном костюме","description":"","visual":null,"parent":{"id":1,"slug":"kukly","name":"Куклы","description":"Разные куклы","visual":null,"parent":null},"children":[]},"materials":[{"id":17,"name":"Фарфор","description":"фарфор","visual":null},{"id":18,"name":"Текстиль","description":"текстиль","visual":null},{"id":19,"name":"Бижутерия","description":"бижутерия","visual":null}],"tags":[]},{"id":204,"slug":"russkaia-krasa-20","name":"Русская краса","vendorCode":"006-013","description":"<p>Кукла в русском, народном костюме. <\/p><p>Высота - 17,5 сантиметров; <\/p><p>Вес - 50 грамм;<\/p>","price":450,"available":10,"published":true,"productGallery":[{"visual":{"filename":"5f9149968ed6f.jpeg","urls":{"original":"http:\/\/suvenir.maestros.ru\/assets\/uploads\/images\/5f\/91\/5f9149968ed6f.jpeg","small":"http:\/\/suvenir.maestros.ru\/media\/cache\/small\/assets\/uploads\/images\/5f\/91\/5f9149968ed6f.jpeg","medium":"http:\/\/suvenir.maestros.ru\/media\/cache\/medium\/assets\/uploads\/images\/5f\/91\/5f9149968ed6f.jpeg"}}}],"category":{"id":12,"slug":"kukla-v-narodnom-kostiume","name":"Кукла в народном костюме","description":"","visual":null,"parent":{"id":1,"slug":"kukly","name":"Куклы","description":"Разные куклы","visual":null,"parent":null},"children":[]},"materials":[{"id":14,"name":"Сатин","description":"Сатин","visual":null},{"id":17,"name":"Фарфор","description":"фарфор","visual":null},{"id":18,"name":"Текстиль","description":"текстиль","visual":null},{"id":19,"name":"Бижутерия","description":"бижутерия","visual":null}],"tags":[]},{"id":203,"slug":"russkaia-krasa-19","name":"Русская краса","vendorCode":"006-012","description":"<p>Кукла в русском, народном костюме. <\/p><p>Высота - 19 сантиметров; <\/p><p>Вес - 50 грамм;<\/p>","price":450,"available":10,"published":true,"productGallery":[{"visual":{"filename":"5f9144101dcfa.jpeg","urls":{"original":"http:\/\/suvenir.maestros.ru\/assets\/uploads\/images\/5f\/91\/5f9144101dcfa.jpeg","small":"http:\/\/suvenir.maestros.ru\/media\/cache\/small\/assets\/uploads\/images\/5f\/91\/5f9144101dcfa.jpeg","medium":"http:\/\/suvenir.maestros.ru\/media\/cache\/medium\/assets\/uploads\/images\/5f\/91\/5f9144101dcfa.jpeg"}}}],"category":{"id":12,"slug":"kukla-v-narodnom-kostiume","name":"Кукла в народном костюме","description":"","visual":null,"parent":{"id":1,"slug":"kukly","name":"Куклы","description":"Разные куклы","visual":null,"parent":null},"children":[]},"materials":[{"id":17,"name":"Фарфор","description":"фарфор","visual":null},{"id":18,"name":"Текстиль","description":"текстиль","visual":null},{"id":19,"name":"Бижутерия","description":"бижутерия","visual":null}],"tags":[]},{"id":202,"slug":"russkaia-krasa-18","name":"Русская краса","vendorCode":"006-011","description":"<p>Кукла в русском, народном костюме. <\/p><p>Высота - 17,5 сантиметров;<\/p><p>Вес - 50 грамм;<\/p>","price":450,"available":10,"published":true,"productGallery":[{"visual":{"filename":"5f9143970ea0c.png","urls":{"original":"http:\/\/suvenir.maestros.ru\/assets\/uploads\/images\/5f\/91\/5f9143970ea0c.png","small":"http:\/\/suvenir.maestros.ru\/media\/cache\/small\/assets\/uploads\/images\/5f\/91\/5f9143970ea0c.png","medium":"http:\/\/suvenir.maestros.ru\/media\/cache\/medium\/assets\/uploads\/images\/5f\/91\/5f9143970ea0c.png"}}}],"category":{"id":12,"slug":"kukla-v-narodnom-kostiume","name":"Кукла в народном костюме","description":"","visual":null,"parent":{"id":1,"slug":"kukly","name":"Куклы","description":"Разные куклы","visual":null,"parent":null},"children":[]},"materials":[{"id":17,"name":"Фарфор","description":"фарфор","visual":null},{"id":18,"name":"Текстиль","description":"текстиль","visual":null},{"id":19,"name":"Бижутерия","description":"бижутерия","visual":null}],"tags":[]},{"id":201,"slug":"russkaia-krasa-17","name":"Русская краса","vendorCode":"006-010","description":"<p>Кукла в русском, народном костюме.<\/p><p>Высота - 15 сантиметров; <\/p><p>Вес - 50 грамм;<\/p>","price":450,"available":10,"published":true,"productGallery":[{"visual":{"filename":"5f9142a90ef96.jpeg","urls":{"original":"http:\/\/suvenir.maestros.ru\/assets\/uploads\/images\/5f\/91\/5f9142a90ef96.jpeg","small":"http:\/\/suvenir.maestros.ru\/media\/cache\/small\/assets\/uploads\/images\/5f\/91\/5f9142a90ef96.jpeg","medium":"http:\/\/suvenir.maestros.ru\/media\/cache\/medium\/assets\/uploads\/images\/5f\/91\/5f9142a90ef96.jpeg"}}}],"category":{"id":12,"slug":"kukla-v-narodnom-kostiume","name":"Кукла в народном костюме","description":"","visual":null,"parent":{"id":1,"slug":"kukly","name":"Куклы","description":"Разные куклы","visual":null,"parent":null},"children":[]},"materials":[{"id":17,"name":"Фарфор","description":"фарфор","visual":null},{"id":18,"name":"Текстиль","description":"текстиль","visual":null},{"id":19,"name":"Бижутерия","description":"бижутерия","visual":null}],"tags":[]},{"id":200,"slug":"russkaia-krasa-16","name":"Русская краса","vendorCode":"006-009","description":"<p>Кукла в русском, народном костюме. <\/p><p>Высота - 16,5 сантиметров; <\/p><p>Вес - 50 грамм;<\/p>","price":450,"available":10,"published":true,"productGallery":[{"visual":{"filename":"5f89fde63a21c.jpeg","urls":{"original":"http:\/\/suvenir.maestros.ru\/assets\/uploads\/images\/5f\/89\/5f89fde63a21c.jpeg","small":"http:\/\/suvenir.maestros.ru\/media\/cache\/small\/assets\/uploads\/images\/5f\/89\/5f89fde63a21c.jpeg","medium":"http:\/\/suvenir.maestros.ru\/media\/cache\/medium\/assets\/uploads\/images\/5f\/89\/5f89fde63a21c.jpeg"}}}],"category":{"id":12,"slug":"kukla-v-narodnom-kostiume","name":"Кукла в народном костюме","description":"","visual":null,"parent":{"id":1,"slug":"kukly","name":"Куклы","description":"Разные куклы","visual":null,"parent":null},"children":[]},"materials":[{"id":11,"name":"Портьера","description":"Портьера","visual":null},{"id":17,"name":"Фарфор","description":"фарфор","visual":null},{"id":18,"name":"Текстиль","description":"текстиль","visual":null},{"id":19,"name":"Бижутерия","description":"бижутерия","visual":null}],"tags":[]},{"id":199,"slug":"russkaia-krasa-15","name":"Русская краса","vendorCode":"006-008","description":"<p>Кукла в русском, народном костюме. <\/p><p>Высота - 17,5 сантиметров; <\/p><p>Вес - 50 грамм;<\/p>","price":450,"available":10,"published":true,"productGallery":[{"visual":{"filename":"5f89ee96698a4.jpeg","urls":{"original":"http:\/\/suvenir.maestros.ru\/assets\/uploads\/images\/5f\/89\/5f89ee96698a4.jpeg","small":"http:\/\/suvenir.maestros.ru\/media\/cache\/small\/assets\/uploads\/images\/5f\/89\/5f89ee96698a4.jpeg","medium":"http:\/\/suvenir.maestros.ru\/media\/cache\/medium\/assets\/uploads\/images\/5f\/89\/5f89ee96698a4.jpeg"}}}],"category":{"id":12,"slug":"kukla-v-narodnom-kostiume","name":"Кукла в народном костюме","description":"","visual":null,"parent":{"id":1,"slug":"kukly","name":"Куклы","description":"Разные куклы","visual":null,"parent":null},"children":[]},"materials":[{"id":17,"name":"Фарфор","description":"фарфор","visual":null},{"id":18,"name":"Текстиль","description":"текстиль","visual":null},{"id":19,"name":"Бижутерия","description":"бижутерия","visual":null}],"tags":[]},{"id":198,"slug":"russkaia-krasa-14","name":"Русская краса","vendorCode":"006-007","description":"<p>Кукла в русском, народном костюме. <\/p><p>Высота - 17 сантиметров; <\/p><p>Вес - 50 грамм;<\/p>","price":450,"available":10,"published":true,"productGallery":[{"visual":{"filename":"5f89ed785148a.jpeg","urls":{"original":"http:\/\/suvenir.maestros.ru\/assets\/uploads\/images\/5f\/89\/5f89ed785148a.jpeg","small":"http:\/\/suvenir.maestros.ru\/media\/cache\/small\/assets\/uploads\/images\/5f\/89\/5f89ed785148a.jpeg","medium":"http:\/\/suvenir.maestros.ru\/media\/cache\/medium\/assets\/uploads\/images\/5f\/89\/5f89ed785148a.jpeg"}}}],"category":{"id":12,"slug":"kukla-v-narodnom-kostiume","name":"Кукла в народном костюме","description":"","visual":null,"parent":{"id":1,"slug":"kukly","name":"Куклы","description":"Разные куклы","visual":null,"parent":null},"children":[]},"materials":[{"id":17,"name":"Фарфор","description":"фарфор","visual":null},{"id":18,"name":"Текстиль","description":"текстиль","visual":null},{"id":19,"name":"Бижутерия","description":"бижутерия","visual":null}],"tags":[]}],"total":207}]
	});

	/**
	 * feedback
	 */

	mock.onPost('/api/feedback/get').reply((config) => {
		return [200, {"errors":null,"item":{"id":8,"url":"http:\/\/suvenir.maestros.ru\/","email":"dan_spb_ru@mail.ru","fio":"Дима","message":"вапапвпар","added":[],"description":null,"completed":false,"responsible":null}}]
	});

	mock.onPost('/api/feedback/get-all').reply((config) => {
		return [200, {"errors":null,"items":[{"id":9,"url":"http:\/\/suvenir.maestros.ru\/","email":"test@test.ru","fio":"fdfdf fdf","message":"test","added":[],"description":null,"completed":false,"responsible":null},{"id":8,"url":"http:\/\/suvenir.maestros.ru\/","email":"dan_spb_ru@mail.ru","fio":"Дима","message":"вапапвпар","added":[],"description":null,"completed":false,"responsible":null},{"id":7,"url":"http:\/\/suvenir.maestros.ru\/","email":"renn@mail.ru","fio":"Renat","message":"Test","added":[],"description":null,"completed":false,"responsible":null},{"id":6,"url":"http:\/\/suvenir.maestros.ru\/","email":"dan_spb_ru@mail.ru","fio":"Diman","message":"dasfsdf","added":[],"description":null,"completed":false,"responsible":null},{"id":5,"url":"http:\/\/suvenir.maestros.ru\/","email":"dan_spb_ru@mail.ru","fio":"Дима","message":"Тест333","added":[],"description":null,"completed":false,"responsible":null},{"id":4,"url":"http:\/\/suvenir.maestros.ru\/","email":"renn@mail.ru","fio":"Ренат","message":"Проверка","added":[],"description":null,"completed":false,"responsible":null},{"id":3,"url":"http:\/\/suvenir.maestros.ru\/","email":"dan_spb_ru@mail.ru","fio":"Дмитрий","message":"Тест1","added":[],"description":null,"completed":false,"responsible":null},{"id":2,"url":"http:\/\/suvenir.maestros.ru\/about","email":"dan_spb_ru@mail.ru","fio":"Дмитрий","message":"Прочитал О Вас и офигел!!! Тест обратного сообщения.","added":[],"description":null,"completed":false,"responsible":null},{"id":1,"url":"http:\/\/suvenir.maestros.ru\/about","email":"dan_spb_ru@mail.ru","fio":"Дмитрий","message":"Прочитал О Вас и офигел!!!","added":[],"description":null,"completed":false,"responsible":null}],"total":9}]
	});

	/**
	 * order
	 */

	mock.onPost('/api/order/get').reply((config) => {
		return [200, {"errors":null,"item":{"id":4,"number":"2010274","added":"2020-10-27T10:06:29+03:00","discount":0,"price":2850,"user":{"id":2,"email":"dan_spb_ru@mail.ru","username":"dan_spb_ru@mail.ru","roles":["ROLE_GOD","ROLE_USER"],"password":"$argon2id$v=19$m=65536,t=4,p=1$LnZISFdYWjVUNk5kUi5hVQ$9BT\/R2fGvQHRGpXSG\/uR2bdRmR+MvMGDAj7bNMiF+HQ","salt":null,"fio":null,"mobile":null,"legal":null,"requisites":null,"subscribe":true,"address":null,"added":"2020-10-27T15:09:31+03:00","__initializer__":null,"__cloner__":null,"__isInitialized__":true},"comment":"Быстрее давайте","details":[{"vendorCode":"006-028","name":"Русская краса","id":220,"qty":1,"price":450},{"vendorCode":"001-001","name":"Подвеска","id":114,"qty":1,"price":350},{"vendorCode":"001-004","name":"Подвеска","id":117,"qty":1,"price":350},{"vendorCode":"008-011","name":"Пушкинская","id":134,"qty":1,"price":350},{"vendorCode":"005-003","name":"Кабардинка","id":15,"qty":3,"price":1350}],"status":{"id":1,"value":"заказ в обработке","__initializer__":null,"__cloner__":null,"__isInitialized__":true},"description":null}}]
	});

	mock.onPost('/api/order/get-all').reply((config) => {
		return [200, {"errors":null,"items":[{"id":4,"number":"2010274","added":"2020-10-27T10:06:29+03:00","discount":0,"price":2850,"user":{"id":2,"email":"dan_spb_ru@mail.ru","username":"dan_spb_ru@mail.ru","roles":["ROLE_GOD","ROLE_USER"],"password":"$argon2id$v=19$m=65536,t=4,p=1$LnZISFdYWjVUNk5kUi5hVQ$9BT\/R2fGvQHRGpXSG\/uR2bdRmR+MvMGDAj7bNMiF+HQ","salt":null,"fio":null,"mobile":null,"legal":null,"requisites":null,"subscribe":true,"address":null,"added":"2020-10-27T15:09:31+03:00","__initializer__":null,"__cloner__":null,"__isInitialized__":true},"comment":"Быстрее давайте","details":[{"vendorCode":"006-028","name":"Русская краса","id":220,"qty":1,"price":450},{"vendorCode":"001-001","name":"Подвеска","id":114,"qty":1,"price":350},{"vendorCode":"001-004","name":"Подвеска","id":117,"qty":1,"price":350},{"vendorCode":"008-011","name":"Пушкинская","id":134,"qty":1,"price":350},{"vendorCode":"005-003","name":"Кабардинка","id":15,"qty":3,"price":1350}],"status":{"id":1,"value":"заказ в обработке","__initializer__":null,"__cloner__":null,"__isInitialized__":true},"description":null}],"total":1}]
	});

	mock.onPost('/api/order/get-statuses').reply((config) => {
		return [200, {"errors":null,"items":[{"id":1,"value":"заказ в обработке"},{"id":2,"value":"заказ подтвержден"},{"id":3,"value":"заказ отправлен"},{"id":4,"value":"заказ выполнен"},{"id":5,"value":"заказ отклонен"}],"total":5}]
	});

	/**
	 * review
	 */

	mock.onPost('/api/review/get').reply((config) => {
		return [200, {"errors":null,"item":{"id":1,"author":"Гедаянс Наргиз, г. Тамбов","text":"<p>Спасибо огромное \"Сувенирный промысел\", уже во второй раз делаем заказ сувениров.<\/p><p>Все куклы, выполнены аккуратно, с быстрым сроком доставки, отличной обратной связью. Материал и качество кукол на очень высоком уровне, в каждую куклу вложен кусочек души.<\/p><p>Очень порадовал новый сайт и наличие скидок.<\/p><p>Обязательно будем заказывать еще Вашу продукцию, и советовать знакомым.<\/p>","published":true,"reviewGallery":[{"visual":{"filename":"5f9bea5598a2b.jpeg","urls":{"original":"http:\/\/suvenir.maestros.ru\/assets\/uploads\/images\/5f\/9b\/5f9bea5598a2b.jpeg","small":"http:\/\/suvenir.maestros.ru\/media\/cache\/small\/assets\/uploads\/images\/5f\/9b\/5f9bea5598a2b.jpeg"}}}],"added":"2020-10-29T19:40:23+03:00"}}]
	});

	mock.onPost('/api/review/get-all').reply((config) => {
		return [200, {"errors":null,"items":[{"id":1,"author":"Гедаянс Наргиз, г. Тамбов","text":"<p>Спасибо огромное \"Сувенирный промысел\", уже во второй раз делаем заказ сувениров.<\/p><p>Все куклы, выполнены аккуратно, с быстрым сроком доставки, отличной обратной связью. Материал и качество кукол на очень высоком уровне, в каждую куклу вложен кусочек души.<\/p><p>Очень порадовал новый сайт и наличие скидок.<\/p><p>Обязательно будем заказывать еще Вашу продукцию, и советовать знакомым.<\/p>","published":true,"reviewGallery":[{"visual":{"filename":"5f9bea5598a2b.jpeg","urls":{"original":"http:\/\/suvenir.maestros.ru\/assets\/uploads\/images\/5f\/9b\/5f9bea5598a2b.jpeg","small":"http:\/\/suvenir.maestros.ru\/media\/cache\/small\/assets\/uploads\/images\/5f\/9b\/5f9bea5598a2b.jpeg"}}}],"added":"2020-10-29T19:40:23+03:00"},{"id":2,"author":"Николай Аделия, г. Санкт-Петербург","text":"<p>Сегодня забрали заказ со склада Сувенирный промысел. Заказ был собран в коробки, так что проверять на месте его не стали. Распаковали уже у нас, осмотрели кукол. Качество хорошее, не могу сказать отличное, но тут сделаю скидку на то, что куклы ручной работы.<\/p><p>В общем будем сотрудничать.<\/p><p>Спасибо.<\/p>","published":true,"reviewGallery":[],"added":"2020-10-29T19:43:20+03:00"}],"total":2}]
	});

	/**
	 * service
	 */

	mock.onGet('/api/service/statistic').reply((config) => {
		return [200, {"errors":null,"products":{"all":217,"published":216},"orders":{"all":1,"new":0},"articles":{"all":4,"published":3},"feedback":{"all":9,"completed":3},"reviews":{"all":2,"published":2},"users":{"all":2,"admins":2}}]
	});

	mock.onGet('/api/service/clear-cache').reply((config) => {
		return [200, {"errors":null,"foundedImages":239,"usedImages":239,"deletedImages":0}]
	});

}

if (IS_LOCAL_ENV) {
	fakeServer()
}