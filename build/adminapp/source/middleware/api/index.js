
import base from '~/views/layout/request'
import user from '~/views/user/request'
import article from '~/views/article/request'
import category from '~/views/category/request'
import material from '~/views/material/request'
import tag from '~/views/tag/request'
import sale from '~/views/sale/request'
import product from '~/views/product/request'
import feedback from '~/views/feedback/request'
import order from '~/views/order/request'
import review from '~/views/review/request'

export default {
	base,
	user,
	article,
	category,
	material,
	tag,
	sale,
	product,
	feedback,
	order,
	review,
};
