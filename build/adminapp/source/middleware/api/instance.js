import axios from 'axios'
import store from '~/middleware/store'

import {Notification} from 'element-ui'

axios.defaults.headers.post['Content-Type'] = 'multipart/form-data';

axios.defaults.transformRequest = [(data) => {
	if ( data ){
		let formData = new FormData();

		Object.keys(data).map(function(key) {
			if (data[key] === undefined) return;
			if (Array.isArray(data[key])) {
				data[key].forEach(item => {
					formData.append(`${key}[]`, typeof item === 'object' ?  JSON.stringify(item) : item);
				});

			} else {
				formData.append(key, data[key]);
			}
		});

		return formData;
	}
}];

axios.interceptors.request.use(config => {
	return config;
}, error => {
	return Promise.reject(error);
});

axios.interceptors.response.use(
	response => {
		if ( response.data && response.data.errors && response.data.errors.length ) {
			writeErrors(response.data.errors);
			throw new Error(response.data.errors);
		}
		statusParser(response.status);
		return response;
	},
	error => {
		if ( error && error.response && error.response.data && error.response.data.errors && error.response.data.errors.length ) {
			writeErrors(error.response.data.errors);
		}
		statusParser(error.response.status);
		return Promise.reject(error);
	}
);

function writeErrors(errors) {
	errors.forEach(error => {
		Notification({
			message: error,
			duration: 8000,
			type: 'warning',
			position: 'bottom-right'
		});
	});
}

function statusParser(status) {
	if (status === 401){
		store.commit('base/SET_AUTH_STATUS', false);
	}
}


export default axios;
