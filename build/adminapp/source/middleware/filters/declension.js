/**
 * {{ 105 | declension('день', 'дня', 'дней') }} // дней
 * @param value
 * @param args
 */

export default function(value, ...args) {
	if (value === undefined) return '';
	const cases = [2, 0, 1, 1, 1, 2];
	const num = Number(value);
	if (Number.isNaN(num)) {
		console.error('declension must be a Number');
		return value;
	}
	return args[
		num % 100 > 4 && num % 100 < 20 ? 2 : cases[num % 10 < 5 ? num % 10 : 5]
	];
}
