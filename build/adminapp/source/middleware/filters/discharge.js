/**
 * {{ '1000000000000' | discharge() }} // 1 000 000 000 000
 * @param value
 * @param mark
 */

export default function(value, mark = ' ') {
	if (value === undefined) return '';
	return String(value).replace(/(\d)(?=(\d{3})+([^\d]|$))/g, `$1${mark}`);
}
