/**
 * {{ 'once upon a time'  | crop(10, '...') }} // once upon...
 * @param value
 * @param limit
 * @param postfix
 * @param minLastLetter
 */

export default function(
	value,
	limit,
	postfix = '...',
	minLastLetter = 2,
) {
	if (value === undefined) return '';
	if (limit === undefined || limit <= 0) return value;
	let text = value.toString().trim();
	if (text.length <= limit) return text;
	text = text.slice(0, limit);
	const arr = text.split(' ');
	const lastSpace = text.lastIndexOf(' ');
	if (arr[arr.length - 1].length < minLastLetter && lastSpace > 0) {
		text = text.substr(0, lastSpace);
	}
	return text + postfix;
}
