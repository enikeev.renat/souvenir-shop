import Vue from 'vue';

import crop from './crop';
import declension from './declension';
import discharge from './discharge';

const filters = {
	crop,
	declension,
	discharge,
};

Object.keys(filters).forEach(name => {
	Vue.filter(name, filters[name]);
});

export { crop, declension, discharge };
