import OverlayScrollbars from 'overlayscrollbars';

export default {
	install: (V)=>{
		V.directive('scrollbar', {
			inserted(el, binding, vnode) {
				let options = {
					className: 'os-theme-thin-light',
					sizeAutoCapable : true,
					paddingAbsolute : true,
					scrollbars : {
						clickScrolling : true,
						autoHide : "leave"
					},
					overflowBehavior:{
						x: 'hidden'
					}
				};
				if (!vnode.scrollbar) vnode.scrollbar = OverlayScrollbars(el, Object.assign(options, binding.value));
			},

			unbind(el, binding, vnode){
				if (vnode.scrollbar) vnode.scrollbar.destroy();
			}
		});
	}
};