import Vue from 'vue';

import eventHub from './eventhub'
import scrollbar from './scrollbar'
import dragndrop from 'vue-drag-drop';
import tooltip from 'v-tooltip';
import lazyload from 'vue-lazyload'
import moment from "./moment";

const library = {
	eventHub: {
		plugin: {
			install: V => {
				V.prototype.$eventHub = eventHub;
			}
		},
		options: {}
	},
	scrollbar: {
		plugin: scrollbar,
		options: {}
	},
	dragndrop: {
		plugin: dragndrop,
		options: {}
	},
	tooltip: {
		plugin: tooltip,
		options: {}
	},
	lazyload: {
		plugin: lazyload,
		options: {
			preLoad: 1.3,
			error: '/assets/img/svg/btn-loader.svg',
			loading: '/assets/img/svg/btn-loader.svg',
			attempt: 1
		}
	},
	moment: {
		plugin: {
			install: V => {
				V.prototype.$moment = moment;
			},
		},
		options: {}
	},
};


Object.keys(library).forEach(name => {
	Vue.use(library[name].plugin, library[name].options = {});
});