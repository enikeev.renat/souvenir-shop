export const highlightString = (value = '', part = '') => {
	if (!value) return '';
	if (!part) return value;
	const innerString = value.toString().toLowerCase().replace(/ё/g, 'е');
	const partString  = part.toString().toLowerCase().replace(/ё/g, 'е');
	const idx         = innerString.indexOf(partString);
	if (idx < 0) return value;
	const begin = value.substr(0, idx),
		highlight = value.substr(idx, part.length),
		end = value.substr(idx + part.length);
	return `${begin}<b class="highlight">${highlight}</b>${end}`;
};