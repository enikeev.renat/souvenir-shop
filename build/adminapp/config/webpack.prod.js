const {merge} = require("webpack-merge");
const baseConfig = require("./webpack.base");
const webpack = require('webpack');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const ManifestPlugin = require('webpack-manifest-plugin');

const config = merge(baseConfig, {
	mode: "production",

	output: {
		publicPath: '/assets/',
		filename: 'js/[name].js?[hash]'
	},

	module: {
		rules: [
			{
				test: /\.s?css$/,
				use: [
					{
						loader: MiniCssExtractPlugin.loader,
						options: {
							publicPath: '../',
							sourceMap: true
						}
					},
					{
						loader: "css-loader",
						options: { sourceMap: true }
					},
					{
						loader: "postcss-loader",
						options: {
							plugins: [
								require('autoprefixer'),
								require('cssnano')({
									preset: [
										'default', {
											discardComments: {
												removeAll: true
											}
										}
									]
								})
							],
							name: '[name].[ext]?[hash]',
							sourceMap: true,
						}
					},
					'resolve-url-loader',
					{
						loader: "sass-loader",
						options: {
							sourceMap: true,
							additionalData: `
								@import "source/assets/scss/_mixins.scss";
								@import "source/assets/scss/_variables.scss";
							`
						}
					}
				]
			},

		]
	},

	plugins: [
		new webpack.DefinePlugin({
			'IS_LOCAL_ENV': JSON.stringify(false),
		}),
		new ManifestPlugin({
			publicPath: '/assets/'
		}),
	]
});

module.exports = new Promise((resolve, reject) => {
	resolve(config);
});