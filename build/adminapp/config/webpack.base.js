
const path = require('path');
const fs = require('fs');
const webpack = require('webpack');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const {VueLoaderPlugin} = require('vue-loader');


const paths = {
	src: path.resolve(__dirname, "../source"),
	dist: path.resolve(__dirname, '../../../public/assets'),
};

const config = {
	externals: {
		paths
	},

	entry: {
		admin: ['@babel/polyfill', `${paths.src}/app.js`]
	},

	output: {
		path: paths.dist,
		filename: 'js/[name].js'
	},

	resolve: {
		extensions: ['.js', '.vue', '.ts'],
		alias: {
			"~": paths.src,
			vue$: "vue/dist/vue.js"
		}
	},

	module: {
		rules: [
			{
				test: /\.tsx?$/,
				exclude: /node_modules/,
				use: 'ts-loader',
			},
			{
				test: /\.js$/,
				exclude: "/node_modules/",
				loader: "babel-loader",
			},
			{
				test: /\.vue$/,
				loader: "vue-loader",
				options: {
					loader: {
						scss: "vue-style-loader!css-loader!sass-loader"
					}
				}
			},
			{
				test: /\.pug$/,
				oneOf: [
					{
						resourceQuery: /^\?vue/,
						use: ['pug-plain-loader']
					},
					{
						use: ['raw-loader', {
							loader: 'pug-plain-loader',
							options: {
								pretty: true,
							}
						}]
					}
				]
			},
			{
				test: /\.(gif|png|jpe?g|svg)$/i,
				include: [path.join(__dirname, "../source/assets/img")],	//, path.join(__dirname, "../source/assets/img")
				use: [
					{
						loader: 'file-loader',
						options: {
							name: '[path][name].[ext]',
							outputPath: 'img/',
							//useRelativePath: true
						}
					},
					/*{
						loader: 'image-webpack-loader',
						options: {

						}
					}*/
				]
			},
			{
				test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
				exclude: [path.join(__dirname, "../source/assets/img")],
				use: [
					{
						loader: 'file-loader',
						options: {
							name: '[name].[ext]',
							outputPath: 'fonts/'
						}
					}
				]
			},
		]
	},

	plugins: [
		new MiniCssExtractPlugin({
			filename: `css/[name].css`
		}),
		new HtmlWebpackPlugin({
			filename: `admin.html`,
			template: `${paths.src}/index.pug`,
			hash: false,
			minify: false,
			inject: 'body'
		}),
		new VueLoaderPlugin()
	]

};

module.exports = config;