const {merge} = require("webpack-merge");
const baseConfig = require("./webpack.base");
const webpack = require('webpack');

const config = merge(baseConfig, {
	mode: "development",
	devtool: "cheap-module-eval-source-map",

	devServer: {
		hot: true,
		open: false,
	},

	module: {
		rules: [
			{
				test: /\.s?css$/,
				use: [
					"style-loader",
					{
						loader: "css-loader",
						options: { sourceMap: true }
					},
					{
						loader: "postcss-loader",
						options: {
							plugins: [
								require('autoprefixer'),
							],
							sourceMap: true,
						}
					},
					'resolve-url-loader',
					{
						loader: "sass-loader",
						options: {
							sourceMap: true,
							additionalData: `
								@import "source/assets/scss/_mixins.scss";
								@import "source/assets/scss/_variables.scss";
							`
						}
					}
				]
			},

		]
	},

	plugins: [
		new webpack.DefinePlugin({
			'IS_LOCAL_ENV': JSON.stringify(true),
		}),
		new webpack.SourceMapDevToolPlugin({
			filename: "[file].map"
		})
	]
});

module.exports = new Promise((resolve, reject) => {
	resolve(config);
});