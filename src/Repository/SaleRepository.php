<?php

namespace App\Repository;

use App\Entity\Sale;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Sale|null find($id, $lockMode = null, $lockVersion = null)
 * @method Sale|null findOneBy(array $criteria, array $orderBy = null)
 * @method Sale[]    findAll()
 * @method Sale[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SaleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Sale::class);
    }

    public function getDiscount(int $price): ?int
    {
        $queryBuilder = $this->createQueryBuilder('sale');

        $queryBuilder->where('sale.min <= '.$price);

        $queryBuilder->addOrderBy('sale.min', 'DESC');

        $queryBuilder->setMaxResults(1);

        $result = $queryBuilder->getQuery()->getResult();

        return (!$result) ? 0 : $result[0]->getValue();
    }
}
