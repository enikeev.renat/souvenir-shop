<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use function get_class;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        $user->setPassword($newEncodedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    public function countAdmins()
    {
        $qb = $this->createQueryBuilder('user')
            ->select('count(user.id)')
            ->where('user.roles LIKE :god')
            ->orWhere('user.roles LIKE :admin')
            ->setParameter('god', '%GOD%')
            ->setParameter('admin', '%ADMIN%');

        return (int)$qb->getQuery()->getSingleScalarResult();
    }

    public function criteria(array $orderBy, $query = null, $limit = null, $offset = null) : ?Collection
    {
        $criteria = Criteria::create();
        $criteria->orderBy($orderBy);
        $criteria->setFirstResult($offset);
        $criteria->setMaxResults($limit);

        if(null !== $query) {
            $criteria->where(Criteria::expr()->orX(
                Criteria::expr()->contains('email', $query),
                Criteria::expr()->contains('fio', $query),
                Criteria::expr()->contains('mobile', $query)
            ));
        }

        return $this->matching($criteria);
    }

    public function criteriaCount($query = null) {
        $criteria = Criteria::create();

        if(null !== $query) {
            $criteria->where(Criteria::expr()->orX(
                Criteria::expr()->contains('email', $query),
                Criteria::expr()->contains('fio', $query),
                Criteria::expr()->contains('mobile', $query)
            ));
        }

        return $this->matching($criteria)->count();
    }
}
