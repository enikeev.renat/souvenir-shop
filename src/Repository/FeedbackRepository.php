<?php

namespace App\Repository;

use App\Entity\Feedback;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Feedback|null find($id, $lockMode = null, $lockVersion = null)
 * @method Feedback|null findOneBy(array $criteria, array $orderBy = null)
 * @method Feedback[]    findAll()
 * @method Feedback[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FeedbackRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Feedback::class);
    }

    public function criteria(array $orderBy, $query = null, $limit = null, $offset = null) : ?Collection
    {
        $criteria = Criteria::create();
        $criteria->orderBy($orderBy);
        $criteria->setFirstResult($offset);
        $criteria->setMaxResults($limit);

        if(null !== $query) {
            $criteria->where(Criteria::expr()->orX(
                Criteria::expr()->contains('email', $query),
                Criteria::expr()->contains('fio', $query)
            ));
        }

        return $this->matching($criteria);
    }

    public function criteriaCount($query = null) {
        $criteria = Criteria::create();

        if(null !== $query) {
            $criteria->where(Criteria::expr()->orX(
                Criteria::expr()->contains('email', $query),
                Criteria::expr()->contains('fio', $query)
            ));
        }

        return $this->matching($criteria)->count();
    }
}
