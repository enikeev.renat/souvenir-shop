<?php

namespace App\Repository;

use App\Entity\Review;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Review|null find($id, $lockMode = null, $lockVersion = null)
 * @method Review|null findOneBy(array $criteria, array $orderBy = null)
 * @method Review[]    findAll()
 * @method Review[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReviewRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Review::class);
    }


    public function criteria(bool $publishedOnly, array $orderBy, $query = null, $limit = null, $offset = null) : ?Collection
    {
        $criteria = Criteria::create();
        $criteria->orderBy($orderBy);
        $criteria->setFirstResult($offset);
        $criteria->setMaxResults($limit);

        if(null !== $query) {
            $criteria->where(Criteria::expr()->contains('author', $query));
        }

        if($publishedOnly) {
            if(null !== $query)
                $criteria->andWhere(Criteria::expr()->eq('published', true));
            else
                $criteria->where(Criteria::expr()->eq('published', true));
        }

        return $this->matching($criteria);
    }

    public function criteriaCount(bool $publishedOnly, $query = null) {
        $criteria = Criteria::create();

        if(null !== $query) {
            $criteria->where(Criteria::expr()->contains('author', $query));
        }

        if($publishedOnly) {
            if(null !== $query)
                $criteria->andWhere(Criteria::expr()->eq('published', true));
            else
                $criteria->where(Criteria::expr()->eq('published', true));
        }

        return $this->matching($criteria)->count();
    }
}
