<?php

namespace App\Repository;

use App\Entity\Order;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Order|null find($id, $lockMode = null, $lockVersion = null)
 * @method Order|null findOneBy(array $criteria, array $orderBy = null)
 * @method Order[]    findAll()
 * @method Order[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Order::class);
    }

    public function criteria(array $orderBy, $limit = null, $offset = null) : ?Collection
    {
        $criteria = Criteria::create();
        $criteria->orderBy($orderBy);
        $criteria->setFirstResult($offset);
        $criteria->setMaxResults($limit);

        return $this->matching($criteria);
    }

    public function criteriaQB(array $orderBy, $query = null, $limit = null, $offset = null)
    {
        $qb = $this->createQueryBuilder('o');

        foreach ($orderBy as $key => $val)
            $qb->addOrderBy('o.' . $key, $val);

        $qb->setFirstResult($offset);

        $qb->setMaxResults($limit);

        if(null !== $query) {
            $qb->join('o.user', 'user');
            $qb->setParameter('q', '%' . $query . '%');
            $qb->where($qb->expr()->orX(
                $qb->expr()->like('o.number', ':q'),
                $qb->expr()->like('user.email', ':q')
            ));
        }

        return $qb->getQuery()->getResult();
    }

    public function criteriaQBCount($query = null) : int
    {
        $qb = $this->createQueryBuilder('o');

        $qb->select('count(o.id)');

        if(null !== $query) {
            $qb->join('o.user', 'user');
            $qb->setParameter('q', '%' . $query . '%');
            $qb->where($qb->expr()->orX(
                $qb->expr()->like('o.number', ':q'),
                $qb->expr()->like('user.email', ':q')
            ));
        }

        return (int)$qb->getQuery()->getSingleScalarResult();
    }
}
