<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Persistence\ManagerRegistry;
use Exception;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    private $queryBuilder;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function findFilter(array $orderBy = null, $limit = null, $offset = null, $price = null, $material = null, $category = null, $available = null, $tag = null)
    {
        $this->queryBuilder = $this->createQueryBuilder('product');

        $this->queryBuilder->where('product.published = true');

        $this->queryBuilder->setMaxResults($limit);

        $this->queryBuilder->setFirstResult($offset);

        foreach ($orderBy as $key => $val)
            $this->queryBuilder->addOrderBy('product.' . $key, $val);

        $this->applyFilter($price, $material, $category, $available, $tag);

        return $this->queryBuilder->getQuery()->getResult();
    }

    public function countFilter($price = null, $material = null, $category = null, $available = null, $tag = null): int
    {
        $this->queryBuilder = $this->createQueryBuilder('product');

        $this->queryBuilder->where('product.published = true');

        $this->applyFilter($price, $material, $category, $available, $tag);

        try {
            return count($this->queryBuilder->select('product.id')->getQuery()->getResult());
        } catch (Exception $e) {
            return 0;
        }
    }

    private function applyFilter($price = null, $material = null, $category = null, $available = null, $tag = null)
    {
        if(null !== $price) {
            $pp = explode("-", $price);
            if(isset($pp[0]) && !empty($pp[0]))
                $this->queryBuilder->andWhere('product.price >= '.$pp[0]);
            if(isset($pp[1]) && !empty($pp[1]))
                $this->queryBuilder->andWhere('product.price <= '.$pp[1]);
        }

        if(null !== $material) {
            if(!is_array($material))
                $material = array($material);
            $this->queryBuilder->join('product.materials', 'materials');
            $this->queryBuilder->andWhere($this->queryBuilder->expr()->in('materials.id', $material));
        }

        if(null !== $tag) {
            if(!is_array($tag))
                $tag = array($tag);
            $this->queryBuilder->join('product.tags', 'tags');
            $this->queryBuilder->andWhere($this->queryBuilder->expr()->in('tags.id', $tag));
        }

        if(null !== $category) {
            if(!is_array($category))
                $category = array($category);
            $this->queryBuilder->andWhere($this->queryBuilder->expr()->in('product.category', $category));
        }

        if(null !== $available)
            $this->queryBuilder->andWhere('product.available > 0 ');

        $this->queryBuilder->groupBy('product.id');
    }

    public function getMinPrice(): int
    {
        $this->queryBuilder = $this->createQueryBuilder('product');

        $this->queryBuilder->where('product.published = true');

        $this->queryBuilder->select('MIN(product.price) as minPrice');

        return $this->queryBuilder->getQuery()->getSingleScalarResult();
    }

    public function getMaxPrice(): int
    {
        $this->queryBuilder = $this->createQueryBuilder('product');

        $this->queryBuilder->where('product.published = true');

        $this->queryBuilder->select('MAX(product.price) as maxPrice');

        return $this->queryBuilder->getQuery()->getSingleScalarResult();
    }

    public function criteria(bool $publishedOnly, array $orderBy, $query = null, $limit = null, $offset = null) : ?Collection
    {
        $criteria = Criteria::create();
        $criteria->orderBy($orderBy);
        $criteria->setFirstResult($offset);
        $criteria->setMaxResults($limit);

        if(null !== $query) {
            $criteria->where(Criteria::expr()->orX(
                Criteria::expr()->contains('vendorCode', $query),
                Criteria::expr()->contains('name', $query)
            ));
        }

        if($publishedOnly) {
            if(null !== $query)
                $criteria->andWhere(Criteria::expr()->eq('published', true));
            else
                $criteria->where(Criteria::expr()->eq('published', true));
        }

        return $this->matching($criteria);
    }

    public function criteriaCount(bool $publishedOnly, $query = null) {
        $criteria = Criteria::create();

        if(null !== $query) {
            $criteria->where(Criteria::expr()->orX(
                Criteria::expr()->contains('vendorCode', $query),
                Criteria::expr()->contains('name', $query)
            ));
        }

        if($publishedOnly) {
            if(null !== $query)
                $criteria->andWhere(Criteria::expr()->eq('published', true));
            else
                $criteria->where(Criteria::expr()->eq('published', true));
        }

        return $this->matching($criteria)->count();
    }
}
