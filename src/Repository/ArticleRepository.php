<?php

namespace App\Repository;

use App\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Persistence\ManagerRegistry;
use Exception;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    private $queryBuilder;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Article::class);
    }

    public function findFilter(array $orderBy = null, $limit = null, $offset = null, $tag = null)
    {
        $this->queryBuilder = $this->createQueryBuilder('article');

        $this->queryBuilder->where('article.published = true');

        $this->queryBuilder->setMaxResults($limit);

        $this->queryBuilder->setFirstResult($offset);

        foreach ($orderBy as $key => $val)
            $this->queryBuilder->addOrderBy('article.' . $key, $val);

        $this->applyFilter($tag);

        return $this->queryBuilder->getQuery()->getResult();
    }

    public function countFilter($tag = null): int
    {
        $this->queryBuilder = $this->createQueryBuilder('article');

        $this->queryBuilder->where('article.published = true');

        $this->applyFilter($tag);

        try {
            return count($this->queryBuilder->select('article.id')->getQuery()->getResult());
        } catch (Exception $e) {
            return 0;
        }
    }

    private function applyFilter($tag = null)
    {
        if(null !== $tag) {
            if(!is_array($tag))
                $tag = array($tag);
            $this->queryBuilder->join('article.tags', 'tags');
            $this->queryBuilder->andWhere($this->queryBuilder->expr()->in('tags.id', $tag));
        }

        $this->queryBuilder->groupBy('article.id');
    }

    public function criteria(bool $publishedOnly, array $orderBy, $query = null, $limit = null, $offset = null) : ?Collection
    {
        $criteria = Criteria::create();
        $criteria->orderBy($orderBy);
        $criteria->setFirstResult($offset);
        $criteria->setMaxResults($limit);

        if(null !== $query) {
            $criteria->where(Criteria::expr()->orX(
                Criteria::expr()->contains('title', $query),
                Criteria::expr()->contains('description', $query)
            ));
        }

        if($publishedOnly) {
            if(null !== $query)
                $criteria->andWhere(Criteria::expr()->eq('published', true));
            else
                $criteria->where(Criteria::expr()->eq('published', true));
        }

        return $this->matching($criteria);
    }

    public function criteriaCount(bool $publishedOnly, $query = null) {
        $criteria = Criteria::create();

        if(null !== $query) {
            $criteria->where(Criteria::expr()->orX(
                Criteria::expr()->contains('title', $query),
                Criteria::expr()->contains('description', $query)
            ));
        }

        if($publishedOnly) {
            if(null !== $query)
                $criteria->andWhere(Criteria::expr()->eq('published', true));
            else
                $criteria->where(Criteria::expr()->eq('published', true));
        }

        return $this->matching($criteria)->count();
    }
}
