<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;

class FaqController extends BaseController
{
    /**
     * @Route("/faq", name="faq")
     */
    public function index()
    {
        $this->data["title"] = "Часто задаваемые вопросы";
        $this->data['keywords'] = "Часто задаваемые вопросы";
        $this->data['description'] = "Часто задаваемые вопросы";

        return $this->render('faq/index.html.twig', $this->data);
    }
}
