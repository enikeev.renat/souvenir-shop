<?php

namespace App\Controller;

use App\Entity\Review;
use App\Service\CartManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BaseController extends AbstractController
{
    protected $data = array();

    protected $cartManager;

    /**
     * BaseController constructor.
     */
    public function __construct(CartManager $cartManager, EntityManagerInterface $em)
    {
        $this->data['title'] = "Не указан заголовок";
        $this->data['keywords'] = "Не указаны ключевые слова";
        $this->data['description'] = "Не указано описание";

        $this->cartManager = $cartManager;

        $this->data['cartCount'] = $cartManager->count();

        $this->data['reviewsCount'] = $em->getRepository(Review::class)->count(['published' => true]);
    }
}
