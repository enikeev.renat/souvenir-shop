<?php

namespace App\Controller;

use App\Entity\Cart;
use App\Entity\Order;
use App\Entity\OrderStatus;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends BaseController
{
    /**
     * @Route("/cart", name="cart")
     */
    public function index()
    {
        $this->data["title"] = "Корзина";
        $this->data['keywords'] = "Корзина";
        $this->data['description'] = "Корзина";

        $this->data['carts'] = $this->cartManager->getCarts();
        $this->data['cartPrices'] = $this->cartManager->getCartPrices();

        return $this->render('cart/index.html.twig', $this->data);
    }

    /**
     * @Route("/cart/order", name="cartOrder")
     */
    public function order(EntityManagerInterface $em, Request $request)
    {
        if($this->cartManager->count() == 0 || $this->getUser() === null)
            return $this->redirectToRoute('cart');

        $cartPrices = $this->cartManager->getCartPrices();
        $carts = $this->cartManager->getCarts();

        $order = new Order();
        $order->setUser($this->getUser());
        $order->setDiscount($cartPrices->getDiscount());
        $order->setPrice($cartPrices->getPriceWithDiscount());
        $order->setAdded(new DateTime());
        $order->setComment($request->get('comment'));
        $status = $em->getRepository(OrderStatus::class)->find(1);
        $order->setStatus($status);
        $order->setDelivery($request->get('delivery') ?? 0);
        $order->setAddress($request->get('address'));
        $order->setMobile($request->get('mobile'));

        $details = [];
        foreach ($carts as $cart) {
            if($cart instanceof Cart) {
                $detail = [];
                $detail['vendorCode'] = $cart->getProduct()->getVendorCode();
                $detail['name'] = $cart->getProduct()->getName();
                $detail['id'] = $cart->getProduct()->getId();
                $detail['qty'] = $cart->getQty();
                $detail['price'] = $cart->getProduct()->getPrice() * $cart->getQty();
                $details[] = $detail;
            }
        }

        $order->setDetails($details);

        if(!empty($request->get('address')))
            $this->getUser()->setAddress($request->get('address'));

        if(!empty($request->get('mobile')))
            $this->getUser()->setMobile($request->get('mobile'));

        $em->persist($order);
        $em->flush();

        $order->setNumber(date('ymd').$order->getId());

        $em->flush();

        $this->cartManager->clear();

        $this->data["title"] = "Заказ № " . $order->getNumber() . ' от ' . $order->getAdded()->format('d.m.Y');
        $this->data['keywords'] = "Заказ № " . $order->getNumber() . ' от ' . $order->getAdded()->format('d.m.Y');
        $this->data['description'] = "Заказ № " . $order->getNumber() . ' от ' . $order->getAdded()->format('d.m.Y');

        $this->data['number'] = $order->getNumber();

        $this->data['cartCount'] = $this->cartManager->count();

        return $this->render('cart/order.html.twig', $this->data);
    }

    /**
     * @Route("/cart/repeat-order/{number}", name="cartRepeatOrder")
     */
    public function repeat(Order $order)
    {
        if(!$this->getUser() || $order->getUser()->getId() != $this->getUser()->getId())
            return $this->redirectToRoute('home');

        $this->cartManager->clear();

        foreach ($order->getDetails() as $detail) {
            $this->cartManager->add($detail['id']);
            $this->cartManager->changeQty([$detail['id'] => $detail['qty']]);
        }

        return $this->redirectToRoute('cart');
    }
}
