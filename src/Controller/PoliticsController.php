<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;

class PoliticsController extends BaseController
{
    /**
     * @Route("/politics", name="politics")
     */
    public function index()
    {
        $this->data["title"] = "Политика конфиденциальности";
        $this->data['keywords'] = "Политика конфиденциальности";
        $this->data['description'] = "Политика конфиденциальности";

        return $this->render('politics/index.html.twig', $this->data);
    }
}
