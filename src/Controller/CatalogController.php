<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Material;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;

class CatalogController extends BaseController
{
    /**
     * @Route("/catalog", name="catalog")
     */
    public function index(EntityManagerInterface $em, ProductRepository $repository)
    {
        $this->data["title"] = "Категории";
        $this->data['keywords'] = "Категории";
        $this->data['description'] = "Категории";

        $this->data['categories'] = $em->getRepository(Category::class)->childrenHierarchy();
		$this->data['materials'] = $em->getRepository(Material::class)->findAll();
		$this->data['min'] = $repository->getMinPrice();
		$this->data['max'] = $repository->getMaxPrice();

        return $this->render('catalog/index.html.twig', $this->data);
    }

    /**
     * @Route("/catalog/{path}", name="catalogItem", requirements={"path"=".+"})
     */
    public function item($path, EntityManagerInterface $em, ProductRepository $repository)
    {
        $slugs = explode("/", $path);
        foreach ($slugs as $slug) {
            $category = $em->getRepository(Category::class)->findOneBy(['slug' => $slug]);
            if(!$category)
                throw $this->createNotFoundException();
        }

        $category = $em->getRepository(Category::class)->findOneBy(['slug' => array_pop($slugs)]);

        $this->data["title"] = $category->getName();
        $this->data['keywords'] = $category->getName();
        $this->data['description'] = $category->getDescription();

        $this->data['category'] = $category;
		$this->data['materials'] = $em->getRepository(Material::class)->findAll();
		$this->data['min'] = $repository->getMinPrice();
		$this->data['max'] = $repository->getMaxPrice();

        $filter = ['category[]=' . $category->getId()];
        $children = $em->getRepository(Category::class)->children($category);
        foreach($children as $child) {
            $filter[] = 'category[]=' . $child->getId();
        }
        $this->data['filter'] = implode("&", $filter);

        return $this->render('catalog/item.html.twig', $this->data);
    }
}
