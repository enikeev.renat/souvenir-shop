<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use SymfonyCasts\Bundle\ResetPassword\Controller\ResetPasswordControllerTrait;
use SymfonyCasts\Bundle\ResetPassword\Exception\ResetPasswordExceptionInterface;
use SymfonyCasts\Bundle\ResetPassword\ResetPasswordHelperInterface;

class RestoreController extends BaseController
{
    use ResetPasswordControllerTrait;

    /**
     * Validates and process the reset URL that the user clicked in their email.
     *
     * @Route("/restore/reset/{token}", name="appResetPassword")
     */
    public function index(Request $request, ResetPasswordHelperInterface $resetPasswordHelper, UserPasswordEncoderInterface $passwordEncoder, string $token = null)
    {
        $this->data["title"] = "Сброс пароля";
        $this->data['keywords'] = "Сброс пароля";
        $this->data['description'] = "Сброс пароля";

        if ($token) {
            // We store the token in session and remove it from the URL, to avoid the URL being
            // loaded in a browser and potentially leaking the token to 3rd party JavaScript.
            $this->storeTokenInSession($token);

            return $this->redirectToRoute('appResetPassword');
        }

        $token = $this->getTokenFromSession();
        if (null === $token) {
            throw $this->createNotFoundException('В URL-адресе или в сеансе не обнаружен токен сброса пароля.');
        }

        try {
            $user = $resetPasswordHelper->validateTokenAndFetchUser($token);
        } catch (ResetPasswordExceptionInterface $e) {
            $this->addFlash('restore_info', sprintf(
                'При проверке запроса на сброс возникла проблема - %s',
                $e->getReason()
            ));

            return $this->redirectToRoute('restoreInfo');
        }

        if (null !== $request->get('repeat_password')) {
            // A password reset token should be used only once, remove it.
            $resetPasswordHelper->removeResetRequest($token);

            // Encode the plain password, and set it.
            $encodedPassword = $passwordEncoder->encodePassword(
                $user,
                $request->get('repeat_password')
            );

            $user->setPassword($encodedPassword);
            $this->getDoctrine()->getManager()->flush();

            // The session is cleaned up after the password has been changed.
            $this->cleanSessionAfterReset();

            $this->addFlash('restore_info', 'Пароль успешно изменён.');

            return $this->redirectToRoute('restoreInfo');
        }

        return $this->render('restore/reset.html.twig', $this->data);
    }

    /**
     * @Route("/restore/info", name="restoreInfo")
     */
    public function info(FlashBagInterface $flash)
    {
        $this->data["title"] = "Сброс пароля";
        $this->data['keywords'] = "Сброс пароля";
        $this->data['description'] = "Сброс пароля";

        $this->data['info'] = array();

        if($flash->has('restore_info'))
            foreach ($flash->get('restore_info') as $info)
                $this->data['info'][] = $info;
        else
            return $this->redirectToRoute('home');

        return $this->render('restore/info.html.twig', $this->data);
    }
}
