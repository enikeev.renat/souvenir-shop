<?php

namespace App\Controller;

use App\Repository\ReviewRepository;
use Symfony\Component\Routing\Annotation\Route;

class ReviewController extends BaseController
{
    /**
     * @Route("/review", name="review")
     */
    public function index(ReviewRepository $repository)
    {
        $this->data["title"] = "Отзывы";
        $this->data['keywords'] = "Отзывы";
        $this->data['description'] = "Отзывы";

        $this->data['reviews'] = $repository->findBy(['published' => true], ['added' => 'DESC']);

        return $this->render('review/index.html.twig', $this->data);
    }
}
