<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Category;
use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends BaseController
{
    /**
     * @Route("/", name="home")
     */
    public function index(EntityManagerInterface $em)
    {
        $this->data["title"] = "Главная страница";
        $this->data['keywords'] = "Ключевые слова главной страницы";
        $this->data['description'] = "Описание главной страницы";

        $categories = $em->getRepository(Category::class)->findAll();
        shuffle($categories);
        $this->data['categories'] = array_slice($categories, 0, 4);

        $this->data['products'] = $this->renderView('pieces/products.list.html.twig', [
            'products' => $em->getRepository(Product::class)->findBy(['published' => true], ['id' => 'DESC'], 6, 0),
            'cartIds' => $this->cartManager->getIds()
        ]);

        $this->data['articles'] = $this->renderView('pieces/articles.list.html.twig', [
            'articles' => $em->getRepository(Article::class)->findBy(['published' => true], ['id' => 'DESC'], 3, 0)
        ]);

        return $this->render('home/index.html.twig', $this->data);
    }
}
