<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Category;
use App\Entity\Material;
use App\Entity\Product;
use App\Entity\Tag;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductsController extends BaseController
{
    /**
     * @Route("/products", name="products")
     */
    public function index(EntityManagerInterface $em, ProductRepository $repository, Request $request)
    {
        $this->data["title"] = "Каталог";
        $this->data['keywords'] = "Каталог";
        $this->data['description'] = "Каталог";

        $this->data['materials'] = $em->getRepository(Material::class)->findAll();

        $this->data['categories'] = $em->getRepository(Category::class)->childrenHierarchy();
        $this->data['min'] = $repository->getMinPrice();
        $this->data['max'] = $repository->getMaxPrice();

        $this->data['tag'] = $em->getRepository(Tag::class)->find($request->get('tag') ?? 0);
        $this->data['material'] = $request->get('material');

        return $this->render('products/index.html.twig', $this->data);
    }

    /**
     * @Route("/products/{slug}", name="productsItem")
     */
    public function item(Product $product, EntityManagerInterface $em)
    {
        $this->data["title"] = $product->getName();
        $this->data['keywords'] = $product->getName();
        $this->data['description'] = $product->getName();

        $this->data['product'] = $product;
        $this->data['cartIds'] = $this->cartManager->getIds();

        $this->data['articles'] = null;

        if($product->getTags()->count() > 0) {
            $tags = [];
            foreach ($product->getTags() as $tag)
                $tags[] = $tag->getId();
            $this->data['articles'] = $this->renderView('pieces/articles.list.html.twig', [
                'articles' => $em->getRepository(Article::class)->findFilter(['id' => 'DESC'], 3, 0, $tags)
            ]);
        }

        return $this->render('products/item.html.twig', $this->data);
    }

    /**
     * @Route("/products/json/list", name="getHtmlProducts")
     */
    public function getHtmlProducts(Request $request, ProductRepository $repository)
    {
        $order = ['id' => 'DESC'];

        if(null !== $request->get('priceValue'))
            $order = ['price' => ($request->get('priceValue') == 1) ? 'DESC' : 'ASC'];

        $products = $repository->findFilter($order, $request->get('limit'), $request->get('offset'), $request->get('price'), $request->get('material'), $request->get('category'), $request->get('available'), $request->get('tag'));

        $total = $repository->countFilter($request->get('price'), $request->get('material'), $request->get('category'), $request->get('available'), $request->get('tag'));

		$html = $this->renderView('pieces/products.list.html.twig', [
			'products' => $products,
			'cartIds' => $this->cartManager->getIds(),
			'total' => $total
		]);

        $done = true;
        if(null !== $request->get('offset') && (count($products) + $request->get('offset')) < $total)
            $done = false;

        return $this->json(
            [
                'html' => $html,
                'done' => $done,
                'total' => $total
            ],
            Response::HTTP_OK,
            [],
            ['json_encode_options' => JSON_UNESCAPED_UNICODE]
        );
    }
}
