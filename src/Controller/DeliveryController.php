<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;

class DeliveryController extends BaseController
{
    /**
     * @Route("/delivery", name="delivery")
     */
    public function index()
    {
        $this->data["title"] = "Доставка";
        $this->data['keywords'] = "Доставка";
        $this->data['description'] = "Доставка";

        return $this->render('delivery/index.html.twig', $this->data);
    }
}
