<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Product;
use App\Entity\Tag;
use App\Repository\ArticleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ArticleController extends BaseController
{
    /**
     * @Route("/article", name="article")
     */
    public function index(Request $request, EntityManagerInterface $em)
    {
        $this->data["title"] = "Статьи";
        $this->data['keywords'] = "Статьи";
        $this->data['description'] = "Статьи";

        $this->data['tag'] = $em->getRepository(Tag::class)->find($request->get('tag') ?? 0);

        return $this->render('article/index.html.twig', $this->data);
    }

    /**
     * @Route("/article/{slug}", name="articleItem")
     */
    public function item(Article $article, EntityManagerInterface $em)
    {
        $this->data["title"] = $article->getTitle();
        $this->data['keywords'] = $article->getTitle();
        $this->data['description'] = $article->getDescription();

        $this->data['article'] = $article;

        $this->data['products'] = null;

        if($article->getTags()->count() > 0) {
            $tags = [];
            foreach ($article->getTags() as $tag)
                $tags[] = $tag->getId();
            $this->data['products'] = $this->renderView('pieces/products.list.html.twig', [
                'products' => $em->getRepository(Product::class)->findFilter(['id' => 'DESC'], 6, 0, null, null, null, null, $tags),
                'cartIds' => $this->cartManager->getIds()
            ]);
        }

        return $this->render('article/item.html.twig', $this->data);
    }

    /**
     * @Route("/article/json/list", name="getHtmlArticles")
     */
    public function getHtmlArticles(Request $request, ArticleRepository $repository)
    {
        $order = ['id' => 'DESC'];

        $articles = $repository->findFilter($order, $request->get('limit'), $request->get('offset'), $request->get('tag'));

        $html = $this->renderView('pieces/articles.list.html.twig', [
            'articles' => $articles
        ]);

        $total = $repository->countFilter($request->get('tag'));

        $done = true;
        if(null !== $request->get('offset') && (count($articles) + $request->get('offset')) < $total)
            $done = false;

        return $this->json(
            [
                'html' => $html,
                'done' => $done,
                'total' => $total
            ],
            Response::HTTP_OK,
            [],
            ['json_encode_options' => JSON_UNESCAPED_UNICODE]
        );
    }
}
