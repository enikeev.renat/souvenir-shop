<?php

namespace App\Controller;

use App\Repository\ArticleRepository;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends BaseController
{
    /**
     * @Route("/search", name="search")
     */
    public function index(Request $request, ProductRepository $productRepository, ArticleRepository $articleRepository)
    {
        $html = '';

        if(null !== $request->get('search') && !empty(trim($request->get('search')))) {

            $products = $productRepository->criteria(true, ['id' => 'DESC'], $request->get('search'));

            $articles = $articleRepository->criteria(true, ['id' => 'DESC'], $request->get('search'));

            $html = $this->renderView('pieces/search.list.html.twig', [
                'products' => $products,
                'articles' => $articles
            ]);
        }

        return $this->json(
            [
                'errors' => null,
                'html' => $html,
            ],
            Response::HTTP_OK,
            [],
            ['json_encode_options' => JSON_UNESCAPED_UNICODE]
        );
    }
}
