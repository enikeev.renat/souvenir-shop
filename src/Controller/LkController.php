<?php

namespace App\Controller;

use App\Repository\OrderRepository;
use App\Repository\ProductRepository;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/lk")
 */
class LkController extends BaseController
{
    /**
     * @Route("", name="lk")
     */
    public function index()
    {
        if(null === $this->getUser())
            return $this->redirectToRoute('home');

        $this->data["title"] = "Личный кабинет";
        $this->data['keywords'] = "Личный кабинет";
        $this->data['description'] = "Личный кабинет";

        return $this->render('lk/index.html.twig', $this->data);
    }

    /**
     * @Route("/history", name="lkHistory")
     */
    public function history(OrderRepository $orderRepository, ProductRepository $productRepository)
    {
        if(null === $this->getUser())
            return $this->redirectToRoute('home');

        $this->data["title"] = "История заказов";
        $this->data['keywords'] = "История заказов";
        $this->data['description'] = "История заказов";

        $orders = $orderRepository->findBy(['user' => $this->getUser()], ['added' => 'DESC']);


        foreach ($orders as $keyOrder => $order) {
            $details = $order->getDetails();
            foreach ($details as $key => $detail) {
                $product = $productRepository->find($detail['id']);
                if(!$product)
                    $details[$key]['slug'] = null;
                else
                    $details[$key]['slug'] = $product->getSlug();
            }
            $orders[$keyOrder]->setDetails($details);
        }

        $this->data['orders'] = $orders;

        return $this->render('lk/history.html.twig', $this->data);
    }

    /**
     * @Route("/settings", name="lkSettings")
     */
    public function settings()
    {
        if(null === $this->getUser())
            return $this->redirectToRoute('home');

        $this->data["title"] = "Настройки";
        $this->data['keywords'] = "Настройки";
        $this->data['description'] = "Настройки";

        return $this->render('lk/settings.html.twig', $this->data);
    }
}
