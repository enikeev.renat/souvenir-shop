<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;

class ContactController extends BaseController
{
    /**
     * @Route("/contact", name="contact")
     */
    public function index()
    {
        $this->data["title"] = "Контакты";
        $this->data['keywords'] = "Контакты";
        $this->data['description'] = "Контакты";

        return $this->render('contact/index.html.twig', $this->data);
    }
}
