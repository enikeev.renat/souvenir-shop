<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;

class AboutController extends BaseController
{
    /**
     * @Route("/about", name="about")
     */
    public function index()
    {
        $this->data["title"] = "О нас";
        $this->data['keywords'] = "О нас";
        $this->data['description'] = "О нас";

        return $this->render('about/index.html.twig', $this->data);
    }
}
