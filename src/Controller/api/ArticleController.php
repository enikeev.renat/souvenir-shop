<?php

namespace App\Controller\api;

use App\Entity\Article;
use App\Entity\ArticleImage;
use App\Entity\Tag;
use App\Repository\ArticleRepository;
use App\Service\ApiImageHandler;
use App\Service\SNHandler;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Security("is_granted('ROLE_GOD') or is_granted('ROLE_ADMIN')")
 * @Route("/api/article")
 */
class ArticleController extends ApiController
{
    /**
     * @Route("/get-all", name="apiArticleGetAll")
     */
    public function getAll(ArticleRepository $repository, Request $request, SNHandler $SNHandler, ApiImageHandler $imageHandler)
    {
        $orderBy = [];
        if(null !== $request->get('sortName') && null !== $request->get('sortDirection'))
            $orderBy = [$request->get('sortName') => $request->get('sortDirection')];

        $articles = $repository->criteria(false, $orderBy, $request->get('query'), $request->get('limit'), $request->get('offset'));

        foreach($articles as $article) {
            $article->setVisual($imageHandler->getVisual($article->getVisualFilename()));
            foreach ($article->getArticleGallery() as $image) {
                $image->setVisual($imageHandler->getVisual($image->getFilename()));
            }
        }

        $this->data['items'] = $SNHandler->getSerializer()->normalize($articles, null, ['groups' => 'api', ObjectNormalizer::ENABLE_MAX_DEPTH => true]);

        $this->data['total'] = $repository->criteriaCount(false, $request->get('query'));

        return $this->apiResponse();
    }

    /**
     * @Route("/get", name="apiArticleGet")
     */
    public function getOne(ArticleRepository $repository, Request $request, SNHandler $SNHandler, ApiImageHandler $imageHandler)
    {
        if(!$request->isMethod('POST')) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Требуется POST запрос с параметрами {id}';
            return $this->apiResponse();
        }

        $article = $repository->find($request->get('id', 0) !== null ? $request->get('id', 0) : 0);

        if(null !== $article) {
            $article->setVisual($imageHandler->getVisual($article->getVisualFilename()));
            foreach ($article->getArticleGallery() as $image) {
                $image->setVisual($imageHandler->getVisual($image->getFilename()));
            }
            $this->data['item'] = $SNHandler->getSerializer()->normalize($article, null, ['groups' => 'api', ObjectNormalizer::ENABLE_MAX_DEPTH => true]);
        } else {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Статья с id='.$request->get('id').' не найдена';
        }

        return $this->apiResponse();
    }

    /**
     * @Route("/new", name="apiArticleNew")
     */
    public function new(EntityManagerInterface $em, Request $request, ValidatorInterface $validator, ApiImageHandler $imageHandler)
    {
        if(!$request->isMethod('POST')) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Требуется POST запрос с параметрами {title, description, content, visual, published, tags, article_gallery}';
            return $this->apiResponse();
        }

        $article = new Article();

        if(!empty($request->get('title')))
            $article->setTitle($request->get('title'));

        $article->setDescription($request->get('description'));

        if(!empty($request->get('content')))
            $article->setContent($request->get('content'));

        if(!empty($request->get('visual') && is_array($request->get('visual')))) {
            foreach ($request->get('visual') as $json) {
                $article->setVisualFilename($imageHandler->handleVisual($json));
            }
        }

        if(!empty($request->get('published')))
            $article->setPublished(($request->get('published') == 'false') ? false : true);

        if(!empty($request->get('tags') && is_array($request->get('tags')))) {
            $tagRepository = $em->getRepository(Tag::class);
            foreach ($request->get('tags') as $id) {
                $tag = $tagRepository->find($id);
                if(null !== $tag && $tag instanceof Tag)
                    $article->addTag($tag);
            }
        }

        $article->setAdded(new DateTime());

        $errors = $validator->validate($article);

        if(count($errors) > 0) {
            $this->status = Response::HTTP_BAD_REQUEST;
            foreach ($errors as $error)
                $this->data['errors'][] = $error->getMessage();
        } else {
            if(!empty($request->get('article_gallery') && is_array($request->get('article_gallery')))) {
                foreach ($request->get('article_gallery') as $json) {
                    $filename = $imageHandler->handleVisual($json);
                    if(!$filename)
                        continue;
                    $articleImage = new ArticleImage();
                    $articleImage->setFilename($filename);
                    $articleImage->setArticle($article);
                    $em->persist($articleImage);
                }
            }
            $em->persist($article);
            $em->flush();

            $this->data['id'] = $article->getId();
        }

        return $this->apiResponse();
    }

    /**
     * @Route("/update", name="apiArticleUpdate")
     */
    public function update(EntityManagerInterface $em, Request $request, ValidatorInterface $validator, ApiImageHandler $imageHandler)
    {
        if(!$request->isMethod('POST')) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Требуется POST запрос с параметрами {id, title, description, content, visual, published, tags, article_gallery}';
            return $this->apiResponse();
        }

        $repository = $em->getRepository(Article::class);
        $article = $repository->find($request->get('id', 0) !== null ? $request->get('id', 0) : 0);

        if(null !== $article && $article instanceof Article) {
            if(!empty($request->get('title')))
                $article->setTitle($request->get('title'));

            $article->setDescription($request->get('description'));

            if(!empty($request->get('content')))
                $article->setContent($request->get('content'));

            if(!empty($request->get('visual') && is_array($request->get('visual')))) {
                foreach ($request->get('visual') as $json) {
                    $article->setVisualFilename($imageHandler->handleVisual($json));
                }
            }

            if(!empty($request->get('published')))
                $article->setPublished(($request->get('published') == 'false') ? false : true);

            if(!empty($request->get('tags') && is_array($request->get('tags')))) {
                $article->getTags()->clear();
                $tagRepository = $em->getRepository(Tag::class);
                foreach ($request->get('tags') as $id) {
                    $tag = $tagRepository->find($id);
                    if(null !== $tag && $tag instanceof Tag)
                        $article->addTag($tag);
                }
            }

            $errors = $validator->validate($article);

            if(count($errors) > 0) {
                $this->status = Response::HTTP_BAD_REQUEST;
                foreach ($errors as $error)
                    $this->data['errors'][] = $error->getMessage();
            } else {
                if(!empty($request->get('article_gallery') && is_array($request->get('article_gallery')))) {
                    $article->getArticleGallery()->clear();
                    foreach ($request->get('article_gallery') as $json) {
                        $filename = $imageHandler->handleVisual($json);
                        if(!$filename)
                            continue;
                        $articleImage = new ArticleImage();
                        $articleImage->setFilename($filename);
                        $articleImage->setArticle($article);
                        $em->persist($articleImage);
                    }
                }
                $em->flush();
            }
        } else {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Статья с id='.$request->get('id').' не найдена';
        }

        return $this->apiResponse();
    }

    /**
     * @Route("/delete", name="apiArticleDelete")
     */
    public function delete(EntityManagerInterface $em, Request $request, ApiImageHandler $imageHandler)
    {
        if(!$request->isMethod('POST')) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Требуется POST запрос с параметрами {id}';
            return $this->apiResponse();
        }

        $repository = $em->getRepository(Article::class);
        $article = $repository->find($request->get('id', 0) !== null ? $request->get('id', 0) : 0);

        if(null !== $article) {
            if(!empty($article->getVisualFilename()))
                $imageHandler->delete($article->getVisualFilename());
            if(!$article->getArticleGallery()->isEmpty()) {
                foreach ($article->getArticleGallery()->getIterator() as $key => $item) {
                    if($item instanceof ArticleImage)
                        $imageHandler->delete($item->getFilename());
                }
            }
            $em->remove($article);
            $em->flush();
        } else {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Статья с id='.$request->get('id').' не найдена';
        }

        return $this->apiResponse();
    }
}