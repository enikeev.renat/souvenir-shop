<?php

namespace App\Controller\api;

use App\Entity\Article;
use App\Entity\ArticleImage;
use App\Entity\Category;
use App\Entity\Feedback;
use App\Entity\Material;
use App\Entity\Order;
use App\Entity\Product;
use App\Entity\ProductImage;
use App\Entity\Review;
use App\Entity\ReviewImage;
use App\Entity\User;
use App\Service\ApiImageHandler;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Security("is_granted('ROLE_GOD') or is_granted('ROLE_ADMIN')")
 * @Route("/api/service")
 */
class ServiceController extends ApiController
{
    /**
     * @Route("/upload-image", name="apiUploadImage")
     */
    public function uploadImage(Request $request, ApiImageHandler $imageHandler)
    {
        $filename = $imageHandler->upload($request->files->get('image'));

        if(null !== $filename) {
            if(null !== $request->get('filters') && is_array($request->get('filters'))) {
                $imageHandler->createThumbnails($filename, $request->get('filters'));
            }

            $this->data['item'] = $imageHandler->getVisual($filename);
        } else {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Файл изображения не был загружен';
        }

        return $this->apiResponse();
    }

    /**
     * @Route("/delete-image", name="apiDeleteImage")
     */
    public function deleteImage(Request $request, ApiImageHandler $imageHandler, EntityManagerInterface $em)
    {
        $filename = $request->files->get('filename');
        $result = $imageHandler->delete($filename);

        if(false === $result) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Файл изображения не был удален. Файл не '.$filename.' найден.';
        } else {
            //Articles
            $items = $em->getRepository(Article::class)->findBy(['visual_filename' => $filename]);
            foreach ($items as $item) {
                if($item instanceof Article)
                    $item->setVisualFilename(NULL);
            }
            $items = $em->getRepository(ArticleImage::class)->findBy(['filename' => $filename]);
            foreach ($items as $item)
                $em->remove($item);

            //Products
            $items = $em->getRepository(ProductImage::class)->findBy(['filename' => $filename]);
            foreach ($items as $item)
                $em->remove($item);

            //Reviews
            $items = $em->getRepository(ReviewImage::class)->findBy(['filename' => $filename]);
            foreach ($items as $item)
                $em->remove($item);

            //Category
            $items = $em->getRepository(Category::class)->findBy(['visual_filename' => $filename]);
            foreach ($items as $item) {
                if($item instanceof Category)
                    $item->setVisualFilename(NULL);
            }

            //Material
            $items = $em->getRepository(Material::class)->findBy(['visual_filename' => $filename]);
            foreach ($items as $item) {
                if($item instanceof Material)
                    $item->setVisualFilename(NULL);
            }

            $em->flush();
        }

        return $this->apiResponse();
    }

    /**
     * @Route("/clear-cache", name="apiClearCache")
     */
    public function clearUnusedImages(ApiImageHandler $imageHandler, EntityManagerInterface $em)
    {
        $usedImages = [];

        //Articles
        $items = $em->getRepository(Article::class)->findAll();
        foreach ($items as $item) {
            if($item instanceof Article && $item->getVisualFilename() !== null)
                $usedImages[] = $item->getVisualFilename();
        }
        $items = $em->getRepository(ArticleImage::class)->findAll();
        foreach ($items as $item)
            if($item instanceof ArticleImage && $item->getFilename() !== null)
                $usedImages[] = $item->getFilename();

        //Products
        $items = $em->getRepository(ProductImage::class)->findAll();
        foreach ($items as $item)
            if($item instanceof ProductImage && $item->getFilename() !== null)
                $usedImages[] = $item->getFilename();

        //Reviews
        $items = $em->getRepository(ReviewImage::class)->findAll();
        foreach ($items as $item)
            if($item instanceof ReviewImage && $item->getFilename() !== null)
                $usedImages[] = $item->getFilename();

        //Category
        $items = $em->getRepository(Category::class)->findAll();
        foreach ($items as $item) {
            if($item instanceof Category && $item->getVisualFilename() !== null)
                $usedImages[] = $item->getVisualFilename();
        }

        //Material
        $items = $em->getRepository(Material::class)->findAll();
        foreach ($items as $item) {
            if($item instanceof Material && $item->getVisualFilename() !== null)
                $usedImages[] = $item->getVisualFilename();
        }

        $this->data['foundedImages'] = 0;
        $this->data['usedImages'] = count($usedImages);
        $this->data['deletedImages'] = 0;

        $finder = new Finder();
        $finder->files()->in($imageHandler->getTargetDirectory());
        foreach ($finder as $file) {
            $this->data['foundedImages']++;
            if(!in_array($file->getFilename(), $usedImages)) {
                $imageHandler->delete($file->getFilename());
                $this->data['deletedImages']++;
            }
        }

        return $this->apiResponse();
    }

    /**
     * @Route("/statistic", name="apiStatistic")
     */
    public function statistic(EntityManagerInterface $em)
    {
        $this->data['products']['all'] = $em->getRepository(Product::class)->count([]);
        $this->data['products']['published'] = $em->getRepository(Product::class)->count(['published' => true]);

        $this->data['orders']['all'] = $em->getRepository(Order::class)->count([]);
        $this->data['orders']['new'] = $em->getRepository(Order::class)->count(['status' => 1]);

        $this->data['articles']['all'] = $em->getRepository(Article::class)->count([]);
        $this->data['articles']['published'] = $em->getRepository(Article::class)->count(['published' => true]);

        $this->data['feedback']['all'] = $em->getRepository(Feedback::class)->count([]);
        $this->data['feedback']['completed'] = $em->getRepository(Feedback::class)->count(['completed' => true]);

        $this->data['reviews']['all'] = $em->getRepository(Review::class)->count([]);
        $this->data['reviews']['published'] = $em->getRepository(Review::class)->count(['published' => true]);

        $this->data['users']['all'] = $em->getRepository(User::class)->count([]);
        $this->data['users']['admins'] = $em->getRepository(User::class)->countAdmins();

        return $this->apiResponse();
    }
}