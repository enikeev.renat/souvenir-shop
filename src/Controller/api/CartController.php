<?php


namespace App\Controller\api;

use App\Service\CartManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/cart")
 */
class CartController extends ApiController
{
    /**
     * @Route("/add", name="apiCartAddProduct")
     */
    public function add(CartManager $cartManager, Request $request)
    {
        if(!$request->isMethod('POST')) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Требуется POST запрос с параметрами {id}';
            return $this->apiResponse();
        }

        if(!$cartManager->add($request->get('id', 0) ?? 0)) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Продукт с id='.$request->get('id').' не добавлен в корзину';
            return $this->apiResponse();
        }

        $this->data['inCart'] = $cartManager->count();

        return $this->apiResponse();
    }

    /**
     * @Route("/remove", name="apiCartDeleteProduct")
     */
    public function remove(CartManager $cartManager, Request $request)
    {
        if(!$request->isMethod('POST')) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Требуется POST запрос с параметрами {id}';
            return $this->apiResponse();
        }

        if(!$cartManager->remove($request->get('id', 0) ?? 0)) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Продукт с id='.$request->get('id').' в корзине не найден';
            return $this->apiResponse();
        }

        $this->data['inCart'] = $cartManager->count();

        return $this->apiResponse();
    }

    /**
     * @Route("/calc", name="apiCartCalc")
     */
    public function calc(CartManager $cartManager, Request $request)
    {
        if(!$request->isMethod('POST')) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Требуется POST запрос с параметрами {array of id=>qty}';
            return $this->apiResponse();
        }

        $this->data['info'] = $cartManager->changeQty($request->request->all());

        return $this->apiResponse();
    }
}