<?php

namespace App\Controller\api;

use App\Entity\Sale;
use App\Repository\SaleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Security("is_granted('ROLE_GOD') or is_granted('ROLE_ADMIN')")
 * @Route("/api/sale")
 */
class SaleController extends ApiController
{
    /**
     * @Route("/get-all", name="apiSaleGetAll")
     */
    public function getAll(SaleRepository $repository, Request $request)
    {
        $this->data['items'] = $repository->findBy([], ['id' => 'DESC'], $request->get('limit'), $request->get('offset'));
        $this->data['total'] = $repository->count([]);

        return $this->apiResponse();
    }

    /**
     * @Route("/get", name="apiSaleGet")
     */
    public function getOne(SaleRepository $repository, Request $request)
    {
        if(!$request->isMethod('POST')) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Требуется POST запрос с параметрами {id}';
            return $this->apiResponse();
        }

        $tag = $repository->find($request->get('id', 0) !== null ? $request->get('id', 0) : 0);

        if(null !== $tag) {
            $this->data['item'] = $tag;
        } else {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Скидка с id='.$request->get('id').' не найдена';
        }

        return $this->apiResponse();
    }

    /**
     * @Route("/new", name="apiSaleNew")
     */
    public function new(EntityManagerInterface $em, Request $request, ValidatorInterface $validator)
    {
        if(!$request->isMethod('POST')) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Требуется POST запрос с параметрами {min, value}';
            return $this->apiResponse();
        }

        $sale = new Sale();

        if(!empty($request->get('min')))
            $sale->setMin($request->get('min'));

        if(!empty($request->get('value')))
            $sale->setValue($request->get('value'));

        $errors = $validator->validate($sale);

        if(count($errors) > 0) {
            $this->status = Response::HTTP_BAD_REQUEST;
            foreach ($errors as $error)
                $this->data['errors'][] = $error->getMessage();
        } else {
            $em->persist($sale);
            $em->flush();

            $this->data['id'] = $sale->getId();
        }

        return $this->apiResponse();
    }

    /**
     * @Route("/update", name="apiSaleUpdate")
     */
    public function update(EntityManagerInterface $em, Request $request, ValidatorInterface $validator)
    {
        if(!$request->isMethod('POST')) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Требуется POST запрос с параметрами {id, min, value}';
            return $this->apiResponse();
        }

        $repository = $em->getRepository(Sale::class);
        $sale = $repository->find($request->get('id', 0) !== null ? $request->get('id', 0) : 0);

        if(null !== $sale && $sale instanceof Sale) {
            if(!empty($request->get('min')))
                $sale->setMin($request->get('min'));

            if(!empty($request->get('value')))
                $sale->setValue($request->get('value'));

            $errors = $validator->validate($sale);

            if(count($errors) > 0) {
                $this->status = Response::HTTP_BAD_REQUEST;
                foreach ($errors as $error)
                    $this->data['errors'][] = $error->getMessage();
            } else {
                $em->flush();
            }
        } else {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Скидка с id='.$request->get('id').' не найдена';
        }

        return $this->apiResponse();
    }

    /**
     * @Route("/delete", name="apiSaleDelete")
     */
    public function delete(EntityManagerInterface $em, Request $request)
    {
        if(!$request->isMethod('POST')) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Требуется POST запрос с параметрами {id}';
            return $this->apiResponse();
        }

        $repository = $em->getRepository(Sale::class);
        $sale = $repository->find($request->get('id', 0) !== null ? $request->get('id', 0) : 0);

        if(null !== $sale) {
            $em->remove($sale);
            $em->flush();
        } else {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Скидка с id='.$request->get('id').' не найдена';
        }

        return $this->apiResponse();
    }
}