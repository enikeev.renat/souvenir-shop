<?php

namespace App\Controller\api;

use App\Entity\Review;
use App\Entity\ReviewImage;
use App\Repository\ReviewRepository;
use App\Service\ApiImageHandler;
use App\Service\SNHandler;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Security("is_granted('ROLE_GOD') or is_granted('ROLE_ADMIN')")
 * @Route("/api/review")
 */
class ReviewController extends ApiController
{
    /**
     * @Route("/get-all", name="apiReviewGetAll")
     */
    public function getAll(ReviewRepository $repository, Request $request, SNHandler $SNHandler, ApiImageHandler $imageHandler)
    {
        $orderBy = [];
        if(null !== $request->get('sortName') && null !== $request->get('sortDirection'))
            $orderBy = [$request->get('sortName') => $request->get('sortDirection')];

        $reviews = $repository->criteria(false, $orderBy, $request->get('query'), $request->get('limit'), $request->get('offset'));

        foreach($reviews as $review) {
            foreach ($review->getReviewGallery() as $image) {
                $image->setVisual($imageHandler->getVisual($image->getFilename()));
            }
        }

        $this->data['items'] = $SNHandler->getSerializer()->normalize($reviews, null, ['groups' => 'api', ObjectNormalizer::ENABLE_MAX_DEPTH => true]);

        $this->data['total'] = $repository->criteriaCount(false, $request->get('query'));

        return $this->apiResponse();
    }

    /**
     * @Route("/get", name="apiReviewGet")
     */
    public function getOne(ReviewRepository $repository, Request $request, SNHandler $SNHandler, ApiImageHandler $imageHandler)
    {
        if(!$request->isMethod('POST')) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Требуется POST запрос с параметрами {id}';
            return $this->apiResponse();
        }

        $review = $repository->find($request->get('id') ?? 0);

        if(null !== $review) {
            foreach ($review->getReviewGallery() as $image) {
                $image->setVisual($imageHandler->getVisual($image->getFilename()));
            }
            $this->data['item'] = $SNHandler->getSerializer()->normalize($review, null, ['groups' => 'api', ObjectNormalizer::ENABLE_MAX_DEPTH => true]);;
        } else {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Отзыв с id='.$request->get('id').' не найден';
        }

        return $this->apiResponse();
    }

    /**
     * @Route("/new", name="apiReviewNew")
     */
    public function new(EntityManagerInterface $em, Request $request, ValidatorInterface $validator, ApiImageHandler $imageHandler)
    {
        if(!$request->isMethod('POST')) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Требуется POST запрос с параметрами {author, text, published, review_gallery}';
            return $this->apiResponse();
        }

        $review = new Review();

        if(!empty($request->get('author')))
            $review->setAuthor($request->get('author'));

        if(!empty($request->get('text')))
            $review->setText($request->get('text'));

        if(!empty($request->get('published')))
            $review->setPublished(($request->get('published') == 'false') ? false : true);

        if(!empty($request->get('added')))
            $review->setAdded(new DateTime($request->get('added')));

        $errors = $validator->validate($review);

        if(count($errors) > 0) {
            $this->status = Response::HTTP_BAD_REQUEST;
            foreach ($errors as $error)
                $this->data['errors'][] = $error->getMessage();
        } else {
            if(!empty($request->get('review_gallery') && is_array($request->get('review_gallery')))) {
                foreach ($request->get('review_gallery') as $json) {
                    $filename = $imageHandler->handleVisual($json);
                    if(!$filename)
                        continue;
                    $reviewImage = new ReviewImage();
                    $reviewImage->setFilename($filename);
                    $reviewImage->setReview($review);
                    $em->persist($reviewImage);
                }
            }
            $em->persist($review);
            $em->flush();

            $this->data['id'] = $review->getId();
        }

        return $this->apiResponse();
    }

    /**
     * @Route("/update", name="apiReviewUpdate")
     */
    public function update(EntityManagerInterface $em, Request $request, ValidatorInterface $validator, ApiImageHandler $imageHandler)
    {
        if(!$request->isMethod('POST')) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Требуется POST запрос с параметрами {id, author, text, published, review_gallery}';
            return $this->apiResponse();
        }

        $repository = $em->getRepository(Review::class);
        $review = $repository->find($request->get('id') ?? 0);

        if(null !== $review && $review instanceof Review) {
            if(!empty($request->get('author')))
                $review->setAuthor($request->get('author'));

            if(!empty($request->get('text')))
                $review->setText($request->get('text'));

            if(!empty($request->get('published')))
                $review->setPublished(($request->get('published') == 'false') ? false : true);

            if(!empty($request->get('added')))
                $review->setAdded(new DateTime($request->get('added')));

            $errors = $validator->validate($review);

            if(count($errors) > 0) {
                $this->status = Response::HTTP_BAD_REQUEST;
                foreach ($errors as $error)
                    $this->data['errors'][] = $error->getMessage();
            } else {
                if(!empty($request->get('review_gallery') && is_array($request->get('review_gallery')))) {
                    $review->getReviewGallery()->clear();
                    foreach ($request->get('review_gallery') as $json) {
                        $filename = $imageHandler->handleVisual($json);
                        if(!$filename)
                            continue;
                        $reviewImage = new ReviewImage();
                        $reviewImage->setFilename($filename);
                        $reviewImage->setReview($review);
                        $em->persist($reviewImage);
                    }
                }
                $em->flush();
            }
        } else {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Отзыв с id='.$request->get('id').' не найден';
        }

        return $this->apiResponse();
    }

    /**
     * @Route("/delete", name="apiReviewDelete")
     */
    public function delete(EntityManagerInterface $em, Request $request, ApiImageHandler $imageHandler)
    {
        if(!$request->isMethod('POST')) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Требуется POST запрос с параметрами {id}';
            return $this->apiResponse();
        }

        $repository = $em->getRepository(Review::class);
        $review = $repository->find($request->get('id') ?? 0);

        if(null !== $review) {
            if(!$review->getReviewGallery()->isEmpty()) {
                foreach ($review->getReviewGallery()->getIterator() as $key => $item) {
                    if($item instanceof ReviewImage)
                        $imageHandler->delete($item->getFilename());
                }
            }
            $em->remove($review);
            $em->flush();
        } else {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Отзыв с id='.$request->get('id').' не найден';
        }

        return $this->apiResponse();
    }
}