<?php

namespace App\Controller\api;

use App\Entity\User;
use App\Repository\UserRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/api/user")
 */
class UserController extends ApiController
{
    /**
     * @Security("is_granted('ROLE_GOD')")
     * @Route("/get-all", name="apiUserGetAll")
     */
    public function getAll(UserRepository $repository, Request $request)
    {
        $orderBy = [];
        if(null !== $request->get('sortName') && null !== $request->get('sortDirection'))
            $orderBy = [$request->get('sortName') => $request->get('sortDirection')];

        $this->data['items'] = $repository->criteria($orderBy, $request->get('query'), $request->get('limit'), $request->get('offset'));
        $this->data['total'] = $repository->criteriaCount($request->get('query'));

        return $this->apiResponse();
    }

    /**
     * @Security("is_granted('ROLE_GOD')")
     * @Route("/get", name="apiUserGet")
     */
    public function getOne(UserRepository $repository, Request $request)
    {
        if(!$request->isMethod('POST')) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Требуется POST запрос с параметрами {id}';
            return $this->apiResponse();
        }

        $user = $repository->find($request->get('id') ?? 0);

        if(null !== $user) {
            $this->data['item'] = $user;
        } else {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Пользователь с id='.$request->get('id').' не найден';
        }

        return $this->apiResponse();
    }

    /**
     * @Security("is_granted('ROLE_GOD')")
     * @Route("/get-roles", name="apiUserGetRoles")
     */
    public function getRoles()
    {
        $this->data['items'] = [
            ['key' => 'ROLE_ADMIN', 'value' => 'Администратор', 'disabled' => false],
            ['key' => 'ROLE_GOD', 'value' => 'Суперадминистратор', 'disabled' => true],
            ['key' => 'ROLE_USER', 'value' => 'Пользователь', 'disabled' => true],
        ];

        return $this->apiResponse();
    }

    /**
     * @Security("is_granted('ROLE_GOD')")
     * @Route("/new", name="apiUserNew")
     */
    public function new(EntityManagerInterface $em, Request $request, UserPasswordEncoderInterface $passwordEncoder, ValidatorInterface $validator)
    {
        if(!$request->isMethod('POST')) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Требуется POST запрос с параметрами {email, fio, mobile, legal, requisites, password, roles}';
            return $this->apiResponse();
        }

        $user = new User();

        if(!empty($request->get('email')))
            $user->setEmail($request->get('email'));

        if(!empty($request->get('roles')) && is_array($request->get('roles')))
            $user->setRoles($request->get('roles'));

        if(!empty($request->get('password')))
            $user->setPassword($passwordEncoder->encodePassword($user, $request->get('password')));

        if(!empty($request->get('fio')))
            $user->setFio($request->get('fio'));

        if(!empty($request->get('mobile')))
            $user->setMobile($request->get('mobile'));

        if(!empty($request->get('address')))
            $user->setAddress($request->get('address'));

        if(!empty($request->get('legal')))
        $user->setLegal(($request->get('legal') == 'false') ? false : true);

        if(!empty($request->get('requisites')))
            $user->setRequisites($request->get('requisites'));

        $user->setAdded(new DateTime());

        $errors = $validator->validate($user);

        if(count($errors) > 0) {
            $this->status = Response::HTTP_BAD_REQUEST;
            foreach ($errors as $error)
                $this->data['errors'][] = $error->getMessage();
        } else {
            $em->persist($user);
            $em->flush();

            $this->data['id'] = $user->getId();
        }

        return $this->apiResponse();
    }

    /**
     * @Security("is_granted('ROLE_GOD')")
     * @Route("/delete", name="apiUserDelete")
     */
    public function delete(EntityManagerInterface $em, Request $request)
    {
        if(!$request->isMethod('POST')) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Требуется POST запрос с параметрами {id}';
            return $this->apiResponse();
        }

        $repository = $em->getRepository(User::class);
        $user = $repository->find($request->get('id', 0) !== null ? $request->get('id', 0) : 0);

        if(null !== $user && $user instanceof User) {
            if(!in_array("ROLE_GOD", $user->getRoles())) {
                $em->remove($user);
                $em->flush();
            } else {
                $this->status = Response::HTTP_BAD_REQUEST;
                $this->data['errors'][] = 'Пользователя с ролью Бога нельзя удалить';
            }
        } else {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Пользователь с id='.$request->get('id').' не найден';
        }

        return $this->apiResponse();
    }

    /**
     * @Route("/change-password", name="apiUserChangePassword")
     */
    public function changePassword(UserRepository $repository, Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        if(!$request->isMethod('POST') || empty($request->get('current_password')) || empty($request->get('new_password'))) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Требуется POST запрос с параметрами {current_password, new_password}';
            return $this->apiResponse();
        }

        if(null === $this->getUser()) {
            $this->status = Response::HTTP_UNAUTHORIZED;
            $this->data['errors'][] = 'Требуется авторизация';
            return $this->apiResponse();
        }

        if(!$passwordEncoder->isPasswordValid($this->getUser(), $request->get('current_password'))) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Неверно введен текущий пароль';
        } else {
            $pswd = $passwordEncoder->encodePassword($this->getUser(), $request->get('new_password'));
            $repository->upgradePassword($this->getUser(), $pswd);
        }

        return $this->apiResponse();
    }

    /**
     * @Route("/change-subscribe", name="apiUserChangeSubscribe")
     */
    public function changeSubscribe(EntityManagerInterface $em, Request $request)
    {
        if(!$request->isMethod('POST')) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Требуется POST запрос с параметрами {subscribe}';
            return $this->apiResponse();
        }

        if(null === $this->getUser()) {
            $this->status = Response::HTTP_UNAUTHORIZED;
            $this->data['errors'][] = 'Требуется авторизация';
            return $this->apiResponse();
        }

        $this->getUser()->setSubscribe($request->get('subscribe') ?? false);
        $em->flush();

        return $this->apiResponse();
    }

    /**
     * @Security("is_granted('ROLE_GOD')")
     * @Route("/update", name="apiUserUpdate")
     */
    public function update(EntityManagerInterface $em, Request $request, ValidatorInterface $validator)
    {
        if(!$request->isMethod('POST')) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Требуется POST запрос с параметрами {id, email, fio, mobile, legal, requisites, roles}';
            return $this->apiResponse();
        }

        $repository = $em->getRepository(User::class);
        $user = $repository->find($request->get('id') ?? 0);

        if(!$user) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Пользователь с id='.$request->get('id').' не найден';
            return $this->apiResponse();
        }

        $user->setEmail($request->get('email'));
        $user->setFio($request->get('fio'));
        $user->setMobile($request->get('mobile'));
        $user->setAddress($request->get('address'));
        $user->setLegal($request->get('legal'));
        $user->setRequisites($request->get('requisites'));
        $user->setRoles($request->get('roles'));

        $errors = $validator->validate($user);

        if(count($errors) > 0) {
            $this->status = Response::HTTP_BAD_REQUEST;
            foreach ($errors as $error)
                $this->data['errors'][] = $error->getMessage();
        } else {
            $em->flush();
        }

        return $this->apiResponse();
    }

    /**
     * @Route("/update-lk", name="apiUserUpdateFromLK")
     */
    public function updateFromLK(EntityManagerInterface $em, Request $request, ValidatorInterface $validator)
    {
        if(!$request->isMethod('POST')) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Требуется POST запрос с параметрами {email, fio, mobile, legal, requisites}';
            return $this->apiResponse();
        }

        if(null === $this->getUser()) {
            $this->status = Response::HTTP_UNAUTHORIZED;
            $this->data['errors'][] = 'Требуется авторизация';
            return $this->apiResponse();
        }

        $this->getUser()->setEmail($request->get('email'));
        $this->getUser()->setFio($request->get('fio'));
        $this->getUser()->setMobile($request->get('mobile'));
        $this->getUser()->setAddress($request->get('address'));
        $this->getUser()->setLegal($request->get('legal'));
        $this->getUser()->setRequisites($request->get('requisites'));

        $errors = $validator->validate($this->getUser());

        if(count($errors) > 0) {
            $this->status = Response::HTTP_BAD_REQUEST;
            foreach ($errors as $error)
                $this->data['errors'][] = $error->getMessage();
        } else {
            $em->flush();
        }

        return $this->apiResponse();
    }

    /**
     * @Security("is_granted('ROLE_GOD') or is_granted('ROLE_ADMIN')")
     * @Route("/info", name="apiUserInfo")
     */
    public function info()
    {
        if(null === $this->getUser()) {
            $this->status = Response::HTTP_UNAUTHORIZED;
            $this->data['errors'][] = 'Требуется авторизация';
        } else {
            $this->data['item'] = $this->getUser();
        }

        return $this->apiResponse();
    }
}
