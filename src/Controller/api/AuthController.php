<?php

namespace App\Controller\api;

use App\Entity\User;
use App\Service\Recaptcha;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use SymfonyCasts\Bundle\ResetPassword\Controller\ResetPasswordControllerTrait;
use SymfonyCasts\Bundle\ResetPassword\Exception\ResetPasswordExceptionInterface;
use SymfonyCasts\Bundle\ResetPassword\ResetPasswordHelperInterface;

/**
 * @Route("/api/auth")
 */
class AuthController extends ApiController
{
    use ResetPasswordControllerTrait;

    /**
     * @Route("/login", name="apiLogin")
     */
    public function login(Request $request)
    {
        if(!$request->isMethod('POST')) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Требуется POST запрос с параметрами {login, password}';
            return $this->apiResponse();
        }

        //Аутентификация обрабатывается классом Security/LoginApiAuthenticator всё происходит в автоматическом режиме

        return $this->apiResponse();
    }

    /**
     * @Route("/logout", name="apiLogout")
     */
    public function logout()
    {
        //Выход осуществляется автоматически и редиректится на
    }

    /**
     * @Route("/logout-message", name="apiLogoutMessage")
     */
    public function logoutMessage()
    {
        return $this->apiResponse();
    }

    /**
     * @Route("/check", name="apiAuthCheck")
     */
    public function check()
    {
        if(null === $this->getUser()) {
            $this->status = Response::HTTP_UNAUTHORIZED;
            $this->data['errors'][] = 'Требуется авторизация';
        }

        return $this->apiResponse();
    }

    /**
     * @Route("/registration", name="apiRegistration")
     */
    public function registration(EntityManagerInterface $em, Request $request, UserPasswordEncoderInterface $passwordEncoder, ValidatorInterface $validator, Recaptcha $recaptcha)
    {
        if(!$request->isMethod('POST')) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Требуется POST запрос с параметрами {email, password}';
            return $this->apiResponse();
        }

        if(null !== $this->getUser()) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Пользователь авторизован, для регистрации нового пользователя необходимо выйти из системы';
            return $this->apiResponse();
        }

        if(!$recaptcha->check($request->get('g-recaptcha-response'))) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Система определила, что вы робот.';
            return $this->apiResponse();
        }

        $user = new User();

        if(!empty($request->get('email')))
            $user->setEmail($request->get('email'));

        if(!empty($request->get('password')))
            $user->setPassword($passwordEncoder->encodePassword($user, $request->get('password')));

        $user->setRoles(['ROLE_USER']);

        if(!empty($request->get('fio')))
            $user->setFio($request->get('fio'));

        if(!empty($request->get('mobile')))
            $user->setMobile($request->get('mobile'));

        if(!empty($request->get('address')))
            $user->setAddress($request->get('address'));

        if(!empty($request->get('legal')))
            $user->setLegal(true);
        else
            $user->setLegal(false);

        if(!empty($request->get('requisites')))
            $user->setRequisites($request->get('requisites'));

        if(!empty($request->get('subscribe')))
            $user->setSubscribe(true);
        else
            $user->setSubscribe(false);

        $user->setAdded(new DateTime());

        $errors = $validator->validate($user);

        if(count($errors) > 0) {
            $this->status = Response::HTTP_BAD_REQUEST;
            foreach ($errors as $error)
                $this->data['errors'][] = $error->getMessage();
        } else {
            $em->persist($user);
            $em->flush();

            $this->data['id'] = $user->getId();
        }

        return $this->apiResponse();
    }

    /**
     * @Route("/restore", name="apiRestore")
     */
    public function restore(Request $request, EntityManagerInterface $em, ResetPasswordHelperInterface $resetPasswordHelper, MailerInterface $mailer)
    {
        if(!$request->isMethod('POST')) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Требуется POST запрос с параметрами {login}';
            return $this->apiResponse();
        }

        $user = $em->getRepository(User::class)->findOneBy(['email' => $request->get('login')]);

        $this->setCanCheckEmailInSession();

        if (!$user) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Пользователь с login='.$request->get('login').' не найден';
            return $this->apiResponse();
        }

        try {
            $resetToken = $resetPasswordHelper->generateResetToken($user);
        } catch (ResetPasswordExceptionInterface $e) {
            $this->status = Response::HTTP_INTERNAL_SERVER_ERROR;
            $this->data['errors'][] = $e->getReason();
            return $this->apiResponse();
        }

        $email = (new TemplatedEmail())
            ->from(new Address('suvenir.promisel@gmail.com', '«Сувенирный промысел»'))
            ->to(($user instanceof User) ? $user->getEmail() : null)
            ->subject('Ваш запрос на сброс пароля')
            ->htmlTemplate('pieces/restore.email.html.twig')
            ->context([
                'resetToken' => $resetToken,
                'tokenLifetime' => $resetPasswordHelper->getTokenLifetime(),
            ]);

        try {
            $mailer->send($email);
        } catch (TransportExceptionInterface $e) {
            $this->status = Response::HTTP_INTERNAL_SERVER_ERROR;
            $this->data['errors'][] = $e->getMessage();
            return $this->apiResponse();
        }

        $this->data['message'] = 'Вам было отправлено электронное письмо со ссылкой, по которой вы можете перейти, чтобы сбросить пароль. Ссылка будет активна ' . date('g', $resetPasswordHelper->getTokenLifetime()) . ' часа.';

        return $this->apiResponse();
    }
}
