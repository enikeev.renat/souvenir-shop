<?php

namespace App\Controller\api;

use App\Entity\Order;
use App\Entity\OrderStatus;
use App\Repository\OrderRepository;
use App\Repository\OrderStatusRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Security("is_granted('ROLE_GOD') or is_granted('ROLE_ADMIN')")
 * @Route("/api/order")
 */
class OrderController extends ApiController
{
    /**
     * @Route("/get-all", name="apiOrderGetAll")
     */
    public function getAll(OrderRepository $repository, Request $request)
    {
        $orderBy = [];
        if(null !== $request->get('sortName') && null !== $request->get('sortDirection'))
            $orderBy = [$request->get('sortName') => $request->get('sortDirection')];

        $this->data['items'] = $repository->criteriaQB($orderBy, $request->get('query'), $request->get('limit'), $request->get('offset'));
        $this->data['total'] = $repository->criteriaQBCount($request->get('query'));

        return $this->apiResponse();
    }

    /**
     * @Route("/get", name="apiOrderGet")
     */
    public function getOne(OrderRepository $repository, Request $request)
    {
        if(!$request->isMethod('POST')) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Требуется POST запрос с параметрами {id}';
            return $this->apiResponse();
        }

        $order = $repository->find($request->get('id') ?? 0);

        if(null !== $order) {
            $this->data['item'] = $order;
        } else {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Заказ с id='.$request->get('id').' не найден';
        }

        return $this->apiResponse();
    }

    /**
     * @Route("/update", name="apiOrderUpdate")
     */
    public function update(EntityManagerInterface $em, Request $request, ValidatorInterface $validator)
    {
        if(!$request->isMethod('POST')) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Требуется POST запрос с параметрами {id, status, description}';
            return $this->apiResponse();
        }

        $repository = $em->getRepository(Order::class);
        $order = $repository->find($request->get('id') ?? 0);

        if(null !== $order && $order instanceof Order) {
            if(!empty($request->get('status'))) {
                $status = $em->getRepository(OrderStatus::class)->find($request->get('status') ?? 0);
                if(null !== $status && $status instanceof OrderStatus)
                    $order->setStatus($status);
            }

            if(!empty($request->get('description')))
                $order->setDescription($request->get('description'));

            $errors = $validator->validate($order);

            if(count($errors) > 0) {
                $this->status = Response::HTTP_BAD_REQUEST;
                foreach ($errors as $error)
                    $this->data['errors'][] = $error->getMessage();
            } else {
                $em->flush();
            }
        } else {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Заказ с id='.$request->get('id').' не найден';
        }

        return $this->apiResponse();
    }

    /**
     * @Route("/delete", name="apiOrderDelete")
     */
    public function delete(EntityManagerInterface $em, Request $request)
    {
        if(!$request->isMethod('POST')) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Требуется POST запрос с параметрами {id}';
            return $this->apiResponse();
        }

        $repository = $em->getRepository(Order::class);
        $order = $repository->find($request->get('id') ?? 0);

        if(null !== $order) {
            $em->remove($order);
            $em->flush();
        } else {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Заказ с id='.$request->get('id').' не найден';
        }

        return $this->apiResponse();
    }

    /**
     * @Route("/get-statuses", name="apiOrderStatusGetAll")
     */
    public function getStatuses(OrderStatusRepository $repository)
    {
        $this->data['items'] = $repository->findAll();

        $this->data['total'] = $repository->count([]);

        return $this->apiResponse();
    }
}