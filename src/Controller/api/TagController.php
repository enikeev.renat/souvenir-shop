<?php

namespace App\Controller\api;

use App\Entity\Tag;
use App\Repository\TagRepository;
use App\Service\SNHandler;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Security("is_granted('ROLE_GOD') or is_granted('ROLE_ADMIN')")
 * @Route("/api/tag")
 */
class TagController extends ApiController
{
    /**
     * @Route("/get-all", name="apiTagGetAll")
     */
    public function getAll(TagRepository $repository, Request $request, SNHandler $SNHandler)
    {
        $tags = $repository->findBy([], ['id' => 'DESC'], $request->get('limit'), $request->get('offset'));

        $this->data['items'] = $SNHandler->getSerializer()->normalize($tags, null, ['groups' => 'api', ObjectNormalizer::ENABLE_MAX_DEPTH => true]);

        $this->data['total'] = $repository->count([]);

        return $this->apiResponse();
    }

    /**
     * @Route("/get", name="apiTagGet")
     */
    public function getOne(TagRepository $repository, Request $request, SNHandler $SNHandler)
    {
        if(!$request->isMethod('POST')) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Требуется POST запрос с параметрами {id}';
            return $this->apiResponse();
        }

        $tag = $repository->find($request->get('id', 0) !== null ? $request->get('id', 0) : 0);

        if(null !== $tag) {
            $this->data['item'] = $SNHandler->getSerializer()->normalize($tag, null, ['groups' => 'api', ObjectNormalizer::ENABLE_MAX_DEPTH => true]);
        } else {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Тэг с id='.$request->get('id').' не найден';
        }

        return $this->apiResponse();
    }

    /**
     * @Route("/new", name="apiTagNew")
     */
    public function new(EntityManagerInterface $em, Request $request, ValidatorInterface $validator)
    {
        if(!$request->isMethod('POST')) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Требуется POST запрос с параметрами {name, description}';
            return $this->apiResponse();
        }

        $tag = new Tag();

        if(!empty($request->get('name')))
            $tag->setName($request->get('name'));

        $tag->setDescription($request->get('description'));

        $errors = $validator->validate($tag);

        if(count($errors) > 0) {
            $this->status = Response::HTTP_BAD_REQUEST;
            foreach ($errors as $error)
                $this->data['errors'][] = $error->getMessage();
        } else {
            $em->persist($tag);
            $em->flush();

            $this->data['id'] = $tag->getId();
        }

        return $this->apiResponse();
    }

    /**
     * @Route("/update", name="apiTagUpdate")
     */
    public function update(EntityManagerInterface $em, Request $request, ValidatorInterface $validator)
    {
        if(!$request->isMethod('POST')) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Требуется POST запрос с параметрами {id, name, description}';
            return $this->apiResponse();
        }

        $repository = $em->getRepository(Tag::class);
        $tag = $repository->find($request->get('id', 0) !== null ? $request->get('id', 0) : 0);

        if(null !== $tag && $tag instanceof Tag) {
            if(!empty($request->get('name')))
                $tag->setName($request->get('name'));

            $tag->setDescription($request->get('description'));

            $errors = $validator->validate($tag);

            if(count($errors) > 0) {
                $this->status = Response::HTTP_BAD_REQUEST;
                foreach ($errors as $error)
                    $this->data['errors'][] = $error->getMessage();
            } else {
                $em->flush();
            }
        } else {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Тэг с id='.$request->get('id').' не найден';
        }

        return $this->apiResponse();
    }

    /**
     * @Route("/delete", name="apiTagDelete")
     */
    public function delete(EntityManagerInterface $em, Request $request)
    {
        if(!$request->isMethod('POST')) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Требуется POST запрос с параметрами {id}';
            return $this->apiResponse();
        }

        $repository = $em->getRepository(Tag::class);
        $tag = $repository->find($request->get('id', 0) !== null ? $request->get('id', 0) : 0);

        if(null !== $tag) {
            $em->remove($tag);
            $em->flush();
        } else {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Тэг с id='.$request->get('id').' не найден';
        }

        return $this->apiResponse();
    }
}