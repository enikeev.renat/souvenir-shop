<?php


namespace App\Controller\api;

use App\Entity\Feedback;
use App\Repository\FeedbackRepository;
use App\Service\Recaptcha;
use App\Service\SNHandler;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/api/feedback")
 */
class FeedbackController extends ApiController
{
    /**
     * @Route("/new", name="apiFeedbackNew")
     */
    public function new(EntityManagerInterface $em, Request $request, ValidatorInterface $validator, Recaptcha $recaptcha)
    {
        if(!$request->isMethod('POST')) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Требуется POST запрос с параметрами {url, email, fio, message}';
            return $this->apiResponse();
        }

        if(!$recaptcha->check($request->get('g-recaptcha-response'))) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Система определила, что вы робот.';
            return $this->apiResponse();
        }

        $feedback = new Feedback();

        if(!empty($request->get('url')))
            $feedback->setUrl($request->get('url'));

        if(!empty($request->get('email')))
            $feedback->setEmail($request->get('email'));

        if(!empty($request->get('fio')))
            $feedback->setFio($request->get('fio'));

        if(!empty($request->get('message')))
            $feedback->setMessage($request->get('message'));

        $feedback->setAdded(new DateTime());

        $feedback->setCompleted(false);

        $errors = $validator->validate($feedback);

        if(count($errors) > 0) {
            $this->status = Response::HTTP_BAD_REQUEST;
            foreach ($errors as $error)
                $this->data['errors'][] = $error->getMessage();
        } else {
            $em->persist($feedback);
            $em->flush();

            $this->data['id'] = $feedback->getId();
            $this->data['message'] = 'Спасибо! Ваше сообщение отправлено.';
        }

        return $this->apiResponse();
    }

    /**
     * @Security("is_granted('ROLE_GOD') or is_granted('ROLE_ADMIN')")
     * @Route("/get-all", name="apiFeedbackGetAll")
     */
    public function getAll(FeedbackRepository $repository, Request $request, SNHandler $SNHandler)
    {
        $orderBy = [];
        if(null !== $request->get('sortName') && null !== $request->get('sortDirection'))
            $orderBy = [$request->get('sortName') => $request->get('sortDirection')];

        $feedbacks = $repository->criteria($orderBy, $request->get('query'), $request->get('limit'), $request->get('offset'));

        $this->data['items'] = $SNHandler->getSerializer()->normalize($feedbacks, null, ['groups' => 'api', ObjectNormalizer::ENABLE_MAX_DEPTH => true]);

        $this->data['total'] = $repository->criteriaCount($request->get('query'));

        return $this->apiResponse();
    }

    /**
     * @Security("is_granted('ROLE_GOD') or is_granted('ROLE_ADMIN')")
     * @Route("/get", name="apiFeedbackGet")
     */
    public function getOne(FeedbackRepository $repository, Request $request, SNHandler $SNHandler)
    {
        if(!$request->isMethod('POST')) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Требуется POST запрос с параметрами {id}';
            return $this->apiResponse();
        }

        $feedback = $repository->find($request->get('id', 0) !== null ? $request->get('id', 0) : 0);

        if(null !== $feedback) {
            $this->data['item'] = $SNHandler->getSerializer()->normalize($feedback, null, ['groups' => 'api', ObjectNormalizer::ENABLE_MAX_DEPTH => true]);
        } else {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Обратная связь с id='.$request->get('id').' не найдена';
        }

        return $this->apiResponse();
    }

    /**
     * @Security("is_granted('ROLE_GOD') or is_granted('ROLE_ADMIN')")
     * @Route("/update", name="apiFeedbackUpdate")
     */
    public function update(EntityManagerInterface $em, Request $request, ValidatorInterface $validator)
    {
        if(!$request->isMethod('POST')) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Требуется POST запрос с параметрами {id, description, completed}';
            return $this->apiResponse();
        }

        $repository = $em->getRepository(Feedback::class);
        $feedback = $repository->find($request->get('id', 0) !== null ? $request->get('id', 0) : 0);

        if(null !== $feedback && $feedback instanceof Feedback) {
            $feedback->setDescription($request->get('description'));

            if(!empty($request->get('completed')))
                $feedback->setCompleted(($request->get('completed') == 'false') ? false : true);

            $feedback->setResponsible($this->getUser());

            $errors = $validator->validate($feedback);

            if(count($errors) > 0) {
                $this->status = Response::HTTP_BAD_REQUEST;
                foreach ($errors as $error)
                    $this->data['errors'][] = $error->getMessage();
            } else {
                $em->flush();
            }
        } else {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Обратная связь с id='.$request->get('id').' не найдена';
        }

        return $this->apiResponse();
    }

    /**
     * @Security("is_granted('ROLE_GOD') or is_granted('ROLE_ADMIN')")
     * @Route("/delete", name="apiFeedbackDelete")
     */
    public function delete(EntityManagerInterface $em, Request $request)
    {
        if(!$request->isMethod('POST')) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Требуется POST запрос с параметрами {id}';
            return $this->apiResponse();
        }

        $repository = $em->getRepository(Feedback::class);
        $feedback = $repository->find($request->get('id', 0) !== null ? $request->get('id', 0) : 0);

        if(null !== $feedback) {
            $em->remove($feedback);
            $em->flush();
        } else {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Обратная связь с id='.$request->get('id').' не найдена';
        }

        return $this->apiResponse();
    }
}