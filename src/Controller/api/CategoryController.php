<?php

namespace App\Controller\api;

use App\Entity\Category;
use App\Service\ApiImageHandler;
use App\Service\SNHandler;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Security("is_granted('ROLE_GOD') or is_granted('ROLE_ADMIN')")
 * @Route("/api/category")
 */
class CategoryController extends ApiController
{
    /**
     * @Route("/get-tree", name="apiCategoryGetTree")
     */
    public function getTree(EntityManagerInterface $em)
    {
        $repository = $em->getRepository(Category::class);

        $categories = $repository->childrenHierarchy();

        $this->data['items'] = $categories;

        return $this->apiResponse();
    }

    /**
     * @Route("/get-all", name="apiCategoryGetAll")
     */
    public function getAll(EntityManagerInterface $em, Request $request, SNHandler $SNHandler, ApiImageHandler $imageHandler)
    {
        $repository = $em->getRepository(Category::class);

        $categories = $repository->findBy([], ['id' => 'DESC']);
        foreach($categories as $category) {
            $category->setVisual($imageHandler->getVisual($category->getVisualFilename()));
            foreach ($category->getChildren() as $children) {
                if($children instanceof Category)
                    $children->setVisual($imageHandler->getVisual($children->getVisualFilename()));
            }
        }

        $this->data['items'] = $SNHandler->getSerializer()->normalize($categories, null, ['groups' => 'api', ObjectNormalizer::ENABLE_MAX_DEPTH => true, 'circular_reference_handler' => function ($object) {
            return $object->getId();
        }]);

        return $this->apiResponse();
    }

    /**
     * @Route("/get", name="apiCategoryGet")
     */
    public function getOne(EntityManagerInterface $em, Request $request, SNHandler $SNHandler, ApiImageHandler $imageHandler)
    {
        if(!$request->isMethod('POST')) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Требуется POST запрос с параметрами {id}';
            return $this->apiResponse();
        }

        $repository = $em->getRepository(Category::class);

        $category = $repository->find($request->get('id', 0) !== null ? $request->get('id', 0) : 0);

        if(null !== $category) {
            $category->setVisual($imageHandler->getVisual($category->getVisualFilename()));
            $this->data['item'] = $SNHandler->getSerializer()->normalize($category, null, ['groups' => 'api', ObjectNormalizer::ENABLE_MAX_DEPTH => true, 'circular_reference_handler' => function ($object) {
                return $object->getId();
            }]);
        } else {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Категория с id='.$request->get('id').' не найдена';
        }

        return $this->apiResponse();
    }

    /**
     * @Route("/new", name="apiCategoryNew")
     */
    public function new(EntityManagerInterface $em, Request $request, ValidatorInterface $validator, ApiImageHandler $imageHandler)
    {
        if(!$request->isMethod('POST')) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Требуется POST запрос с параметрами {name, description, visual_filename, parent_id}';
            return $this->apiResponse();
        }

        $repository = $em->getRepository(Category::class);

        $category = new Category();

        if(!empty($request->get('name')))
            $category->setName($request->get('name'));

        $category->setDescription($request->get('description'));

        if(!empty($request->get('visual') && is_array($request->get('visual')))) {
            foreach ($request->get('visual') as $json) {
                $category->setVisualFilename($imageHandler->handleVisual($json));
            }
        }

        $parent = $repository->find($request->get('parent_id', 0) !== null ? $request->get('parent_id', 0) : 0);
        if($parent instanceof Category)
            $category->setParent($parent);

        $errors = $validator->validate($category);

        if(count($errors) > 0) {
            $this->status = Response::HTTP_BAD_REQUEST;
            foreach ($errors as $error)
                $this->data['errors'][] = $error->getMessage();
        } else {
            $em->persist($category);
            $em->flush();

            $this->data['id'] = $category->getId();
        }

        return $this->apiResponse();
    }

    /**
     * @Route("/update", name="apiCategoryUpdate")
     */
    public function update(EntityManagerInterface $em, Request $request, ValidatorInterface $validator, ApiImageHandler $imageHandler)
    {
        if(!$request->isMethod('POST')) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Требуется POST запрос с параметрами {id, name, description, visual_filename, parent_id}';
            return $this->apiResponse();
        }

        $repository = $em->getRepository(Category::class);
        $category = $repository->find($request->get('id', 0) !== null ? $request->get('id', 0) : 0);

        if(null !== $category && $category instanceof Category) {
            if(!empty($request->get('name')))
                $category->setName($request->get('name'));

            $category->setDescription($request->get('description'));

            if(!empty($request->get('visual') && is_array($request->get('visual')))) {
                foreach ($request->get('visual') as $json) {
                    $category->setVisualFilename($imageHandler->handleVisual($json));
                }
            }

            $parent = $repository->find($request->get('parent_id', 0) !== null ? $request->get('parent_id', 0) : 0);
            if($parent instanceof Category)
                $category->setParent($parent);

            $errors = $validator->validate($category);

            if(count($errors) > 0) {
                $this->status = Response::HTTP_BAD_REQUEST;
                foreach ($errors as $error)
                    $this->data['errors'][] = $error->getMessage();
            } else {
                $em->flush();
            }
        } else {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Категория с id='.$request->get('id').' не найдена';
        }

        return $this->apiResponse();
    }

    /**
     * @Route("/delete", name="apiCategoryDelete")
     */
    public function delete(EntityManagerInterface $em, Request $request, ApiImageHandler $imageHandler)
    {
        if(!$request->isMethod('POST')) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Требуется POST запрос с параметрами {id}';
            return $this->apiResponse();
        }

        $repository = $em->getRepository(Category::class);
        $category = $repository->find($request->get('id', 0) !== null ? $request->get('id', 0) : 0);

        if(null !== $category) {
            $imageHandler->delete($category->getVisualFilename());
            $em->remove($category);
            $em->flush();
        } else {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Категория с id='.$request->get('id').' не найдена';
        }

        return $this->apiResponse();
    }
}