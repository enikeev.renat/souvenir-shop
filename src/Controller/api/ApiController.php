<?php

namespace App\Controller\api;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class ApiController extends AbstractController
{
    protected $data;

    protected $status;

    protected $context;

    protected $headers;

    public function __construct()
    {
        $this->status = Response::HTTP_OK;
        $this->data['errors'] = null;
        $this->headers = [];
        $this->context = ['json_encode_options' => JSON_UNESCAPED_UNICODE];
    }

    public function apiResponse()
    {
        return $this->json($this->data, $this->status, $this->headers, $this->context);
    }
}
