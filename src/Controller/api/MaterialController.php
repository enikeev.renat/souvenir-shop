<?php

namespace App\Controller\api;

use App\Entity\Material;
use App\Repository\MaterialRepository;
use App\Service\ApiImageHandler;
use App\Service\SNHandler;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Security("is_granted('ROLE_GOD') or is_granted('ROLE_ADMIN')")
 * @Route("/api/material")
 */
class MaterialController extends ApiController
{
    /**
     * @Route("/get-all", name="apiMaterialGetAll")
     */
    public function getAll(MaterialRepository $repository, Request $request, SNHandler $SNHandler, ApiImageHandler $imageHandler)
    {
        $materials = $repository->findBy([], ['id' => 'DESC'], $request->get('limit'), $request->get('offset'));
        foreach($materials as $material) {
            $material->setVisual($imageHandler->getVisual($material->getVisualFilename()));
        }

        $this->data['items'] = $SNHandler->getSerializer()->normalize($materials, null, ['groups' => 'api', ObjectNormalizer::ENABLE_MAX_DEPTH => true]);

        $this->data['total'] = $repository->count([]);

        return $this->apiResponse();
    }

    /**
     * @Route("/get", name="apiMaterialGet")
     */
    public function getOne(MaterialRepository $repository, Request $request, SNHandler $SNHandler, ApiImageHandler $imageHandler)
    {
        if(!$request->isMethod('POST')) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Требуется POST запрос с параметрами {id}';
            return $this->apiResponse();
        }

        $material = $repository->find($request->get('id', 0) !== null ? $request->get('id', 0) : 0);

        if(null !== $material) {
            $material->setVisual($imageHandler->getVisual($material->getVisualFilename()));
            $this->data['item'] = $SNHandler->getSerializer()->normalize($material, null, ['groups' => 'api', ObjectNormalizer::ENABLE_MAX_DEPTH => true]);
        } else {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Материал с id='.$request->get('id').' не найден';
        }

        return $this->apiResponse();
    }

    /**
     * @Route("/new", name="apiMaterialNew")
     */
    public function new(EntityManagerInterface $em, Request $request, ValidatorInterface $validator, ApiImageHandler $imageHandler)
    {
        if(!$request->isMethod('POST')) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Требуется POST запрос с параметрами {name, description, visual}';
            return $this->apiResponse();
        }

        $material = new Material();

        if(!empty($request->get('name')))
            $material->setName($request->get('name'));

        $material->setDescription($request->get('description'));

        if(!empty($request->get('visual') && is_array($request->get('visual')))) {
            foreach ($request->get('visual') as $json) {
                $material->setVisualFilename($imageHandler->handleVisual($json));
            }
        }

        $errors = $validator->validate($material);

        if(count($errors) > 0) {
            $this->status = Response::HTTP_BAD_REQUEST;
            foreach ($errors as $error)
                $this->data['errors'][] = $error->getMessage();
        } else {
            $em->persist($material);
            $em->flush();

            $this->data['id'] = $material->getId();
        }

        return $this->apiResponse();
    }

    /**
     * @Route("/update", name="apiMaterialUpdate")
     */
    public function update(EntityManagerInterface $em, Request $request, ValidatorInterface $validator, ApiImageHandler $imageHandler)
    {
        if(!$request->isMethod('POST')) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Требуется POST запрос с параметрами {id, name, description, visual}';
            return $this->apiResponse();
        }

        $repository = $em->getRepository(Material::class);
        $material = $repository->find($request->get('id', 0) !== null ? $request->get('id', 0) : 0);

        if(null !== $material && $material instanceof Material) {
            if(!empty($request->get('name')))
                $material->setName($request->get('name'));

            $material->setDescription($request->get('description'));

            if(!empty($request->get('visual') && is_array($request->get('visual')))) {
                foreach ($request->get('visual') as $json) {
                    $material->setVisualFilename($imageHandler->handleVisual($json));
                }
            }

            $errors = $validator->validate($material);

            if(count($errors) > 0) {
                $this->status = Response::HTTP_BAD_REQUEST;
                foreach ($errors as $error)
                    $this->data['errors'][] = $error->getMessage();
            } else {
                $em->flush();
            }
        } else {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Материал с id='.$request->get('id').' не найден';
        }

        return $this->apiResponse();
    }

    /**
     * @Route("/delete", name="apiMaterialDelete")
     */
    public function delete(EntityManagerInterface $em, Request $request, ApiImageHandler $imageHandler)
    {
        if(!$request->isMethod('POST')) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Требуется POST запрос с параметрами {id}';
            return $this->apiResponse();
        }

        $repository = $em->getRepository(Material::class);
        $material = $repository->find($request->get('id', 0) !== null ? $request->get('id', 0) : 0);

        if(null !== $material) {
            $imageHandler->delete($material->getVisualFilename());
            $em->remove($material);
            $em->flush();
        } else {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Материал с id='.$request->get('id').' не найден';
        }

        return $this->apiResponse();
    }
}