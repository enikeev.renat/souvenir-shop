<?php

namespace App\Controller\api;

use App\Entity\Category;
use App\Entity\Material;
use App\Entity\Product;
use App\Entity\ProductImage;
use App\Entity\Tag;
use App\Repository\ProductRepository;
use App\Service\ApiImageHandler;
use App\Service\SNHandler;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Security("is_granted('ROLE_GOD') or is_granted('ROLE_ADMIN')")
 * @Route("/api/product")
 */
class ProductController extends ApiController
{
    /**
     * @Route("/get-all", name="apiProductGetAll")
     */
    public function getAll(ProductRepository $repository, Request $request, SNHandler $SNHandler, ApiImageHandler $imageHandler)
    {
        $orderBy = [];
        if(null !== $request->get('sortName') && null !== $request->get('sortDirection'))
            $orderBy = [$request->get('sortName') => $request->get('sortDirection')];

        $products = $repository->criteria(false, $orderBy, $request->get('query'), $request->get('limit'), $request->get('offset'));

        foreach ($products as $product) {
            foreach ($product->getProductGallery() as $image) {
                $image->setVisual($imageHandler->getVisual($image->getFilename()));
            }
        }

        $this->data['items'] = $SNHandler->getSerializer()->normalize($products, null, ['groups' => 'api', ObjectNormalizer::ENABLE_MAX_DEPTH => true]);

        $this->data['total'] = $repository->criteriaCount(false, $request->get('query'));

        return $this->apiResponse();
    }

    /**
     * @Route("/get", name="apiProductGet")
     */
    public function getOne(ProductRepository $repository, Request $request, SNHandler $SNHandler, ApiImageHandler $imageHandler)
    {
        if(!$request->isMethod('POST')) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Требуется POST запрос с параметрами {id}';
            return $this->apiResponse();
        }

        $product = $repository->find($request->get('id', 0) !== null ? $request->get('id', 0) : 0);

        if(null !== $product) {
            foreach ($product->getProductGallery() as $image) {
                $image->setVisual($imageHandler->getVisual($image->getFilename()));
            }
            $this->data['item'] = $SNHandler->getSerializer()->normalize($product, null, ['groups' => 'api', ObjectNormalizer::ENABLE_MAX_DEPTH => true]);
        } else {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Продукт с id='.$request->get('id').' не найден';
        }

        return $this->apiResponse();
    }

    /**
     * @Route("/new", name="apiProductNew")
     */
    public function new(EntityManagerInterface $em, Request $request, ValidatorInterface $validator, ApiImageHandler $imageHandler)
    {
        if(!$request->isMethod('POST')) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Требуется POST запрос с параметрами {name, vendor_code, description, price, available, published, product_gallery, category_id, materials, tags}';
            return $this->apiResponse();
        }

        $product = new Product();

        if(!empty($request->get('name')))
            $product->setName($request->get('name'));

        $product->setVendorCode($request->get('vendor_code'));

        $product->setDescription($request->get('description'));

        $product->setPrice($request->get('price'));

        $product->setAvailable($request->get('available'));

        if(!empty($request->get('published')))
            $product->setPublished(($request->get('published') == 'false') ? false : true);

        $category = $em->getRepository(Category::class)->find($request->get('category_id', 0) !== null ? $request->get('category_id', 0) : 0);
        if($category instanceof Category || $category === null)
            $product->setCategory($category);

        if(!empty($request->get('tags') && is_array($request->get('tags')))) {
            $tagRepository = $em->getRepository(Tag::class);
            foreach ($request->get('tags') as $id) {
                $tag = $tagRepository->find($id);
                if(null !== $tag && $tag instanceof Tag)
                    $product->addTag($tag);
            }
        }

        if(!empty($request->get('materials') && is_array($request->get('materials')))) {
            $materialRepository = $em->getRepository(Material::class);
            foreach ($request->get('materials') as $id) {
                $material = $materialRepository->find($id);
                if(null !== $material && $material instanceof Material)
                    $product->addMaterial($material);
            }
        }

        $product->setAdded(new DateTime());

        $errors = $validator->validate($product);

        if(count($errors) > 0) {
            $this->status = Response::HTTP_BAD_REQUEST;
            foreach ($errors as $error)
                $this->data['errors'][] = $error->getMessage();
        } else {
            if(!empty($request->get('product_gallery') && is_array($request->get('product_gallery')))) {
                foreach ($request->get('product_gallery') as $json) {
                    $filename = $imageHandler->handleVisual($json);
                    if(!$filename)
                        continue;
                    $productImage = new ProductImage();
                    $productImage->setFilename($filename);
                    $productImage->setProduct($product);
                    $em->persist($productImage);
                }
            }
            $em->persist($product);
            $em->flush();

            $this->data['id'] = $product->getId();
        }

        return $this->apiResponse();
    }

    /**
     * @Route("/update", name="apiProductUpdate")
     */
    public function update(EntityManagerInterface $em, Request $request, ValidatorInterface $validator, ApiImageHandler $imageHandler)
    {
        if(!$request->isMethod('POST')) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Требуется POST запрос с параметрами {id, name, vendor_code, description, price, available, published, product_gallery, category_id, materials, tags}';
            return $this->apiResponse();
        }

        $repository = $em->getRepository(Product::class);
        $product = $repository->find($request->get('id', 0) !== null ? $request->get('id', 0) : 0);

        if(null !== $product && $product instanceof Product) {
            if(!empty($request->get('name')))
                $product->setName($request->get('name'));

            $product->setVendorCode($request->get('vendor_code'));

            $product->setDescription($request->get('description'));

            $product->setPrice($request->get('price'));

            $product->setAvailable($request->get('available'));

            if(!empty($request->get('published')))
                $product->setPublished(($request->get('published') == 'false') ? false : true);

            $category = $em->getRepository(Category::class)->find($request->get('category_id', 0) !== null ? $request->get('category_id', 0) : 0);
            if($category instanceof Category || $category === null)
                $product->setCategory($category);

            if(!empty($request->get('tags') && is_array($request->get('tags')))) {
                $tagRepository = $em->getRepository(Tag::class);
                $product->getTags()->clear();
                foreach ($request->get('tags') as $id) {
                    $tag = $tagRepository->find($id);
                    if(null !== $tag && $tag instanceof Tag)
                        $product->addTag($tag);
                }
            }

            if(!empty($request->get('materials') && is_array($request->get('materials')))) {
                $materialRepository = $em->getRepository(Material::class);
                $product->getMaterials()->clear();
                foreach ($request->get('materials') as $id) {
                    $material = $materialRepository->find($id);
                    if(null !== $material && $material instanceof Material)
                        $product->addMaterial($material);
                }
            }

            $errors = $validator->validate($product);

            if(count($errors) > 0) {
                $this->status = Response::HTTP_BAD_REQUEST;
                foreach ($errors as $error)
                    $this->data['errors'][] = $error->getMessage();
            } else {
                if(!empty($request->get('product_gallery') && is_array($request->get('product_gallery')))) {
                    $product->getProductGallery()->clear();
                    foreach ($request->get('product_gallery') as $json) {
                        $filename = $imageHandler->handleVisual($json);
                        if(!$filename)
                            continue;
                        $productImage = new ProductImage();
                        $productImage->setFilename($filename);
                        $productImage->setProduct($product);
                        $em->persist($productImage);
                    }
                }
                $em->flush();
            }
        } else {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Продукт с id='.$request->get('id').' не найден';
        }

        return $this->apiResponse();
    }

    /**
     * @Route("/delete", name="apiProductDelete")
     */
    public function delete(EntityManagerInterface $em, Request $request, ApiImageHandler $imageHandler)
    {
        if(!$request->isMethod('POST')) {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Требуется POST запрос с параметрами {id}';
            return $this->apiResponse();
        }

        $repository = $em->getRepository(Product::class);
        $product = $repository->find($request->get('id', 0) !== null ? $request->get('id', 0) : 0);

        if(null !== $product && $product instanceof Product) {
            if(!$product->getProductGallery()->isEmpty()) {
                foreach ($product->getProductGallery()->getIterator() as $key => $item) {
                    if($item instanceof ProductImage)
                        $imageHandler->delete($item->getFilename());
                }
            }
            $em->remove($product);
            $em->flush();
        } else {
            $this->status = Response::HTTP_BAD_REQUEST;
            $this->data['errors'][] = 'Продукт с id='.$request->get('id').' не найден';
        }

        return $this->apiResponse();
    }
}