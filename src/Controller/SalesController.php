<?php

namespace App\Controller;

use App\Repository\SaleRepository;
use Symfony\Component\Routing\Annotation\Route;

class SalesController extends BaseController
{
    /**
     * @Route("/sales", name="sales")
     */
    public function index(SaleRepository $repository)
    {
        $this->data["title"] = "Скидки";
        $this->data['keywords'] = "Скидки";
        $this->data['description'] = "Скидки";

        $this->data['sales'] = $repository->findAll();

        return $this->render('sales/index.html.twig', $this->data);
    }
}
