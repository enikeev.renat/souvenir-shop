<?php

namespace App\DataFixtures;

use App\Entity\OrderStatus;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * for use fixtures:
 * composer require orm-fixtures --dev
 * php bin/console make:fixtures
 * after added and rewrite fixture class load it:
 * php bin/console doctrine:fixtures:load --append
 */
class UserFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $this->fillUsers($manager);
        $this->fillOrderStatuses($manager);
    }

    private function fillUsers(ObjectManager $manager)
    {
        $user = new User();
        $user->setEmail('admin@admin.ru');
        $user->setRoles(['ROLE_GOD']);
        $user->setPassword($this->passwordEncoder->encodePassword($user, '123456'));

        $manager->persist($user);
        $manager->flush();

        $user = new User();
        $user->setEmail('dan_spb_ru@mail.ru');
        $user->setRoles(['ROLE_GOD']);
        $user->setPassword($this->passwordEncoder->encodePassword($user, '3934137'));

        $manager->persist($user);
        $manager->flush();
    }

    private function fillOrderStatuses(ObjectManager $manager)
    {
        $status = new OrderStatus();
        $status->setValue('заказ в обработке');

        $manager->persist($status);
        $manager->flush();

        $status = new OrderStatus();
        $status->setValue('заказ подтвержден');

        $manager->persist($status);
        $manager->flush();

        $status = new OrderStatus();
        $status->setValue('заказ отправлен');

        $manager->persist($status);
        $manager->flush();

        $status = new OrderStatus();
        $status->setValue('заказ выполнен');

        $manager->persist($status);
        $manager->flush();

        $status = new OrderStatus();
        $status->setValue('заказ отклонен');

        $manager->persist($status);
        $manager->flush();
    }
}
