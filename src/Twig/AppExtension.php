<?php


namespace App\Twig;


use App\Entity\Category;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class AppExtension extends AbstractExtension
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getFilters()
    {
        return [
            new TwigFilter('visual', [$this, 'getVisualPath']),
            new TwigFilter('path', [$this, 'getCatalogPath']),
            new TwigFilter('catalogBreadcrumbs', [$this, 'getCatalogBreadcrumbs']),
            new TwigFilter('crop', [$this, 'cropText']),
            new TwigFilter('declension', [$this, 'declensionText']),
        ];
    }

    public function getVisualPath($filename)
    {
        return mb_substr($filename, 0, 2) . '/' . mb_substr($filename, 2, 2) . '/' . $filename;
    }

    public function getCatalogPath($id)
    {
        $category = $this->em->getRepository(Category::class)->find($id);
        $pathCategories = $this->em->getRepository(Category::class)->getPath($category);
        $path = [];
        foreach ($pathCategories as $pathCategory) {
            $path[] = $pathCategory->getSlug();
        }
        return implode("/", $path);
    }

    public function getCatalogBreadcrumbs($id)
    {
        $category = $this->em->getRepository(Category::class)->find($id);
        $pathCategories = $this->em->getRepository(Category::class)->getPath($category);
        $breadcrumbs = [];
        foreach ($pathCategories as $pathCategory) {
            $breadcrumbs[] = [
                'id' => $pathCategory->getId(),
                'slug' => $pathCategory->getSlug(),
                'name' => $pathCategory->getName()
            ];
        }

        if(count($breadcrumbs) > 1)
            unset($breadcrumbs[count($breadcrumbs) - 1]);
        else
            $breadcrumbs = [];

        return $breadcrumbs;
    }

	public function cropText($value, $limit = 10, $postfix = '...', $minLastLetter = 3)
	{
		if (empty($value)) return '';
		if ($limit <= 0) return $value;
		$text = trim(strval($value));
		if (strlen($text) <= $limit) return $text;
		$text = substr($text, 0, $limit);
		$arr = explode(" ", $text);
		$lastSpace = strrpos($text, ' ');
		if (strlen($arr[count($arr) - 1]) < $minLastLetter && $lastSpace > 0) {
			$text = substr($text, 0, $lastSpace);
		}
		return $text . $postfix;
	}

	public function declensionText($count, $case1 = '', $case2 = '', $case3 = '')
	{
		if (empty($count)) return '';
		return [$case1, $case2, $case3][($count%100>4 and $count%100<20)? 2 : [2, 0, 1, 1, 1, 2][($count%10<5)?$count%10:5]];
	}
}