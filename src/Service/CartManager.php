<?php


namespace App\Service;


use App\Entity\Cart;
use App\Entity\CartPrices;
use App\Entity\Product;
use App\Entity\Sale;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Security;

class CartManager
{
    private $security;

    private $session;

    private $em;

    public function __construct(Security $security, SessionInterface $session, EntityManagerInterface $em)
    {
        $this->security = $security;
        $this->session = $session;
        $this->em = $em;

        if(!$this->session->has('cart'))
            $this->session->set('cart', json_encode([]));

        $this->sync();
    }

    /**
     * Добавление товара в корзину
     * @param $id - id продукта
     * @return bool
     */
    public function add($id) : bool
    {
        $product = $this->em->getRepository(Product::class)->find($id);

        if(!$product)
            return false;

        if(null !== $this->security->getUser()) {
            $cart = $this->em->getRepository(Cart::class)->findBy(['user' => $this->security->getUser(), 'product' => $id]);
            if(!$cart) {
                $cart = new Cart();
                $cart->setUser($this->security->getUser());
                $cart->setProduct($product);
                $cart->setQty(1);

                $this->em->persist($cart);
                $this->em->flush();
                return true;
            } else {
                return false;
            }
        }

        $carts = json_decode($this->session->get('cart'), true);
        $check = false;
        foreach ($carts as $key => $cart) {
            if($cart['id'] == $id) {
                $check = true;
                break;
            }
        }

        if($check)
            return false;

        $carts[] = [
            'id' => $id,
            'qty' => 1
        ];

        $this->session->set('cart', json_encode($carts));

        return true;
    }

    /**
     * Удаление из корзины товара
     * @param $id - id продукта
     * @return bool
     */
    public function remove($id) : bool
    {
        if(null !== $this->security->getUser()) {
            $cart = $this->em->getRepository(Cart::class)->findOneBy(['user' => $this->security->getUser(), 'product' => $id]);
            if(!$cart) {
                return false;
            } else {
                $this->em->remove($cart);
                $this->em->flush();
                return true;
            }
        }

        $carts = json_decode($this->session->get('cart'), true);
        $check = false;
        foreach ($carts as $key => $cart) {
            if($cart['id'] == $id) {
                unset($carts[$key]);
                $check = true;
                break;
            }
        }

        if(!$check)
            return false;

        $this->session->set('cart', json_encode($carts));

        return true;
    }

    /**
     * Синхронизация
     * Если пользователь авторизовался, то производится перенос из сессии в БД
     */
    public function sync() : bool
    {
        if(!$this->security->getUser())
            return false;

        $carts = json_decode($this->session->get('cart'), true);
        foreach($carts as $cart) {
            $check = $this->em->getRepository(Cart::class)->findOneBy(['user' => $this->security->getUser(), 'product' => $cart['id']]);
            if(!$check) {
                $product = $this->em->getRepository(Product::class)->find($cart['id']);
                if(null !== $product) {
                    $cart = new Cart();
                    $cart->setUser($this->security->getUser());
                    $cart->setProduct($product);
                    $cart->setQty(1);

                    $this->em->persist($cart);
                }
            }
        }

        $this->em->flush();

        $this->session->set('cart', json_encode([]));

        return true;
    }

    /**
     * Очистка корзины
     */
    public function clear(): void
    {
        $this->session->set('cart', json_encode([]));

        if(null !== $this->security->getUser()) {
            $carts = $this->em->getRepository(Cart::class)->findBy(['user' => $this->security->getUser()]);
            foreach ($carts as $cart)
                $this->em->remove($cart);
            $this->em->flush();
        }
    }

    /**
     * Подсчет товаров в корзине
     */
    public function count() : int
    {
        if(null !== $this->security->getUser()) {
            return $this->em->getRepository(Cart::class)->count(['user' => $this->security->getUser()]);
        }

        return count(json_decode($this->session->get('cart'), true));
    }

    public function getIds() : array
    {
        $heap = [];

        if(null !== $this->security->getUser()) {
            $carts = $this->em->getRepository(Cart::class)->findBy(['user' => $this->security->getUser()]);
            foreach ($carts as $cart)
                $heap[] = $cart->getProduct()->getId();

            return $heap;
        }

        $carts = json_decode($this->session->get('cart'), true);
        foreach ($carts as $cart)
            $heap[] = $cart['id'];

        return $heap;
    }

    /**
     * Получение товаров в корзине
     * @return object[]|null
     */
    public function getCarts(): ?array
    {
        if(null !== $this->security->getUser()) {
            return $this->em->getRepository(Cart::class)->findBy(['user' => $this->security->getUser()]);
        }

        $sess = json_decode($this->session->get('cart'), true);

        $carts = [];
        foreach ($sess as $ses) {
            $product = $this->em->getRepository(Product::class)->find($ses['id']);
            if(null !== $product) {
                $cart = new Cart();
                $cart->setProduct($product);
                $cart->setQty($ses['qty']);
                $carts[] = $cart;
            }
        }

        return (count($carts) == 0) ? null : $carts;
    }

    /**
     * Получение детальной информации стоимости
     * @return CartPrices|null
     */
    public function getCartPrices(): ?CartPrices
    {
        $carts = $this->getCarts();

        if(!$carts)
            return null;

        $cartPrices = new CartPrices();
        $cartPrices->setGoodsCount(count($carts));
        foreach ($carts as $cart) {
            $cartPrices->setItemsCount($cartPrices->getItemsCount() + $cart->getQty());
            $cartPrices->setPriceWithoutDiscount($cartPrices->getPriceWithoutDiscount() + $cart->getProduct()->getPrice()*$cart->getQty());
        }

        $percent = $this->em->getRepository(Sale::class)->getDiscount($cartPrices->getPriceWithoutDiscount());

        $cartPrices->setDiscount($cartPrices->getPriceWithoutDiscount() * $percent / 100);
        $cartPrices->setPriceWithDiscount($cartPrices->getPriceWithoutDiscount() - $cartPrices->getDiscount());

        return $cartPrices;
    }

    /**
     * Обновление количества товара и возврат обновленной детальной информации стоимости
     * @param array $qtys
     * @return CartPrices|null
     */
    public function changeQty(array $qtys): ?CartPrices
    {
        if(null !== $this->security->getUser()) {
            $carts = $this->getCarts();
            foreach ($qtys as $key => $qty) {
                if(is_numeric($key)) {
                    foreach ($carts as $cart) {
                        if($key == $cart->getProduct()->getId())
                            $cart->setQty($qty);
                    }
                }
            }

            $this->em->flush();

            return $this->getCartPrices();
        }

        $carts = json_decode($this->session->get('cart'), true);

        foreach ($qtys as $key => $qty) {
            if(is_numeric($key)) {
                foreach ($carts as $cartKey => $cart) {
                    if($cart['id'] == $key) {
                        $carts[$cartKey]['qty'] = $qty;
                    }
                }
            }
        }

        $this->session->set('cart', json_encode($carts));

        return $this->getCartPrices();
    }
}