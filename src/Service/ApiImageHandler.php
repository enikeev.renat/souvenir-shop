<?php

namespace App\Service;

use App\Entity\Visual;
use Exception;
use Intervention\Image\ImageManagerStatic as Intervention;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Liip\ImagineBundle\Imagine\Data\DataManager;
use Liip\ImagineBundle\Imagine\Filter\FilterManager;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\UrlHelper;

class ApiImageHandler
{
    private $filename;

    private $targetDirectory;

    private $relativeDirectory;

    private $filesystem;

    private $cacheManager;

    private $filterManager;

    private $dataManager;

    private $urlHelper;

    public function __construct($targetDirectory, $relativeDirectory, Filesystem $filesystem, CacheManager $cacheManager, FilterManager $filterManager, DataManager $dataManager, UrlHelper $urlHelper)
    {
        $this->targetDirectory = $targetDirectory;
        $this->filesystem = $filesystem;
        $this->cacheManager = $cacheManager;
        $this->filterManager = $filterManager;
        $this->dataManager = $dataManager;
        $this->relativeDirectory = $relativeDirectory;
        $this->urlHelper = $urlHelper;
    }

    public function getTargetDirectory()
    {
        return $this->targetDirectory;
    }

    public function getRelativeDirectory()
    {
        return $this->relativeDirectory;
    }

    public function upload(UploadedFile $file)
    {
        if(null === $file)
            return null;

        $this->filename = uniqid().'.'.$file->guessExtension();

        $this->makeDirs();

        Intervention::make($file)->resize(1920, 1280, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        })->save($this->getPath());

        return $this->filename;
    }

    private function getFirstDirName()
    {
        return $this->filename !== null ? mb_substr($this->filename, 0, 2) : '';
    }

    private function getSecondDirName()
    {
        return $this->filename !== null ? mb_substr($this->filename, 2, 2) : '';
    }

    private function getPath()
    {
        return $this->getTargetDirectory()."/".$this->getFirstDirName()."/".$this->getSecondDirName()."/".$this->filename;
    }

    private function getRelativePath()
    {
        return $this->getRelativeDirectory()."/".$this->getFirstDirName()."/".$this->getSecondDirName()."/".$this->filename;
    }

    private function makeDirs()
    {
        if(!$this->filesystem->exists($this->getTargetDirectory()."/".$this->getFirstDirName()))
            $this->filesystem->mkdir($this->getTargetDirectory()."/".$this->getFirstDirName(), 0755);

        if(!$this->filesystem->exists($this->getTargetDirectory()."/".$this->getFirstDirName()."/".$this->getSecondDirName()))
            $this->filesystem->mkdir($this->getTargetDirectory()."/".$this->getFirstDirName()."/".$this->getSecondDirName(), 0755);
    }

    public function delete($filename)
    {
        if(empty($filename))
            return false;

        $this->filename = $filename;

        if(!$this->filesystem->exists($this->getPath()))
            return false;


        $this->filesystem->remove($this->getPath());

        $this->cacheManager->remove($this->getRelativePath());

        return true;
    }

    public function createThumbnails($filename, $filters) : void
    {
        if(empty($filename))
            return;

        if(!is_array($filters))
            return;

        $this->filename = $filename;

        if(!$this->filesystem->exists($this->getPath()))
            return;

        foreach($filters as $filter) {
            try {
                if (!$this->cacheManager->isStored($this->getRelativePath(), $filter)) {
                    $binary = $this->dataManager->find($filter, $this->getRelativePath());
                    $filteredBinary = $this->filterManager->applyFilter($binary, $filter);

                    $this->cacheManager->store($filteredBinary, $this->getRelativePath(), $filter);
                    $this->cacheManager->resolve($this->getRelativePath(), $filter);
                }
            } catch (Exception $e) {
                //do nothing
            }
        }
    }

    public function getVisual($filename) : ?Visual
    {
        if(empty($filename))
            return null;

        $this->filename = $filename;

        $visual = new Visual();
        $visual->setFilename($filename);
        $urls = [
            'original' => $this->urlHelper->getAbsoluteUrl($this->getRelativePath())
        ];

        foreach ($this->filterManager->getFilterConfiguration()->all() as $key => $val) {
            if ($this->cacheManager->isStored($this->getRelativePath(), $key)) {
                $urls[$key] = $this->cacheManager->getBrowserPath($this->getRelativePath(), $key);
            }
        }

        $visual->setUrls($urls);

        return $visual;
    }

    public function rotate($filename, $degrees)
    {
        $this->filename = $filename;

        Intervention::make($this->getPath())->rotate($degrees * -1)->save($this->getPath());

        $cached_filters = [];

        foreach ($this->filterManager->getFilterConfiguration()->all() as $key => $val) {
            if ($this->cacheManager->isStored($this->getRelativePath(), $key)) {
                $cached_filters[] = $key;
            }
        }

        $this->cacheManager->remove($this->getRelativePath());

        $this->createThumbnails($filename, $cached_filters);
    }

    public function handleVisual($json) : ?string
    {
        $visual = json_decode($json);

        if(!isset($visual->filename))
            return null;

        if(isset($visual->removed) && $visual->removed === true) {
            $this->delete($visual->filename);
            return null;
        }

        if(isset($visual->rotate)) {
            $this->rotate($visual->filename, $visual->rotate);
        }

        return $visual->filename;
    }
}