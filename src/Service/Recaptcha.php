<?php


namespace App\Service;


class Recaptcha
{
    private $secretKey;

    public function __construct($secretKey)
    {
        $this->secretKey = $secretKey;
    }

    public function check($gResponse) : bool
    {
        if(empty($gResponse))
            return false;

        $response = json_decode(file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$this->secretKey.'&response='.$gResponse));

        return $response->success != true OR $response->score <= 0.5 ? false : true;
    }
}