<?php


namespace App\Service;


use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class SNHandler
{
    private $serializer;

    private $normalizer;

    public function __construct()
    {
        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $this->normalizer = [new DateTimeNormalizer(), new ObjectNormalizer($classMetadataFactory)];
        $this->serializer = new Serializer($this->normalizer);
    }

    public function getSerializer()
    {
        return $this->serializer;
    }
}