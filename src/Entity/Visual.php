<?php


namespace App\Entity;

use Symfony\Component\Serializer\Annotation\Groups;

class Visual
{
    /**
     * @Groups({"api"})
     */
    private $filename;

    /**
     * @Groups({"api"})
     */
    private $urls = array();

    public function getFilename()
    {
        return $this->filename;
    }

    public function setFilename($filename): void
    {
        $this->filename = $filename;
    }

    public function getUrls(): array
    {
        return $this->urls;
    }

    public function setUrls(array $urls): void
    {
        $this->urls = $urls;
    }
}