<?php

namespace App\Entity;

use App\Repository\ReviewRepository;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ReviewRepository::class)
 */
class Review
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"api"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message = "Автор должен быть указан")
     * @Groups({"api"})
     */
    private $author;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank(message = "Текст отзыва не может быть пустым")
     * @Groups({"api"})
     */
    private $text;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"api"})
     */
    private $published;

    /**
     * @ORM\OneToMany(targetEntity=ReviewImage::class, mappedBy="review", orphanRemoval=true)
     * @Groups({"api"})
     */
    private $reviewGallery;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"api"})
     */
    private $added;

    public function __construct()
    {
        $this->reviewGallery = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAuthor(): ?string
    {
        return $this->author;
    }

    public function setAuthor(string $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getPublished(): ?bool
    {
        return $this->published;
    }

    public function setPublished(bool $published): self
    {
        $this->published = $published;

        return $this;
    }

    /**
     * @return Collection|ReviewImage[]
     */
    public function getReviewGallery(): Collection
    {
        return $this->reviewGallery;
    }

    public function addReviewGallery(ReviewImage $reviewGallery): self
    {
        if (!$this->reviewGallery->contains($reviewGallery)) {
            $this->reviewGallery[] = $reviewGallery;
            $reviewGallery->setReview($this);
        }

        return $this;
    }

    public function removeReviewGallery(ReviewImage $reviewGallery): self
    {
        if ($this->reviewGallery->contains($reviewGallery)) {
            $this->reviewGallery->removeElement($reviewGallery);
            // set the owning side to null (unless already changed)
            if ($reviewGallery->getReview() === $this) {
                $reviewGallery->setReview(null);
            }
        }

        return $this;
    }

    public function getAdded(): ?DateTimeInterface
    {
        return $this->added;
    }

    public function setAdded(DateTimeInterface $added): self
    {
        $this->added = $added;

        return $this;
    }
}
