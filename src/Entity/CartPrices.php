<?php


namespace App\Entity;


class CartPrices
{
    private $goodsCount = 0;

    private $itemsCount = 0;

    private $priceWithoutDiscount = 0;

    private $discount = 0;

    private $priceWithDiscount = 0;

    public function getGoodsCount(): int
    {
        return $this->goodsCount;
    }

    public function setGoodsCount(int $goodsCount): self
    {
        $this->goodsCount = $goodsCount;

        return $this;
    }

    public function getItemsCount(): int
    {
        return $this->itemsCount;
    }

    public function setItemsCount(int $itemsCount): self
    {
        $this->itemsCount = $itemsCount;

        return $this;
    }

    public function getPriceWithoutDiscount(): int
    {
        return $this->priceWithoutDiscount;
    }

    public function setPriceWithoutDiscount(int $priceWithoutDiscount): self
    {
        $this->priceWithoutDiscount = $priceWithoutDiscount;

        return $this;
    }

    public function getDiscount(): int
    {
        return $this->discount;
    }

    public function setDiscount(int $discount): self
    {
        $this->discount = $discount;

        return $this;
    }

    public function getPriceWithDiscount(): int
    {
        return $this->priceWithDiscount;
    }

    public function setPriceWithDiscount(int $priceWithDiscount): self
    {
        $this->priceWithDiscount = $priceWithDiscount;

        return $this;
    }
}